<?php

return [
	'account_number' => env('FEDEX_ACCOUNT_NUMBER', null),
	'meter_number' => env('FEDEX_METER_NUMBER', null),
	'key' => env('FEDEX_KEY', null),
	'password' => env('FEDEX_PASSWORD', null),
];