#!/bin/bash

echo "Please enter the NAME of new database! (example: database1)"
read dbname
# echo "Please enter the database CHARACTER SET! (example: latin1, utf8, ...)"
# read charset
echo "Creating new database..."
mysql -uroot -e "CREATE DATABASE ${dbname} CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
echo "Database COLLATE utf8_unicode_ci successfully created!"
# echo "Showing existing databases..."
# mysql -uroot -e "show databases;"
echo "============="
# echo "Please enter the USERNAME of new database! (example: user1)"
# read username
# echo "Please enter the PASSWORD for new user!"
# read userpass
echo "Creating new user..."
mysql -uroot -e "CREATE USER ${dbname}_user@localhost IDENTIFIED WITH mysql_native_password BY 'Yan@1234';"
echo "User successfully created!"
echo "Granting ALL privileges on ${dbname} to ${dbname}_user!"
mysql -uroot -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${dbname}_user'@'localhost' WITH GRANT OPTION;"
mysql -uroot -e "FLUSH PRIVILEGES;"
echo "You're good now :)"
echo "DB_DATABASE=${dbname}"
echo "DB_USERNAME=${dbname}_user"
echo "DB_PASSWORD=Yan@1234"
echo "===== Edit .env file with the above info before continue ====="
echo " "
echo "Caution: if User model is moved. Edit new path in auth.php > providers > user > model!"
echo "Done editing? Want to continue installing Voyager admin? [y/n?]"
read confirm
if [ $confirm = "y" ]; then
    echo "Start require voyager"
    composer require tcg/voyager
    echo "======"
    php artisan voyager:install --with-dummy
else
    echo "No voyager!"
fi
exit