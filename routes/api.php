<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get-colors', "APIController@getColors");
Route::get('get-sizes', "APIController@getSizes");

Route::get('get-product-categories', "APIController@productCategories");
Route::post('get-all-products', "APIController@allProduct");
Route::post('get-product-info', "APIController@productInfo");
Route::post('list-product-details', "APIController@listProductDetails");

Route::group(['prefix' => 'product-input'], function(){
	Route::post('/', "Voyager\ProductInputVoyagerController@getProductInput");
	// Route::post('get-serial', "APIController@getSerial");
	// Route::post('add-serial', "APIController@addSerial");
	Route::post('remove-serial', "Voyager\ProductInputVoyagerController@removeSerial");
	Route::post('check-serial-unique', "Voyager\ProductInputVoyagerController@uniqueSerial");
	Route::post('submit', "Voyager\ProductInputVoyagerController@productInputSubmit");
});

Route::group(['prefix' => 'order-information'], function(){
	Route::post('serial-status-to-1', "Voyager\OrderInformationVoyagerController@changeSerialStatusToOne");
	Route::post('serial-status-to-0', "Voyager\OrderInformationVoyagerController@changeSerialStatusToZero");
});