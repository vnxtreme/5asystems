<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//PAYPAL
Route::get('paypal/success', 'PaypalController@success')->name('paypal.success');
Route::get('paypal/cancel', 'PaypalController@cancel')->name('paypal.cancel');

Route::get('/', 'HomeController@index')->name('home');
Route::get('category/{categorySlug}', 'ProductController@list')->name('product.list');
Route::get('search', 'ProductController@search')->name('search');
Route::get('my-company/{slug}', 'HomeController@myCompany')->name('my-company');
Route::get('contact', 'HomeController@contact')->name('contact');
Route::post('send-contact', 'HomeController@sendContact')->name('contact.post');

Route::group(['prefix' => 'cart', 'as' => 'cart'], function () {
    Route::get('/', 'CartController@cart');
    Route::get('checkout', 'CartController@checkout')->name('.checkout');
    Route::post('post-checkout', 'CartController@checkoutPost')->name('.checkout.post');
    Route::get('checkout-success', 'CartController@checkoutSuccess')->name('.checkout.success');
    Route::post('add-cart', 'CartController@add')->name('.add');
    Route::post('update', 'CartController@update')->name('.update');
	Route::get('remove/{rowId}', 'CartController@remove')->name('.remove');
	
	Route::post('validate-address-id', "CartController@validateAddressId")->name('.validate-address-id');
	Route::post('validate-address', "CartController@validateAddress")->name('.validate-address');
});

Route::group(['prefix' => 'user', 'as' => 'user', 'middleware' => 'auth'], function () {
    Route::get('my-account', 'UserController@account')->name('.account');
    Route::get('profile', 'UserController@profile')->name('.profile');
    Route::post('profile-update', 'UserController@profileUpdate')->name('.profile.update');
	Route::get('addresses', 'UserController@addresses')->name('.addresses');
	
	Route::match(['post', 'get'], 'address-add', 'UserController@addressAdd')->name('.addresses.add');
	Route::match(['post', 'get'], 'address-update/{address_id}', 'UserController@adressUpdate')->name('.addresses.update');
	Route::get('address-delete', 'UserController@addressDelete')->name('.addresses.delete');
	
    Route::get('order-history', 'UserController@orderHistory')->name('.order-history');
});

Route::get('mail', 'CartController@mail');

Route::group(['prefix' => 'coupon'], function () {
    Route::post('apply', 'CouponController@apply')->name('coupon.apply');
    Route::post('destroy', 'CouponController@destroy')->name('coupon.destroy');
});

Route::group(['prefix' => 'news'], function () {
    Route::get('/', 'NewsController@list')->name('news');
    Route::get('{slug}', 'NewsController@detail')->name('news.detail');
});

Route::group(['prefix' => 'admin'], function () {
	Route::get('customers', 'Voyager\VoyagerUserController@customer');
    Voyager::routes();
});

Auth::routes(['verify' => true]);

Route::group(['prefix' => 'fedex'], function () {
    Route::get('check-rate', 'FedEx\RateRequestController@checkRate');
    Route::get('validate', 'FedEx\ValidateAddressController@handle');
	Route::post('track', 'FedEx\TrackController@trackById');
});

Route::group(['prefix' => 'excel'], function () {
    Route::get('import-view', 'ImportController@importView')->name('import.view');
    Route::post('import-product', 'ImportController@product')->name('import.product');
    Route::post('import-stock', 'ImportController@stock')->name('import.stock');
});

Route::get('{slug}', 'ProductController@detail')->name('product.detail'); //always bottom
