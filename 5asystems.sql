-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: 5asystems
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` int(11) NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_or_province_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (7,5,'Xtreme','VN',NULL,'1219446171','124/35/3 Xo Viet Nghe Tinh',70000,'HO Chi Minh',NULL,0,'2019-05-19 06:50:50','2019-05-19 06:50:50'),(8,8,'Hoang','Nguyen',NULL,'1219446171','1 Xo Viet Nghe Tinh',70000,'HO Chi Minh',NULL,0,'2019-05-20 07:37:54','2019-05-20 07:37:54'),(9,8,'Hoang','Nguyen',NULL,'123123123','38/6A Nguyen Van Troi, Phu Nhuan',70000,'HO Chi Minh',NULL,0,'2019-05-22 11:00:07','2019-05-22 11:00:07');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carousels`
--

DROP TABLE IF EXISTS `carousels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `carousels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carousels`
--

LOCK TABLES `carousels` WRITE;
/*!40000 ALTER TABLE `carousels` DISABLE KEYS */;
INSERT INTO `carousels` VALUES (1,'Lorem ipsum dolor','Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium autem eligendi aliquid id! Quasi assumenda atque optio nisi, aliquam dolor hic aliquid et autem ipsa similique animi. Sequi, nam modi.','https://tinhte.vn','carousels/May2019/macbook.png',NULL,'2019-05-19 17:30:54','2019-05-20 08:47:44'),(2,'Lorem ipsum dolor 2','Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium autem eligendi aliquid id! Quasi assumenda atque optio nisi, aliquam dolor hic aliquid et autem ipsa similique animi. Sequi, nam modi.','http://genk.vn','carousels/May2019/phone.png',NULL,'2019-05-19 17:31:18','2019-05-20 08:47:36');
/*!40000 ALTER TABLE `carousels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_feature_home` smallint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `hex` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Green','#00f900','2019-04-13 08:20:43','2019-05-08 01:37:08'),(2,'Black','#000000','2019-04-13 08:20:48','2019-05-08 01:36:47'),(3,'White','#ffffff','2019-04-13 08:20:53','2019-05-08 01:36:05');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Digital Electronics','New Street 10-693 New York Poland','123456789','lesa@gmail.com',NULL,'2019-05-20 03:07:23','2019-05-20 03:07:23');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `is_one_time` tinyint(1) NOT NULL DEFAULT '0',
  `valid_until` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupons_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
INSERT INTO `coupons` VALUES (1,'XTREME','percentage',20,1,'2019-05-22',NULL,'2019-05-22 10:47:25');
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_messages`
--

DROP TABLE IF EXISTS `customer_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_messages`
--

LOCK TABLES `customer_messages` WRITE;
/*!40000 ALTER TABLE `customer_messages` DISABLE KEYS */;
INSERT INTO `customer_messages` VALUES (1,'nghoang2013@gmail.com','asdad asd sadas dl lorem',NULL,'2019-05-20 03:28:34','2019-05-20 03:28:34'),(2,'nghoang2013@gmail.comm','321321313123',NULL,'2019-05-20 03:55:06','2019-05-20 03:55:06');
/*!40000 ALTER TABLE `customer_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `birthdate` date DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,NULL,'Hoang Nguyen','Hoang','Nguyen','xtreme@gmail.com','users/default.png','$2y$10$snar81maUmpBh0EXy0lyAu8HwXtPdBn9x2qsaSk17P3ewY.M7hg/u',NULL,1,NULL,NULL,NULL,'2019-05-19 12:08:38','2019-05-19 12:08:38');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'{}',1),(2,1,'name','text','Name',0,1,1,1,1,1,'{}',2),(3,1,'email','text','Email',1,1,1,1,1,1,'{}',3),(4,1,'password','password','Password',0,0,1,1,1,1,'{}',4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'{}',5),(6,1,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,'{}',9),(9,1,'user_belongsto_role_relationship','relationship','Role',0,0,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',11),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,0,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',12),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,'{}',14),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',0,0,1,1,1,1,'{}',10),(22,4,'id','number','ID',1,0,0,0,0,0,'{}',1),(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(24,4,'order','number','Order',1,1,1,1,1,1,'{\"display\":{\"width\":1},\"default\":1}',5),(25,4,'name','text','Name',1,1,1,1,1,1,'{\"display\":{\"width\":7}}',3),(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"display\":{\"width\":5},\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"required,unique:categories,slug\"}}',4),(27,4,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',12),(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(29,5,'id','number','ID',1,0,0,0,0,0,'{}',1),(30,5,'author_id','text','Author',1,0,1,1,0,1,'{}',2),(31,5,'category_id','text','Category',0,0,1,1,1,0,'{}',3),(32,5,'title','text','Title',1,1,1,1,1,1,'{}',4),(33,5,'excerpt','text_area','Excerpt',0,0,1,1,1,1,'{}',5),(34,5,'body','rich_text_box','Body',1,0,1,1,1,1,'{}',6),(35,5,'image','image','Post Image',0,1,1,1,1,1,'{\"quality\":\"100%\",\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"70%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"450\",\"height\":\"350\"}}]}',7),(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(37,5,'meta_description','text_area','Meta Description',0,0,1,1,1,1,'{}',9),(38,5,'meta_keywords','text_area','Meta Keywords',0,0,1,1,1,1,'{}',10),(39,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(40,5,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',12),(41,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(42,5,'seo_title','text','SEO Title',0,1,1,1,1,1,'{}',14),(43,5,'featured','checkbox','Featured',1,1,1,1,1,1,'{}',15),(44,6,'id','number','ID',1,0,0,0,0,0,NULL,1),(45,6,'author_id','text','Author',1,0,0,0,0,0,NULL,2),(46,6,'title','text','Title',1,1,1,1,1,1,NULL,3),(47,6,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,4),(48,6,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,5),(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(50,6,'meta_description','text','Meta Description',1,0,1,1,1,1,NULL,7),(51,6,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,NULL,8),(52,6,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(53,6,'created_at','timestamp','Created At',1,1,1,0,0,0,NULL,10),(54,6,'updated_at','timestamp','Updated At',1,0,0,0,0,0,NULL,11),(55,6,'image','image','Page Image',0,1,1,1,1,1,NULL,12),(56,7,'id','text','Id',1,0,0,0,0,0,'{}',1),(57,7,'name','text','Name',1,1,1,1,1,1,'{\"display\":{\"width\":8}}',2),(58,7,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(59,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(60,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(62,8,'locale','text','Locale',0,1,1,1,1,1,'{}',3),(63,8,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(64,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(65,9,'id','text','Id',1,0,0,0,0,0,'{}',1),(66,9,'name','text','Name',1,1,1,1,1,1,'{}',2),(67,9,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',3),(68,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(69,10,'id','text','Id',1,0,0,0,0,0,'{}',1),(70,10,'duration','number','Tháng',1,1,1,1,1,1,'{}',2),(71,10,'price','number','Đơn giá',1,1,1,1,1,1,'{}',3),(72,10,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(73,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(74,11,'id','text','Id',1,0,0,0,0,0,'{}',1),(75,11,'name','text','Name',1,1,1,1,1,1,'{\"display\":{\"width\":8}}',3),(76,11,'slug','text','Slug',1,1,1,1,1,1,'{\"display\":{\"width\":4},\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:products,slug\"}}',4),(77,11,'status','checkbox','Status',1,1,1,1,1,1,'{\"on\":\"Active\",\"off\":\"Hidden\",\"checked\":true}',13),(78,11,'seo_title','text','Seo Title',0,0,1,1,1,1,'{}',5),(79,11,'meta_description','text','Meta Description',0,0,1,1,1,1,'{}',7),(80,11,'meta_keywords','text','Meta Keywords',0,0,1,1,1,1,'{}',6),(81,11,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',17),(82,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',18),(93,11,'category_id','text','Category Id',0,0,1,1,1,1,'{}',2),(94,11,'excerpt','text','Excerpt',0,0,1,1,1,1,'{}',8),(95,11,'content','rich_text_box','Content',0,0,1,1,1,1,'{}',9),(99,13,'id','text','Id',1,0,0,0,0,0,'{}',1),(100,13,'name','text','Name',1,1,1,1,1,1,'{\"display\":{\"width\":7}}',4),(101,13,'slug','text','Slug',1,1,1,1,1,1,'{\"display\":{\"width\":4},\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:product_categories,slug\"}}',5),(102,13,'parent_id','text','Parent Id',0,1,1,1,1,1,'{}',2),(103,13,'order','number','Order',0,1,1,1,1,1,'{\"display\":{\"width\":1}}',6),(104,13,'image','image','Image',0,1,1,1,1,1,'{\"quality\":\"100%\",\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"60%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"400\",\"height\":\"400\"}}],\"preserveFileUploadName\":true,\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\":attribute is greater than 2Mb.\"}}}',10),(105,13,'seo_title','text','Seo Title',0,0,1,1,1,1,'{\"display\":{\"width\":6}}',7),(106,13,'meta_description','text_area','Meta Description',0,0,1,1,1,1,'{}',9),(107,13,'meta_keywords','text','Meta Keywords',0,0,1,1,1,1,'{\"display\":{\"width\":6}}',8),(108,13,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',12),(109,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(110,11,'order','number','Order',0,1,1,1,1,1,'{}',12),(111,13,'product_category_belongsto_product_category_relationship','relationship','Belongs to',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"parent_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(116,8,'name','text','Name',1,1,1,1,1,1,'{}',2),(118,15,'id','text','Id',1,0,0,0,0,0,'{}',1),(120,15,'product_detail_id','text','Product Detail Id',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(121,15,'quantity','number','Quantity',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',4),(122,15,'created_at','timestamp','Created At',0,1,0,0,0,0,'{}',5),(123,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(124,15,'product_input_hasmany_product_relationship','relationship','Product',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\ProductDetail\",\"table\":\"product_details\",\"type\":\"belongsTo\",\"column\":\"product_detail_id\",\"key\":\"id\",\"label\":\"product_id\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(131,17,'id','text','Id',1,0,0,0,0,0,'{}',1),(132,17,'subject','text','Subject',1,1,1,1,1,1,'{}',2),(133,17,'html','rich_text_box','Html',1,0,1,1,1,1,'{}',3),(134,17,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(135,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(136,18,'id','text','Id',1,0,0,0,0,0,'{}',1),(137,18,'name','text','Name',0,1,1,1,1,1,'{\"display\":{\"width\":6}}',2),(138,18,'description','text_area','Description',0,0,1,1,1,1,'{}',5),(139,18,'link','text','Link',0,1,1,1,1,1,'{\"display\":{\"width\":5}}',3),(140,18,'image','image','Image',1,1,1,1,1,1,'{\"quality\":\"100%\",\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"70%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"300\"}}],\"preserveFileUploadName\":true}',6),(141,18,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',7),(142,18,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(143,4,'seo_title','text','Seo Title',0,0,1,1,1,1,'{\"display\":{\"width\":6}}',6),(144,4,'meta_description','text','Meta Description',0,0,1,1,1,1,'{}',8),(145,4,'meta_keywords','text','Meta Keywords',0,0,1,1,1,1,'{\"display\":{\"width\":6}}',7),(146,4,'image','image','Image',0,1,1,1,1,1,'{\"preserveFileUploadName\":true,\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\":attribute is greater than 2Mb.\"}}}',9),(147,4,'image_fb','image','Image Fb',0,0,1,1,1,1,'{\"preserveFileUploadName\":true,\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\":attribute is greater than 2Mb.\"}}}',10),(148,4,'is_feature_home','checkbox','Is Feature Home',0,1,1,1,1,1,'{\"on\":\"\\u2714\",\"off\":\"-\",\"checked\":false}',11),(149,13,'image_fb','image','Image Fb',0,0,1,1,1,1,'{\"preserveFileUploadName\":true,\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\":attribute is greater than 2Mb.\"}}}',11),(150,7,'hex','color','Hex',0,1,1,1,1,1,'{\"display\":{\"width\":4}}',3),(151,11,'image','image','Image (1:1)',0,1,1,1,1,1,'{\"quality\":\"100%\",\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"60%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"450\",\"height\":\"450\"}}],\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\"The :attribute is greater than 2mb\"}},\"preserveFileUploadName\":true}',10),(152,11,'image_fb','image','Image (2:1)',0,0,1,1,1,1,'{\"resize\":{\"thumbnails\":[{\"type\":\"resize\",\"name\":\"resize-500\",\"width\":1400,\"height\":700}]},\"quality\":\"100%\",\"validation\":{\"rule\":\"mimes:jpeg,png|max:2000\",\"messages\":{\"max\":\"The :attribute is greater than 2Mb\"}},\"preserveFileUploadName\":true}',11),(167,11,'featured','checkbox','Featured',0,1,1,1,1,1,'{\"on\":\"Homepage\",\"off\":\"-\",\"checked\":false}',14),(168,18,'order','number','Order',0,1,1,1,1,1,'{\"display\":{\"width\":1}}',4),(169,11,'is_popular','checkbox','Popular',0,1,1,1,1,1,'{\"on\":\"Popular\",\"off\":\"-\",\"checked\":false}',15),(170,11,'is_bestseller','checkbox','Bestseller',0,1,1,1,1,1,'{\"on\":\"Bestseller\",\"off\":\"-\",\"checked\":false}',16),(171,1,'email_verified_at','timestamp','Email Verified At',0,0,0,0,0,0,'{}',8),(172,1,'first_name','text','First Name',0,0,1,1,1,1,'{}',13),(173,1,'last_name','text','Last Name',0,0,1,1,1,1,'{}',15),(174,1,'gender','text','Gender',0,0,1,1,1,1,'{}',16),(175,1,'newsletter','text','Newsletter',0,0,1,1,1,1,'{}',17),(176,1,'birthdate','text','Birthdate',0,0,1,1,1,1,'{}',18),(177,21,'id','text','Id',1,0,0,0,0,0,'{}',1),(178,21,'title','text','Title',1,1,1,1,1,1,'{\"display\":{\"width\":6}}',2),(179,21,'content','rich_text_box','Content',1,0,1,1,1,1,'{}',4),(180,21,'order','number','Order',0,1,1,1,1,1,'{\"display\":{\"width\":1}}',5),(181,21,'status','checkbox','Status',1,1,1,1,1,1,'{\"on\":\"Active\",\"off\":\"Hidden\",\"checked\":true,\"display\":{\"width\":2}}',6),(182,21,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',7),(183,21,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(184,21,'slug','text','Slug',1,1,1,1,1,1,'{\"display\":{\"width\":3},\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:my_companies,slug\"}}',3),(185,22,'id','text','Id',1,0,0,0,0,0,'{}',1),(186,22,'name','text','Name',0,1,1,1,1,1,'{}',2),(187,22,'address','text','Address',1,1,1,1,1,1,'{}',3),(188,22,'phone','text','Phone',0,1,1,1,1,1,'{}',4),(189,22,'email','text','Email',0,1,1,1,1,1,'{}',5),(190,22,'order','text','Order',0,1,1,1,1,1,'{}',6),(191,22,'created_at','timestamp','Created At',0,0,0,0,0,1,'{}',7),(192,22,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(193,23,'id','text','Id',1,0,0,0,0,0,'{}',1),(194,23,'email','text','Email',1,1,1,1,1,1,'{}',2),(195,23,'message','rich_text_box','Message',1,1,1,1,1,1,'{}',3),(196,23,'type','hidden','Type',0,0,0,0,0,0,'{}',4),(197,23,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',5),(198,23,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(199,24,'id','text','Id',1,0,0,0,0,0,'{}',1),(200,24,'code','text','Code',1,1,1,1,1,1,'{\"display\":{\"width\":3}}',2),(201,24,'type','select_dropdown','Type',1,1,1,1,1,1,'{\"display\":{\"width\":2},\"default\":\"percentage\",\"options\":{\"percentage\":\"Percentage\",\"number\":\"Number\"}}',4),(202,24,'value','number','Value',1,1,1,1,1,1,'{\"display\":{\"width\":2}}',3),(203,24,'is_one_time','checkbox','Is One Time',1,1,1,1,1,1,'{\"on\":\"True\",\"off\":\"False\",\"checked\":true,\"display\":{\"width\":2}}',5),(204,24,'valid_until','date','Valid Until',0,1,1,1,1,1,'{\"display\":{\"width\":3}}',6),(205,24,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',7),(206,24,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(207,25,'id','number','Id',1,1,0,1,0,0,'{\"display\":{\"width\":8}}',1),(208,25,'name','text','Name',0,1,1,1,1,1,'{\"display\":{\"width\":5}}',3),(209,25,'email','text','Email',0,1,1,1,1,1,'{\"display\":{\"width\":3}}',4),(210,25,'phone','text','Phone',0,0,1,1,1,1,'{\"display\":{\"width\":2}}',5),(211,25,'address','text','Address',0,0,1,1,1,1,'{\"display\":{\"width\":6}}',7),(212,25,'delivery_message','text_area','Delivery Message',0,0,1,1,1,1,'{}',18),(213,25,'discount_id','hidden','Discount Id',0,0,1,1,1,1,'{}',11),(214,25,'postal_code','text','Postal Code',0,0,1,1,1,1,'{\"display\":{\"width\":2}}',9),(215,25,'city','text','City',0,0,1,1,1,1,'{\"display\":{\"width\":2}}',8),(216,25,'delivery_fee','text','Delivery Fee (USD)',1,0,1,1,1,1,'{\"display\":{\"width\":2}}',14),(217,25,'discount','text','Discount (USD)',1,0,1,1,1,1,'{\"display\":{\"width\":2}}',13),(218,25,'coupon','text','Coupon',0,0,1,1,1,1,'{\"display\":{\"width\":2}}',12),(219,25,'tax','text','Tax',1,0,1,1,1,1,'{\"display\":{\"width\":2}}',15),(220,25,'sub_total','text','Sub Total (USD)',0,0,1,1,1,1,'{\"display\":{\"width\":2}}',16),(221,25,'total','text','Total (USD)',0,1,1,1,1,1,'{\"display\":{\"width\":2}}',17),(222,25,'status','select_dropdown','Status',0,1,1,1,1,1,'{\"display\":{\"width\":2},\"default\":\"pending\",\"options\":{\"pending\":\"Wating payment\",\"sending\":\"Sending\",\"completed\":\"Completed\"}}',6),(223,25,'created_at','timestamp','Created At',0,1,0,1,0,0,'{\"display\":{\"width\":4}}',2),(224,25,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',19),(225,25,'payment_method','select_dropdown','Payment Method',0,0,1,1,1,1,'{\"display\":{\"width\":2},\"default\":\"bank\",\"options\":{\"bank\":\"Bank\"}}',10);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','App\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":\"roleNotNull\"}','2019-04-08 07:54:11','2019-05-20 06:11:36'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2019-04-08 07:54:11','2019-05-18 02:33:13'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2019-04-08 07:54:12','2019-05-18 00:55:23'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2019-04-08 07:54:12','2019-04-08 07:54:12'),(7,'colors','colors','Màu sắc','Màu sắc','voyager-paint-bucket','App\\Models\\Color',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-04-12 23:58:05','2019-05-08 01:36:34'),(8,'units','units','Đơn vị tính','Đơn vị tính','voyager-dollar','App\\Models\\Unit',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-04-12 23:59:28','2019-04-13 10:56:35'),(9,'sizes','sizes','Kích thước','Kích thước','voyager-pen','App\\Models\\Size',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-04-13 00:01:59','2019-04-13 00:01:59'),(10,'warranties','warranties','Hạn bảo hành','Hạn bảo hành','voyager-hammer','App\\Models\\Warranty',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-04-13 00:03:45','2019-04-13 00:03:45'),(11,'products','products','Product','Products','voyager-diamond','App\\Models\\Product',NULL,'App\\Http\\Controllers\\Voyager\\ProductVoyagerController',NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-04-13 00:09:34','2019-05-19 18:56:00'),(13,'product_categories','product-categories','Product Category','Product Categories','voyager-archive','App\\Models\\ProductCategory',NULL,NULL,NULL,1,1,'{\"order_column\":\"parent_id\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":\"slug\",\"scope\":null}','2019-04-13 01:10:13','2019-05-18 02:33:00'),(15,'product_inputs','product-inputs','Product Input','Product Inputs','voyager-move','App\\Models\\ProductInput',NULL,'App\\Http\\Controllers\\Voyager\\ProductInputVoyagerController',NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":\"created_at\",\"scope\":null}','2019-04-15 08:25:00','2019-05-18 11:16:46'),(17,'email_templates','email-templates','Email Template','Email Templates','voyager-pen','App\\Models\\EmailTemplate',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-05-06 21:48:17','2019-05-06 21:48:17'),(18,'carousels','carousels','Carousel','Carousels','voyager-photos','App\\Models\\Carousel',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-07 08:13:49','2019-05-17 23:30:34'),(21,'my_companies','my-companies','My Company','My Companies','voyager-company','App\\Models\\MyCompany',NULL,NULL,NULL,1,0,'{\"order_column\":\"order\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-20 02:38:41','2019-05-20 02:43:37'),(22,'contacts','contacts','Contact','Contacts','voyager-phone','App\\Models\\Contact',NULL,NULL,NULL,1,0,'{\"order_column\":\"order\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-05-20 03:06:53','2019-05-20 03:06:53'),(23,'customer_messages','customer-messages','Customer Message','Customer Messages','voyager-mail','App\\Models\\CustomerMessage',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-20 03:25:38','2019-05-26 00:54:02'),(24,'coupons','coupons','Coupon','Coupons','voyager-ticket','App\\Models\\Coupon',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-20 06:25:12','2019-05-20 07:06:24'),(25,'order_informations','order-informations','Order Information','Order Informations','voyager-basket','App\\Models\\OrderInformation',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-20 11:33:19','2019-05-26 00:39:36');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `email_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
INSERT INTO `email_templates` VALUES (1,'Order confirm','<p>Dear [customer_name],</p>\r\n<p>your order is <strong>confirmed</strong>.</p>\r\n<p>Total amount is [total_amount]</p>','2019-05-06 21:49:24','2019-05-06 23:16:35'),(2,'[Admin] Order confirm','<p>Order information:</p>\r\n<p>Customer name : [customer_name]</p>\r\n<p>Email : [email]</p>\r\n<p>Phone : [phone]</p>\r\n<p>Address: [address]</p>\r\n<p>City : [city]</p>\r\n<p>Total : [total_amount]</p>','2019-05-18 10:44:49','2019-05-18 10:44:49'),(3,'[Admin] Low in Quantity','<p>Product: [name]</p>\r\n<p>Product ID: [product_id]</p>\r\n<p>Current quantity: [qty]</p>','2019-05-21 07:52:57','2019-05-21 08:08:35');
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-04-08 07:54:11','2019-04-08 07:54:11','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,5,6,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,5,2,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,5,3,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,15,'2019-04-08 07:54:11','2019-05-26 01:03:17',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,4,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,5,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,7,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,8,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.settings.index',NULL),(11,1,'Categories','','_self','voyager-categories',NULL,5,1,'2019-04-08 07:54:11','2019-05-26 01:03:17','voyager.categories.index',NULL),(12,1,'News','','_self','voyager-news','#000000',NULL,13,'2019-04-08 07:54:12','2019-05-26 01:03:27','voyager.posts.index','null'),(14,1,'Hooks','','_self','voyager-hook',NULL,5,9,'2019-04-08 07:54:12','2019-05-26 01:03:17','voyager.hooks',NULL),(15,1,'Color','','_self','voyager-paint-bucket','#000000',23,1,'2019-04-12 23:58:05','2019-04-14 22:51:13','voyager.colors.index','null'),(16,1,'Unit price','','_self','voyager-dollar','#000000',23,2,'2019-04-12 23:59:28','2019-05-20 10:51:20','voyager.units.index','null'),(17,1,'Size','','_self','voyager-pen','#000000',23,3,'2019-04-13 00:01:59','2019-05-20 10:51:20','voyager.sizes.index','null'),(18,1,'Warranty','','_self','voyager-hammer','#000000',23,4,'2019-04-13 00:03:45','2019-05-20 10:51:20','voyager.warranties.index','null'),(19,1,'Product','','_self','voyager-diamond','#000000',NULL,5,'2019-04-13 00:09:34','2019-05-20 12:20:30','voyager.products.index','null'),(21,1,'Product Category','','_self','voyager-archive','#000000',NULL,4,'2019-04-13 01:10:13','2019-05-20 12:20:30','voyager.product-categories.index','null'),(23,1,'Product Attributes','','_self',NULL,'#ff9300',NULL,6,'2019-04-13 01:26:22','2019-05-20 12:20:28',NULL,''),(24,1,'Product Input','','_self','voyager-move',NULL,NULL,7,'2019-04-15 08:25:00','2019-05-20 12:20:28','voyager.product-inputs.index',NULL),(26,1,'Email Templates','','_self','voyager-pen',NULL,NULL,9,'2019-05-06 21:48:17','2019-05-20 12:20:22','voyager.email-templates.index',NULL),(27,1,'Carousels','','_self','voyager-photos',NULL,NULL,2,'2019-05-07 08:13:49','2019-05-20 10:51:20','voyager.carousels.index',NULL),(30,1,'My Companies','','_self','voyager-company',NULL,NULL,10,'2019-05-20 02:38:42','2019-05-20 12:20:22','voyager.my-companies.index',NULL),(31,1,'Contacts','','_self','voyager-phone',NULL,NULL,11,'2019-05-20 03:06:53','2019-05-20 12:20:22','voyager.contacts.index',NULL),(32,1,'Customer Messages','','_self','voyager-mail',NULL,NULL,12,'2019-05-20 03:25:38','2019-05-20 12:20:22','voyager.customer-messages.index',NULL),(33,1,'Coupons','','_self','voyager-ticket',NULL,NULL,8,'2019-05-20 06:25:12','2019-05-20 12:20:22','voyager.coupons.index',NULL),(34,1,'Order Informations','','_self','voyager-basket',NULL,NULL,3,'2019-05-20 11:33:19','2019-05-20 12:20:30','voyager.order-informations.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2019-04-08 07:54:11','2019-04-08 07:54:11');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2016_01_01_000000_create_pages_table',2),(235,'2016_01_01_000000_create_posts_table',3),(236,'2016_02_15_204651_create_categories_table',3),(237,'2017_04_11_000000_alter_post_nullable_fields_table',3),(238,'2019_04_09_143631_create_colors_table',3),(239,'2019_04_09_144015_create_sizes_table',3),(240,'2019_04_09_144410_create_warranties_table',3),(241,'2019_04_09_144529_create_units_table',3),(242,'2019_04_09_144530_create_product_categories_table',3),(243,'2019_04_09_145532_create_products_table',3),(244,'2019_04_09_145807_create_product_details_table',3),(245,'2019_04_09_150451_create_orders_table',3),(246,'2019_04_09_152018_create_order_items_table',3),(248,'2019_04_15_074709_create_product_inputs_table',4),(249,'2019_05_05_174752_create_shoppingcart_table',5),(253,'2019_05_07_043010_create_email_templates_table',6),(257,'2019_05_07_073544_create_order_informations_table',7),(258,'2019_05_07_073826_create_orders_table',7),(264,'2019_05_10_121029_create_coupons_table',8),(265,'2019_05_17_065640_create_user_coupons_table',8),(266,'2019_05_19_070809_create_addresses_table',9),(269,'2019_05_07_144339_create_carousels_table',10),(275,'2019_05_19_151559_create_customers_table',11),(277,'2019_05_20_093405_create_my_companies_table',12),(278,'2019_05_20_100341_create_contacts_table',13),(279,'2019_05_20_102313_create_customer_messages_table',14);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_companies`
--

DROP TABLE IF EXISTS `my_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `my_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `my_companies_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_companies`
--

LOCK TABLES `my_companies` WRITE;
/*!40000 ALTER TABLE `my_companies` DISABLE KEYS */;
INSERT INTO `my_companies` VALUES (1,'Delivery','delivery','<h2 style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 20px; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 1px; font-size: 1.25rem;\">Shipments and returns</h2>\r\n<h3 style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 2px; font-size: 16px;\">Your pack shipment</h3>\r\n<p style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p>\r\n<p style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br style=\"box-sizing: inherit;\" /><br style=\"box-sizing: inherit;\" />Boxes are amply sized and your items are well-protected.</p>',NULL,1,'2019-05-20 02:43:48','2019-05-20 02:43:48'),(2,'Legal Notice','legal-notice','<h2 style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 20px; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 1px; font-size: 1.25rem;\">Legal</h2>\r\n<h3 style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 2px; font-size: 16px;\">Credits</h3>\r\n<p style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Concept and production:</p>\r\n<p style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">This Online store was created using&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #000000; text-decoration-line: none; touch-action: manipulation;\" href=\"http://www.prestashop.com/\">Prestashop Shopping Cart Software</a>,check out PrestaShop\'s&nbsp;<a style=\"box-sizing: inherit; background-color: transparent; color: #000000; text-decoration-line: none; touch-action: manipulation;\" href=\"http://www.prestashop.com/blog/en/\">ecommerce blog</a>&nbsp;for news and advices about selling online and running your ecommerce website.</p>',NULL,1,'2019-05-20 02:44:20','2019-05-20 02:44:20'),(3,'Terms And Conditions Of Use','terms-and-conditions-of-use','<h1 class=\"page-heading\" style=\"box-sizing: inherit; font-size: 18px; margin: 0px 0px 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 1px;\">Terms and conditions of use</h1>\r\n<h3 class=\"page-subheading\" style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 2px; font-size: 16px;\">Rule 1</h3>\r\n<p class=\"bottom-indent\" style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<h3 class=\"page-subheading\" style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 2px; font-size: 16px;\">Rule 2</h3>\r\n<p class=\"bottom-indent\" style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>\r\n<h3 class=\"page-subheading\" style=\"box-sizing: inherit; margin-top: 0px; margin-bottom: 0.5rem; font-family: Poppins-Regular; font-weight: normal; line-height: 1.1; color: #414141; letter-spacing: 2px; font-size: 16px;\">Rule 3</h3>\r\n<p class=\"bottom-indent\" style=\"box-sizing: inherit; margin-top: 1rem; margin-bottom: 1rem; color: #000000; font-family: Poppins-Regular; font-size: 16px;\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>',NULL,1,'2019-05-20 02:44:39','2019-05-20 02:44:39');
/*!40000 ALTER TABLE `my_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_informations`
--

DROP TABLE IF EXISTS `order_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `order_informations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `discount_id` int(10) unsigned DEFAULT NULL,
  `postal_code` int(7) DEFAULT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT 'bank',
  `delivery_fee` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `coupon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` double NOT NULL DEFAULT '0',
  `sub_total` double DEFAULT '0',
  `total` double DEFAULT '0',
  `status` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_informations`
--

LOCK TABLES `order_informations` WRITE;
/*!40000 ALTER TABLE `order_informations` DISABLE KEYS */;
INSERT INTO `order_informations` VALUES (16,'Hoang Nguyen','nghoang2013@gmail.com','0799446171','nghoang2013@gmail.com','test 2',NULL,70000,'Ho Chi Minh','bank',0,0,NULL,0,100,100,'pending','2019-05-26 06:50:00','2019-05-26 00:39:57');
/*!40000 ALTER TABLE `order_informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_detail_id` bigint(20) unsigned DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `order_information_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (29,141,100,1,16,'2019-05-25 23:31:33','2019-05-25 23:31:33');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2019-04-08 07:54:12','2019-04-08 07:54:12');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(2,1),(3,1),(4,1),(4,2),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(16,2),(17,1),(18,1),(18,2),(19,1),(19,2),(20,1),(20,2),(21,1),(21,2),(22,1),(23,1),(23,2),(24,1),(24,2),(25,1),(25,2),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(42,2),(43,1),(44,1),(44,2),(45,1),(45,2),(46,1),(46,2),(47,1),(47,2),(48,1),(49,1),(49,2),(50,1),(50,2),(51,1),(51,2),(52,1),(52,2),(53,1),(54,1),(54,2),(55,1),(55,2),(56,1),(56,2),(57,1),(57,2),(58,1),(59,1),(59,2),(60,1),(60,2),(61,1),(61,2),(62,1),(62,2),(63,1),(64,1),(64,2),(65,1),(65,2),(66,1),(66,2),(72,1),(72,2),(73,1),(74,1),(74,2),(75,1),(75,2),(76,1),(76,2),(82,1),(82,2),(83,1),(84,1),(85,1),(85,2),(86,1),(86,2),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(122,2),(123,1),(124,1),(124,2),(125,1),(125,2),(126,1),(126,2),(127,1),(127,2),(128,1),(129,1),(129,2),(130,1),(130,2),(131,1),(131,2),(132,1),(132,2),(133,1),(134,1),(134,2),(135,1),(135,2),(136,1),(136,2),(137,1),(137,2),(138,1),(139,1),(139,2),(140,1),(140,2),(141,1),(141,2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(2,'browse_bread',NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(3,'browse_database',NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(4,'browse_media',NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(5,'browse_compass',NULL,'2019-04-08 07:54:11','2019-04-08 07:54:11'),(6,'browse_menus','menus','2019-04-08 07:54:11','2019-04-08 07:54:11'),(7,'read_menus','menus','2019-04-08 07:54:11','2019-04-08 07:54:11'),(8,'edit_menus','menus','2019-04-08 07:54:11','2019-04-08 07:54:11'),(9,'add_menus','menus','2019-04-08 07:54:11','2019-04-08 07:54:11'),(10,'delete_menus','menus','2019-04-08 07:54:11','2019-04-08 07:54:11'),(11,'browse_roles','roles','2019-04-08 07:54:11','2019-04-08 07:54:11'),(12,'read_roles','roles','2019-04-08 07:54:11','2019-04-08 07:54:11'),(13,'edit_roles','roles','2019-04-08 07:54:11','2019-04-08 07:54:11'),(14,'add_roles','roles','2019-04-08 07:54:11','2019-04-08 07:54:11'),(15,'delete_roles','roles','2019-04-08 07:54:11','2019-04-08 07:54:11'),(16,'browse_users','users','2019-04-08 07:54:11','2019-04-08 07:54:11'),(17,'read_users','users','2019-04-08 07:54:11','2019-04-08 07:54:11'),(18,'edit_users','users','2019-04-08 07:54:11','2019-04-08 07:54:11'),(19,'add_users','users','2019-04-08 07:54:11','2019-04-08 07:54:11'),(20,'delete_users','users','2019-04-08 07:54:11','2019-04-08 07:54:11'),(21,'browse_settings','settings','2019-04-08 07:54:11','2019-04-08 07:54:11'),(22,'read_settings','settings','2019-04-08 07:54:11','2019-04-08 07:54:11'),(23,'edit_settings','settings','2019-04-08 07:54:11','2019-04-08 07:54:11'),(24,'add_settings','settings','2019-04-08 07:54:11','2019-04-08 07:54:11'),(25,'delete_settings','settings','2019-04-08 07:54:11','2019-04-08 07:54:11'),(26,'browse_categories','categories','2019-04-08 07:54:11','2019-04-08 07:54:11'),(27,'read_categories','categories','2019-04-08 07:54:11','2019-04-08 07:54:11'),(28,'edit_categories','categories','2019-04-08 07:54:11','2019-04-08 07:54:11'),(29,'add_categories','categories','2019-04-08 07:54:11','2019-04-08 07:54:11'),(30,'delete_categories','categories','2019-04-08 07:54:11','2019-04-08 07:54:11'),(31,'browse_posts','posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(32,'read_posts','posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(33,'edit_posts','posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(34,'add_posts','posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(35,'delete_posts','posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(36,'browse_pages','pages','2019-04-08 07:54:12','2019-04-08 07:54:12'),(37,'read_pages','pages','2019-04-08 07:54:12','2019-04-08 07:54:12'),(38,'edit_pages','pages','2019-04-08 07:54:12','2019-04-08 07:54:12'),(39,'add_pages','pages','2019-04-08 07:54:12','2019-04-08 07:54:12'),(40,'delete_pages','pages','2019-04-08 07:54:12','2019-04-08 07:54:12'),(41,'browse_hooks',NULL,'2019-04-08 07:54:12','2019-04-08 07:54:12'),(42,'browse_colors','colors','2019-04-12 23:58:05','2019-04-12 23:58:05'),(43,'read_colors','colors','2019-04-12 23:58:05','2019-04-12 23:58:05'),(44,'edit_colors','colors','2019-04-12 23:58:05','2019-04-12 23:58:05'),(45,'add_colors','colors','2019-04-12 23:58:05','2019-04-12 23:58:05'),(46,'delete_colors','colors','2019-04-12 23:58:05','2019-04-12 23:58:05'),(47,'browse_units','units','2019-04-12 23:59:28','2019-04-12 23:59:28'),(48,'read_units','units','2019-04-12 23:59:28','2019-04-12 23:59:28'),(49,'edit_units','units','2019-04-12 23:59:28','2019-04-12 23:59:28'),(50,'add_units','units','2019-04-12 23:59:28','2019-04-12 23:59:28'),(51,'delete_units','units','2019-04-12 23:59:28','2019-04-12 23:59:28'),(52,'browse_sizes','sizes','2019-04-13 00:01:59','2019-04-13 00:01:59'),(53,'read_sizes','sizes','2019-04-13 00:01:59','2019-04-13 00:01:59'),(54,'edit_sizes','sizes','2019-04-13 00:01:59','2019-04-13 00:01:59'),(55,'add_sizes','sizes','2019-04-13 00:01:59','2019-04-13 00:01:59'),(56,'delete_sizes','sizes','2019-04-13 00:01:59','2019-04-13 00:01:59'),(57,'browse_warranties','warranties','2019-04-13 00:03:45','2019-04-13 00:03:45'),(58,'read_warranties','warranties','2019-04-13 00:03:45','2019-04-13 00:03:45'),(59,'edit_warranties','warranties','2019-04-13 00:03:45','2019-04-13 00:03:45'),(60,'add_warranties','warranties','2019-04-13 00:03:45','2019-04-13 00:03:45'),(61,'delete_warranties','warranties','2019-04-13 00:03:45','2019-04-13 00:03:45'),(62,'browse_products','products','2019-04-13 00:09:34','2019-04-13 00:09:34'),(63,'read_products','products','2019-04-13 00:09:34','2019-04-13 00:09:34'),(64,'edit_products','products','2019-04-13 00:09:34','2019-04-13 00:09:34'),(65,'add_products','products','2019-04-13 00:09:34','2019-04-13 00:09:34'),(66,'delete_products','products','2019-04-13 00:09:34','2019-04-13 00:09:34'),(72,'browse_product_categories','product_categories','2019-04-13 01:10:13','2019-04-13 01:10:13'),(73,'read_product_categories','product_categories','2019-04-13 01:10:13','2019-04-13 01:10:13'),(74,'edit_product_categories','product_categories','2019-04-13 01:10:13','2019-04-13 01:10:13'),(75,'add_product_categories','product_categories','2019-04-13 01:10:13','2019-04-13 01:10:13'),(76,'delete_product_categories','product_categories','2019-04-13 01:10:13','2019-04-13 01:10:13'),(82,'browse_product_inputs','product_inputs','2019-04-15 08:25:00','2019-04-15 08:25:00'),(83,'read_product_inputs','product_inputs','2019-04-15 08:25:00','2019-04-15 08:25:00'),(84,'edit_product_inputs','product_inputs','2019-04-15 08:25:00','2019-04-15 08:25:00'),(85,'add_product_inputs','product_inputs','2019-04-15 08:25:00','2019-04-15 08:25:00'),(86,'delete_product_inputs','product_inputs','2019-04-15 08:25:00','2019-04-15 08:25:00'),(97,'browse_email_templates','email_templates','2019-05-06 21:48:17','2019-05-06 21:48:17'),(98,'read_email_templates','email_templates','2019-05-06 21:48:17','2019-05-06 21:48:17'),(99,'edit_email_templates','email_templates','2019-05-06 21:48:17','2019-05-06 21:48:17'),(100,'add_email_templates','email_templates','2019-05-06 21:48:17','2019-05-06 21:48:17'),(101,'delete_email_templates','email_templates','2019-05-06 21:48:17','2019-05-06 21:48:17'),(102,'browse_carousels','carousels','2019-05-07 08:13:49','2019-05-07 08:13:49'),(103,'read_carousels','carousels','2019-05-07 08:13:49','2019-05-07 08:13:49'),(104,'edit_carousels','carousels','2019-05-07 08:13:49','2019-05-07 08:13:49'),(105,'add_carousels','carousels','2019-05-07 08:13:49','2019-05-07 08:13:49'),(106,'delete_carousels','carousels','2019-05-07 08:13:49','2019-05-07 08:13:49'),(117,'browse_my_companies','my_companies','2019-05-20 02:38:41','2019-05-20 02:38:41'),(118,'read_my_companies','my_companies','2019-05-20 02:38:41','2019-05-20 02:38:41'),(119,'edit_my_companies','my_companies','2019-05-20 02:38:41','2019-05-20 02:38:41'),(120,'add_my_companies','my_companies','2019-05-20 02:38:41','2019-05-20 02:38:41'),(121,'delete_my_companies','my_companies','2019-05-20 02:38:41','2019-05-20 02:38:41'),(122,'browse_contacts','contacts','2019-05-20 03:06:53','2019-05-20 03:06:53'),(123,'read_contacts','contacts','2019-05-20 03:06:53','2019-05-20 03:06:53'),(124,'edit_contacts','contacts','2019-05-20 03:06:53','2019-05-20 03:06:53'),(125,'add_contacts','contacts','2019-05-20 03:06:53','2019-05-20 03:06:53'),(126,'delete_contacts','contacts','2019-05-20 03:06:53','2019-05-20 03:06:53'),(127,'browse_customer_messages','customer_messages','2019-05-20 03:25:38','2019-05-20 03:25:38'),(128,'read_customer_messages','customer_messages','2019-05-20 03:25:38','2019-05-20 03:25:38'),(129,'edit_customer_messages','customer_messages','2019-05-20 03:25:38','2019-05-20 03:25:38'),(130,'add_customer_messages','customer_messages','2019-05-20 03:25:38','2019-05-20 03:25:38'),(131,'delete_customer_messages','customer_messages','2019-05-20 03:25:38','2019-05-20 03:25:38'),(132,'browse_coupons','coupons','2019-05-20 06:25:12','2019-05-20 06:25:12'),(133,'read_coupons','coupons','2019-05-20 06:25:12','2019-05-20 06:25:12'),(134,'edit_coupons','coupons','2019-05-20 06:25:12','2019-05-20 06:25:12'),(135,'add_coupons','coupons','2019-05-20 06:25:12','2019-05-20 06:25:12'),(136,'delete_coupons','coupons','2019-05-20 06:25:12','2019-05-20 06:25:12'),(137,'browse_order_informations','order_informations','2019-05-20 11:33:19','2019-05-20 11:33:19'),(138,'read_order_informations','order_informations','2019-05-20 11:33:19','2019-05-20 11:33:19'),(139,'edit_order_informations','order_informations','2019-05-20 11:33:19','2019-05-20 11:33:19'),(140,'add_order_informations','order_informations','2019-05-20 11:33:19','2019-05-20 11:33:19'),(141,'delete_order_informations','order_informations','2019-05-20 11:33:19','2019-05-20 11:33:19');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,NULL,'VIDERER VOLUPTATUM TE EUM','VIDERER VOLUPTATUM TE EUM','Ei has mutat solum. Fugit atomorum efficiantur an vim, te mea diceret democritum referrentur, et altera...','<div id=\"lipsum\" class=\"articleContent\" style=\"box-sizing: inherit; margin: 0px 0px 10px; width: 1417.5px; color: #414141; font-family: Poppins-Regular; font-size: 16px;\">&nbsp;</div>\r\n<div class=\"sdsarticle-des\" style=\"box-sizing: inherit; margin: 15px 0px; text-align: justify; line-height: 24px; color: #414141; font-family: Poppins-Regular; font-size: 16px;\">Ei has mutat solum. Fugit atomorum efficiantur an vim, te mea diceret democritum referrentur, et altera aliquid mea. Sed illud dictas placerat ex, vel ea nihil recusabo consectetuer. Est et utamur similique, pro repudiare gubergren in.</div>','posts/May2019/5sYxYmF1OdHEwK3LPdsJ.jpg','viderer-voluptatum-te-eum','Ei has mutat solum. Fugit atomorum efficiantur an vim, te mea diceret democritum referrentur, et altera...',NULL,'PUBLISHED',0,'2019-05-18 00:57:03','2019-05-18 08:20:39'),(2,1,NULL,'Lorem ipsum dolor',NULL,'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni deserunt recusandae pariatur veniam impedit accusamus vero suscipit, labore nisi praesentium quaerat laborum molestiae veritatis corrupti nam perspiciatis','<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni deserunt recusandae pariatur veniam impedit accusamus vero suscipit, labore nisi praesentium quaerat laborum molestiae veritatis corrupti nam perspiciatis, minus aspernatur. Veritatis.</p>','posts/May2019/iv9SyGVCmyRFMxgusYjq.jpg','lorem-ipsum-dolor',NULL,NULL,'PUBLISHED',0,'2019-05-18 03:35:58','2019-05-18 03:35:58');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_categories_slug_unique` (`slug`),
  KEY `product_categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `product_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `product_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_categories`
--

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` VALUES (1,'Door locks','door-locks',NULL,NULL,NULL,NULL,'Door lock','Door lock 5Asystem','Khoá cửa','2019-04-13 02:25:48','2019-05-20 08:51:30'),(2,'Camera','camera',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Lock 1','lock-1',1,NULL,NULL,NULL,'Lock 1',NULL,NULL,'2019-04-13 03:08:34','2019-05-20 08:51:00'),(4,'Camera giám sát','camera-giam-sat',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Nikon','nikon',2,NULL,NULL,NULL,'Nikon',NULL,NULL,'2019-05-07 10:44:56','2019-05-07 10:44:56'),(6,'Lock 2','lock-2',1,NULL,'product-categories/May2019/social.jpg',NULL,'Lock 2',NULL,NULL,'2019-05-15 08:32:03','2019-05-20 08:51:09'),(7,'Sub smart lock 2','sub-smart-lock-2',6,NULL,NULL,NULL,'Sub smart lock 2',NULL,NULL,'2019-05-15 10:54:41','2019-05-20 09:00:12'),(8,'Sub lock 1','sub-lock-1',3,NULL,NULL,NULL,'Sub lock 1',NULL,NULL,'2019-05-15 20:49:37','2019-05-20 08:59:05'),(9,'Sub lock 2','sub-lock-2',3,NULL,NULL,NULL,'Sub lock 2',NULL,NULL,'2019-05-15 21:20:22','2019-05-20 08:59:17');
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_details`
--

DROP TABLE IF EXISTS `product_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `color_id` int(10) unsigned NOT NULL,
  `size_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned DEFAULT NULL,
  `warranty` int(10) unsigned NOT NULL DEFAULT '12',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `group_key` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_details_warranty_id_foreign` (`warranty`),
  KEY `product_details_color_id_foreign` (`color_id`),
  KEY `product_details_product_id_foreign` (`product_id`),
  KEY `product_details_size_id_foreign` (`size_id`),
  KEY `product_details_unit_id_foreign` (`unit_id`),
  CONSTRAINT `product_details_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `colors` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_details_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_details_size_id_foreign` FOREIGN KEY (`size_id`) REFERENCES `sizes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_details_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_details`
--

LOCK TABLES `product_details` WRITE;
/*!40000 ALTER TABLE `product_details` DISABLE KEYS */;
INSERT INTO `product_details` VALUES (136,1,3,3,NULL,12,12,200.00,NULL,'[\"products\\/May2019\\/10632749868_2018156771.jpg\",\"products\\/May2019\\/ava2-medium3.jpg\"]',0,'2019-05-19 13:00:16','2019-05-21 09:17:27'),(137,2,1,1,NULL,12,12,2.00,NULL,'[\"products\\/May2019\\/ava1111.jpg\",\"products\\/May2019\\/g1F8iJbTaYCH4kacfD9o.jpg\"]',0,'2019-05-19 13:02:57','2019-05-25 23:19:20'),(139,4,1,2,NULL,12,12,100.00,NULL,'[\"products\\/May2019\\/22.jpg\",\"products\\/May2019\\/wNEDBzChD63et5CTeoig.jpg\"]',0,'2019-05-19 13:08:19','2019-05-19 13:08:19'),(140,5,2,2,NULL,12,12,5.00,NULL,'[\"products\\/May2019\\/social1.jpg\"]',0,'2019-05-19 13:26:39','2019-05-25 23:14:00'),(141,7,3,3,NULL,12,12,100.00,NULL,'[\"products\\/May2019\\/WeChat-Image_2018120614425831.jpg\"]',0,'2019-05-19 13:29:32','2019-05-25 23:31:33'),(142,7,2,2,NULL,12,12,105.00,NULL,'[\"products\\/May2019\\/banner-camera-ip-d3-1024x5761.jpg\"]',1,'2019-05-19 13:29:32','2019-05-19 13:29:32'),(144,8,2,2,NULL,12,12,6.00,NULL,'[\"products\\/May2019\\/cover-template-1.JPG\",\"products\\/May2019\\/cover-template-2.JPG\",\"products\\/May2019\\/template-11.jpg\"]',0,'2019-05-19 13:36:29','2019-05-25 23:21:55'),(146,9,2,2,NULL,12,8,300.00,NULL,'[\"products\\/May2019\\/3-101.jpg\"]',0,'2019-05-19 13:41:57','2019-05-25 23:21:55');
/*!40000 ALTER TABLE `product_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_inputs`
--

DROP TABLE IF EXISTS `product_inputs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `product_inputs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_detail_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_inputs`
--

LOCK TABLES `product_inputs` WRITE;
/*!40000 ALTER TABLE `product_inputs` DISABLE KEYS */;
INSERT INTO `product_inputs` VALUES (2,137,2,'2019-05-25 21:09:50','2019-05-25 21:09:50'),(3,137,2,'2019-05-25 21:10:53','2019-05-25 21:10:53'),(4,141,3,'2019-05-25 21:11:01','2019-05-25 21:11:01'),(5,140,100,'2019-05-25 21:11:13','2019-05-25 21:11:13');
/*!40000 ALTER TABLE `product_inputs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) DEFAULT NULL,
  `seo_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_fb` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(4) DEFAULT '0',
  `is_popular` tinyint(4) DEFAULT '0',
  `is_bestseller` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `product_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'KHÓA VÂN TAY THẺ TỪ MÃ SỐ 5ASYSTEMS TS 2000 PLUS','khoa-van-tay-the-tu-ma-so-5asystems-ts-2000-plus',1,'- Chất liệu: Làm bằng nhôm siêu nhẹ\r\n- Màu sắc: Đen\r\n- Mở cửa: Vân tay, mật khẩu, thẻ từ, chìa khóa.','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; font-weight: bold; outline: 0px !important;\">Kh&oacute;a V&acirc;n Tay TS 2000 PLUS Chất Liệu Nh&ocirc;m Si&ecirc;u Nhẹ Si&ecirc;u Bền</span><span style=\"box-sizing: border-box; outline: 0px !important;\">&nbsp;chất liệu nh&ocirc;m từ kh&oacute;a TS 2000 PLUS được d&ugrave;ng nhiều trong sản xuất m&aacute;y bay, n&ecirc;n kết cấu chất liệu v&ocirc; c&ugrave;ng chắc chắn v&agrave; nhẹ kh&oacute;a v&acirc;n tay TS 2000 PLUS lu&ocirc;n bền đẹp bất chấp với mọi điều kiện thay đổi của thời tiết bởi c&oacute; t&iacute;nh chất chống nhiệt chống nước. Trong đ&oacute; c&oacute; khả năng chống nước hơn c&aacute;c loại kh&oacute;a th&ocirc;ng thường. kh&oacute;a TS2000 PLUS c&oacute; độ bền rất cao do vật liệu n&agrave;y kh&ocirc;ng bị oxy ho&aacute;, kh&ocirc;ng bị l&atilde;o ho&aacute; hay v&agrave;ng trong điều kiện bức xạ mặt trời v&agrave; mưa axit. Với thiết kế nhỏ gọn nhứ kh&oacute;a TS 2000 PLUS th&igrave; việc lắp đặt thay kh&oacute;a cũ trở nhanh ch&oacute;ng v&agrave; dễ d&agrave;ng, kh&ocirc;ng như c&aacute;c loại kh&oacute;a điện tử rườm r&agrave; kh&oacute; lắp đặt tr&ecirc;n thị trường hiện nay. Với vật liệu l&agrave;m từ nh&ocirc;m, với c&aacute;c chi tiết g&oacute;c c&aacute;ch được thiết kế v&ocirc; c&ugrave;ng tinh xảo với hai m&agrave;u đen v&agrave; bạc, m&agrave;n h&igrave;nh cảm ứng, được t&iacute;ch hợp đồng hồ tạo n&ecirc;n kh&ocirc;ng gian sang trọng v&agrave; cao cấp cho ng&ocirc;i nh&agrave; của bạn Cuối C&ugrave;ng t&iacute;nh năng đặc biệt của kh&oacute;a TS 2000 PLUS l&agrave; c&oacute; thể xem được lịch sử ra v&agrave;o theo ng&agrave;y giờ, năm th&aacute;ng gi&uacute;p đỡ tốt cho người d&ugrave;ng c&oacute; thể kiểm so&aacute;t ra v&agrave;o.&nbsp;</span><span style=\"box-sizing: border-box; font-weight: bold; outline: 0px !important;\">Những t&iacute;nh năng ch&iacute;nh của kh&oacute;a</span><span style=\"box-sizing: border-box; outline: 0px !important;\">&nbsp;- 200 thẻ từ - 200 v&acirc;n tay - 200 m&atilde; số - 2 ch&igrave;a kh&oacute;a cơ - Theo d&otilde;i lịch sử ra v&agrave;o - Chốt cửa an to&agrave;n&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10632764593_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10632749868_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10632782158_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10606879831_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10632746904_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10606915152_2018156771.jpg\" /></span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important;\" src=\"https://dinhcaocongnghe.com/storage/products/May2019/10606912177_2018156771.jpg\" /></span></p>',1,NULL,'KHÓA VÂN TAY THẺ TỪ MÃ SỐ 5ASYSTEMS TS 2000 PLUS',NULL,NULL,'products/May2019/ava2-medium.jpg','products/May2019/img-fb.jpg',1,0,1,'2019-04-12 20:47:53','2019-05-19 12:56:44'),(2,'KHÓA VÂN TAY THẺ TỪ MÃ SỐ ĐẲNG CẤP SK6000','khoa-van-tay-the-tu-ma-so-dang-cap-sk6000',3,'Với thiết kế vô cùng chắc chắn SK 6000 được mệnh danh là cổ xe bọc giáp vì lõi khóa được bảo vệ bằng một hợp kim bên ngoài phủ thêm 1 lớp nhôm cao cấp, Loại nhôm siêu nhẹ dùng để chế tạo máy bay, chống lực và tác động cực mạnh, ngoài ra khóa sẽ báo động khi chịu lực, tác động mạnh hoặc nhập mật khẩu sai nhiều lần. Khóa SK 6000 bảo vệ gia chủ tuyệt đối','<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\"><span style=\"box-sizing: border-box; font-weight: bold; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">KH&Oacute;A V&Acirc;N TAY ĐỈNH CAO NHẤT CỦA 5ASYSTEMS USA SK6000</span></span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">&nbsp;</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Với thiết kế v&ocirc; c&ugrave;ng chắc chắn &nbsp;SK 6000 được mệnh danh l&agrave; cổ xe bọc gi&aacute;p v&igrave; l&otilde;i kh&oacute;a được bảo vệ bằng một &nbsp;hợp kim b&ecirc;n ngo&agrave;i phủ th&ecirc;m 1 lớp &nbsp;nh&ocirc;m &nbsp;cao cấp, Loại nh&ocirc;m si&ecirc;u nhẹ d&ugrave;ng để chế tạo m&aacute;y bay, chống lực v&agrave; t&aacute;c động cực mạnh, ngo&agrave;i ra kh&oacute;a sẽ b&aacute;o động khi chịu lực, t&aacute;c động mạnh hoặc nhập mật khẩu sai nhiều lần. Kh&oacute;a SK 6000 bảo vệ gia chủ tuyệt đối</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Đ&oacute; l&agrave; độ bảo vệ tuyệt đối của kh&oacute;a, c&ograve;n về thiết kế b&ecirc;n ngo&agrave;i của SK 6000 qua c&aacute;c đường thẳng tới độ cong được thiết kế v&ocirc; c&ugrave;ng tinh xảo, đường n&eacute;t của kh&oacute;a tạo n&ecirc;n sự tinh tế v&agrave; h&agrave;i h&ograve;a trong kh&ocirc;ng gian gia chủ với tay cầm được t&iacute;ch hợp mở kh&oacute;a v&acirc;n tay v&ocirc; c&ugrave;ng tiện dụng, m&agrave;n h&igrave;nh cảm ứng tr&ecirc;n kh&oacute;a tạo n&ecirc;n sự sang trọng bắt kịp xu hướng c&ocirc;ng nghệ 4.0</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Với vẻ ngo&agrave;i cứng c&aacute;p, thiết kế tinh xảo th&igrave; chức năng của SK6000 liệu c&oacute; tương xứng với vẻ bề ngo&agrave;i của n&oacute;, c&acirc;u trả lời l&agrave; c&oacute; ! với 4 chức năng v&ocirc; c&ugrave;ng th&ocirc;ng minh mang tới sự tiện lợi cho gia chủ</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Mở kh&oacute;a bằng v&acirc;n tay t&iacute;ch hợp tr&ecirc;n tay nắm ( lưu được tối đa 200 v&acirc;n tay )</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Mở kh&oacute;a bằng mật khẩu danh cho những người mở kh&oacute;a kh&oacute; bằng v&acirc;n tay ( lưu được tối đa 200 m&atilde; số)</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Mở kh&oacute;a bằng thẻ từ tiện dụng trong việc quản l&yacute; , kiểm so&aacute;t ra v&agrave;o cho kh&aacute;ch của gia chủ ( 200 thẻ từ)</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">Ch&igrave;a kh&oacute;a cơ: 2 ch&igrave;a chống sao ch&eacute;p mọi h&igrave;nh thức</span><br style=\"box-sizing: inherit; outline: 0px !important; font-family: Tahoma; font-size: 13px; color: #222222;\" /><span style=\"box-sizing: border-box; outline: 0px !important; color: #222222; font-family: Tahoma; font-size: 13px;\">SK6000 chuy&ecirc;n dụng cho tất cả c&aacute;c d&ograve;ng cửa gỗ thịnh h&agrave;nh nhất hiện nay. Với tốc độ mở cửa cựa nhanh, độ nhạy cảm ứng v&acirc;n tay ch&iacute;nh x&aacute;c 100%.</span></p>\r\n<ol style=\"box-sizing: inherit; margin: 0px 0px 0px 1.4rem; font-family: Tahoma; font-size: 1rem; padding: 0px; line-height: 1.6; list-style-position: outside; color: #222222; outline: 0px !important;\">\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\" value=\"2\"><span style=\"box-sizing: inherit; outline: 0px !important; font-weight: bold; line-height: inherit;\">CƠ CHẾ HOẠT ĐỘNG</span></li>\r\n</ol>\r\n<ul style=\"box-sizing: inherit; margin: 0px 0px 0px 1.1rem; font-family: Tahoma; font-size: 1rem; padding: 0px; line-height: 1.6; list-style-position: outside; color: #222222; outline: 0px !important;\">\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">Sử dụng&nbsp; pin Sac 5400 MAH , thời gian hoạt động l&ecirc;n đến 12&nbsp;th&aacute;ng.</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">Cổng kết nối micro usb: n&acirc;ng cấp phi&ecirc;n bản v&agrave; ph&ograve;ng hờ bất trắc khi bạn qu&ecirc;n thay pin cho kho&aacute; SK6000</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">Cảnh b&aacute;o bằng giọng n&oacute;i: khi pin yếu, khi c&oacute; người mở kho&aacute; kh&ocirc;ng đ&uacute;ng.</li>\r\n</ul>\r\n<ol style=\"box-sizing: inherit; margin: 0px 0px 0px 1.4rem; font-family: Tahoma; font-size: 1rem; padding: 0px; line-height: 1.6; list-style-position: outside; color: #222222; outline: 0px !important;\">\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\" value=\"3\"><span style=\"box-sizing: inherit; outline: 0px !important; font-weight: bold; line-height: inherit;\">ĐỈNH CAO C&Ocirc;NG NGHỆ CAM KẾT SẼ MANG ĐẾN CHO BẠN</span></li>\r\n</ol>\r\n<ul style=\"box-sizing: inherit; margin: 0px 0px 0px 1.1rem; font-family: Tahoma; font-size: 1rem; padding: 0px; line-height: 1.6; list-style-position: outside; color: #222222; outline: 0px !important;\">\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">SẢN PHẨM CHẤT LƯỢNG</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">GI&Aacute; CẢ HỢP L&Yacute;</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">GIẢI PH&Aacute;P HIỆU QUẢ TỐI ƯU</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">DỊCH VỤ BẢO H&Agrave;NH TỐT NHẤT, Đ&Aacute;P ỨNG Y&Ecirc;U CẦU CỦA MỌI ĐỐI TƯỢNG</li>\r\n<li style=\"box-sizing: inherit; outline: 0px !important; font-size: 13px; margin: 0px; padding: 0px;\">C&Ugrave;NG VỚI PHONG C&Aacute;CH L&Agrave;M VIỆC CHUY&Ecirc;N NGHIỆP</li>\r\n</ul>',1,NULL,'KHÓA VÂN TAY THẺ TỪ MÃ SỐ ĐẲNG CẤP SK6000',NULL,NULL,'products/May2019/ava111.jpg','products/May2019/img-fb2.jpg',1,1,0,'2019-04-15 01:54:20','2019-05-19 13:02:52'),(4,'KHÓA CỬA VÂN TAY 5ASYSTEMS TS 7000','khoa-cua-van-tay-5asystems-ts-7000',6,'- Chất liệu: Làm bằng hợp kim nguyên khối.\r\n- Màu sắc: Màu đồng\r\n- Mở cửa: Vân tay, mật khẩu, thẻ từ, chìa khóa.','<p>Product 3</p>',1,NULL,'KHÓA CỬA VÂN TAY 5ASYSTEMS TS 7000',NULL,NULL,'products/May2019/wNEDBzChD63et5CTeoig1.jpg','products/May2019/img-fb3.jpg',1,0,1,'2019-04-15 02:00:05','2019-05-19 13:08:19'),(5,'Bộ kit camera 4 KÊNH NVR GDX-TSK580','bo-kit-camera-4-kenh-nvr-gdx-tsk580',9,'Nếu bạn đang tìm kiếm một chiếc Camera lắp đặt đơn giản – hình ảnh rõ nét chân thực – đảm bảo tính thẩm mỹ – hiệu quả sử dụng cao…thì BỘ KIT 4 KÊNH NVR GDX-TSK580 PRO thực sự là sản phẩm đáp ứng được đầy đủ các tiêu chí mà bạn đưa ra. Thao tác cài đặt hết sức đơn giản, kiểu dáng nhỏ gọn, đẹp và hiện đại, hoạt động bền bỉ.','<h3 style=\"box-sizing: border-box; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; font-weight: 500; color: #222222; margin: 0px; font-size: 1.6875rem; text-align: center; line-height: 1.2 !important; outline: 0px !important;\">Bộ kit camera 4 K&Ecirc;NH NVR GDX-TSK580&nbsp; &nbsp;quan s&aacute;t ng&agrave;y đ&ecirc;m</h3>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1.6875rem; text-align: center; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important; font-size: 12pt;\">Bộ kit camera 4 k&ecirc;nh th&iacute;ch hợp lắp cả trong nh&agrave; v&agrave; ngo&agrave;i trời, ứng dụng quan s&aacute;t tại c&aacute;c biệt thự, hộ gia đ&igrave;nh, t&iacute;nh thẩm mỹ cao.</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\"><span style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\">Trọn bộ kit bao gồm</span>&nbsp;04 camera IP Wifi th&acirc;n trụ, 01 đầu ghi IP 4 k&ecirc;nh ,&nbsp;c&aacute;c phụ kiện đi k&egrave;m (nguồn đầu ghi, nguồn camera, d&acirc;y mạng, ăng-ten). . Trọn bộ đ&atilde; bao gồm c&ocirc;ng lắp đặt, c&agrave;i đặt xem qua mạng miễn ph&iacute; trọn đời&nbsp;<br style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\" /><br style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\" /><span style=\"box-sizing: border-box; outline: 0px !important; font-size: 18pt;\">Ưu điểm vượt trội của bộ kit camera Wifi 4 k&ecirc;nh&nbsp;</span></p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\">-&nbsp;<em style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\"><span style=\"box-sizing: border-box; outline: 0px !important;\">H&igrave;nh ảnh sắc n&eacute;t:</span></em>&nbsp;C&aacute;c camera IP đều sở hữu độ ph&acirc;n giải cao HD 1080P, cho chất lượng h&igrave;nh ảnh vượt trội.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\">-&nbsp;<em style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\"><span style=\"box-sizing: border-box; outline: 0px !important;\">Quan s&aacute;t ng&agrave;y đ&ecirc;m:</span></em>&nbsp;Trong điều kiện đủ &aacute;nh s&aacute;ng, camera thu về h&igrave;nh ảnh với m&agrave;u sắc sống động, thực. Trong điều kiện thiếu &aacute;nh s&aacute;ng, camera sử dụng đ&egrave;n hồng ngoại chiếu s&aacute;ng, quan s&aacute;t trong đ&ecirc;m, với tầm nh&igrave;n hồng ngoại 10 đến 30 m&eacute;t. Camera t&iacute;ch hợp 03 đ&egrave;n hồng ngoại, c&ocirc;ng nghệ Led Array cho h&igrave;nh ảnh đen trắng r&otilde; n&eacute;t, kh&ocirc;ng c&oacute; hiện tượng tập trung s&aacute;ng g&acirc;y ch&oacute;i, độ nhiễu thấp.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\">-&nbsp;<em style=\"box-sizing: border-box; outline: 0px !important; font-size: 13px;\"><span style=\"box-sizing: border-box; outline: 0px !important;\">Kết nối kh&ocirc;ng d&acirc;y:</span></em>&nbsp;Bộ Kit &nbsp;t&iacute;ch hợp kết nối wifi (802.11 b/g/n) tr&ecirc;n cả camera v&agrave; đầu ghi, ngo&agrave;i ra c&oacute; t&iacute;ch hợp cổng mạng RJ 45 100/1000Mb. Chỉ cần kết nối camera với bộ nguồn để chạy camera, đồng thời camera ứng dụng c&ocirc;ng nghệ POE cho ph&eacute;p lấy nguồn trực tiếp tr&ecirc;n c&aacute;p mạng, hạn chế việc đi d&acirc;y.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\">T&iacute;n hiệu Wifi mạnh mẽ, khoảng c&aacute;ch truyền t&iacute;n hiệu tối đa cho cả 4 camera tới đầu ghi l&agrave; 30 m&eacute;t (xuy&ecirc;n tường), với 1 camera c&oacute; thể l&ecirc;n tới 100 m&eacute;t.</p>\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 1rem; font-family: Roboto, \'Helvetica Neue\', Arial, sans-serif; color: #222222; font-size: 1rem; text-align: center; outline: 0px !important;\"><img style=\"box-sizing: border-box; border: 0px none; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important; max-height: 100%; margin: 0px auto; display: block;\" src=\"https://media3.scdn.vn/img2/2018/7_31/QrUE70_simg_d0daf0_800x1200_max.jpg\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><img style=\"box-sizing: border-box; border: 0px none; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important; max-height: 100%; margin: 0px auto; display: block;\" src=\"https://media3.scdn.vn/img2/2018/7_31/qfkTqY_simg_d0daf0_800x1200_max.jpg\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><img style=\"box-sizing: border-box; border: 0px none; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important; max-height: 100%; margin: 0px auto; display: block;\" src=\"https://media3.scdn.vn/img2/2018/7_31/tL4ySi_simg_d0daf0_800x1200_max.jpg\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><br style=\"box-sizing: border-box; outline: 0px !important;\" /><img style=\"box-sizing: border-box; border: 0px none; vertical-align: middle; outline: 0px !important; max-width: 100%; height: auto !important; max-height: 100%; margin: 0px auto; display: block;\" src=\"https://media3.scdn.vn/img2/2018/7_31/IaHddD_simg_d0daf0_800x1200_max.jpg\" /></p>',1,NULL,'Bộ kit camera 4 KÊNH NVR GDX-TSK580',NULL,NULL,'products/May2019/camera-kit-4-kenh-nvr-gdx-tsk-580-full-hd-1080p.jpg','products/May2019/img-fb4.jpg',1,0,0,'2019-04-20 06:11:46','2019-05-19 13:26:39'),(7,'CAMERA IP OUTDOOR 5AD3','camera-ip-outdoor-5ad3',7,'Sử dụng công nghệ mới cho hình ảnh sắc nét\r\nĐộ phân giải: 2.0 Megapixel (Full HD)\r\nỐng kính: siêu rộng 2.8mm\r\nTầm quan sát hồng ngoại : 20 mét','<p><span style=\"color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\">&nbsp;Quan s&aacute;t ng&agrave;y đ&ecirc;m,hoạt động độc lập, hỗ trợ người d&ugrave;ng gi&aacute;m s&aacute;t từ xa với khả năng truyền h&igrave;nh ảnh chất lượng 2.0 Megapixel (Full HD), th&ocirc;ng qua App GDX-PRO tr&ecirc;n Smarphone, gi&uacute;p người d&ugrave;ng gi&aacute;m s&aacute;t nh&agrave; ri&ecirc;ng, cửa h&agrave;ng từ xa , khả chống nước, va đập cao v&igrave; c&oacute; lớp bảo vệ bằng hợp kim chắc chắn đặc biệt d&ograve;ng Camera 5A IP D3 n&agrave;y kh&ocirc;ng cần hệ thống đường d&acirc;y phức tạp như loại thường v&agrave; tiết kiệm chi ph&iacute; nhờ kh&ocirc;ng sử dụng đầu ghi h&igrave;nh, ổ cứng - Camera 5A IP D3 cắm l&agrave; chạy xem online ngay tại bất kỳ đ&acirc;u tr&ecirc;n thế giới, nhỏ gọn, tiện dụng, dễ lắp đặt v&agrave; sử dụng, Với bộ thu s&oacute;ng wifi c&ocirc;ng nghệ cao n&ecirc;n bắt s&oacute;ng cực kỳ tốt , tự động c&acirc;n bằng s&aacute;ng, quay h&igrave;nh ban đ&ecirc;m tốt. - Sản phẩm th&iacute;ch hợp cho hệ thống an ninh, ph&ograve;ng chống sự x&acirc;m nhập bất hợp ph&aacute;p của doanh nghiệp. Gia đ&igrave;nh lắp đặt camera để chống trộm, theo d&otilde;i trẻ nhỏ hay tr&ocirc;ng non người gi&agrave;. C&ocirc;ng ty, cửa h&agrave;ng c&oacute; thể sử dụng khi cần quan s&aacute;t nhằm quản l&yacute; nh&acirc;n vi&ecirc;n, xưởng sản xuất, gi&uacute;p quản l&yacute; hệ thống nh&acirc;n sự tại văn ph&ograve;ng dễ d&agrave;ng với chi ph&iacute; thấp nhất.&nbsp;</span></p>\r\n<p><img class=\"alignnone wp-image-2419 size-large\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/wp-content/uploads/2018/12/banner-camera-ip-d3-1024x576.jpg\" alt=\"\" width=\"1024\" height=\"576\" /><img class=\"alignnone wp-image-2422\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/wp-content/uploads/2018/12/WeChat-Image_20181206144258.jpg\" alt=\"\" width=\"1024\" height=\"1018\" /></p>',1,NULL,'CAMERA IP OUTDOOR 5AD3',NULL,NULL,'products/May2019/WeChat-Image_201812061442583.jpg','products/May2019/banner-camera-ip-d3-1024x576.jpg',1,1,1,'2019-05-05 00:48:34','2019-05-19 13:29:32'),(8,'5AIPESD6 PRO IP CAMERA WIRELESS','5aipesd6-pro-ip-camera-wireless',4,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Natus labore sequi obcaecati sit incidunt fugit quia alias libero ipsum deleniti unde, minus dolorum illum aliquid voluptas voluptatem. Fugit, itaque eius.','<p><span style=\"box-sizing: border-box; font-weight: bold; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\">Với c&ocirc;ng nghệ ng&agrave;y c&agrave;ng ph&aacute;t triển, D&ograve;ng camera ip của 5ASYSTEMS&nbsp;ng&agrave;y c&agrave;ng được n&acirc;ng cấp nhiều t&iacute;nh năng mới nhằm phục vụ cho nhu cầu của người sử dụng. Với khả năng t&iacute;ch hợp được nhiều t&iacute;nh năng độc đ&aacute;o, camera IP 5A lu&ocirc;n l&agrave; sự lựa chọn h&agrave;ng đầu cho c&aacute;c nh&agrave; sản xuất ph&aacute;t triển khả năng linh hoạt, tiện dụng &ndash; Thiết kế với vỏ bảo vệ ti&ecirc;u chuẩn chống thấm, ngăn mưa v&agrave; bụi, th&aacute;ch thức mọi điều kiện của khắc nghiệt của thời tiết. &ndash; T&iacute;ch hợp ghi h&igrave;nh cả ng&agrave;y lẫn đ&ecirc;m. Bao gồm 30 đ&egrave;n led cung cấp khả năng quan s&aacute;t khi &aacute;nh s&aacute;ng = 0, độ xa tối đa 25 m&eacute;t. - Ứng dụng :&nbsp;5AIPes&nbsp;mở rộng cho d&ograve;ng&nbsp;CAMERA &nbsp;OUTDOOR WIRELESS &nbsp;5AIPESD6 &nbsp;tối ưu hết tất cả , xem cực kỳ mượt với &nbsp;3G , độ trể h&igrave;nh ảnh được khắc phục ho&agrave;n to&agrave;n , kh&ocirc;ng như những d&ograve;ng camera ip kh&aacute;c.</span><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/camera-ip/out-dome.jpg\" alt=\"\" width=\"950\" height=\"950\" /><span style=\"box-sizing: border-box; font-weight: bold; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\">*&nbsp;Tất cả đều TỰ ĐỘNG &amp; MIỄN PH&Iacute;: &nbsp;Kh&ocirc;ng cần c&agrave;i đặt,&nbsp;Kh&ocirc;ng cần cấu h&igrave;nh,&nbsp;Kh&ocirc;ng cần t&ecirc;n miền , kh&ocirc;ng cần IP tĩnh* sử dụng c&ocirc;ng nghệ p2p smart streaming video (CẮM L&Agrave; CHẠY &amp; XEM NGAY TR&Ecirc;N ĐIỆN THOẠI TẠI BẤT K&Igrave; Đ&Acirc;U TR&Ecirc;N THẾ GIỚI.)* sử dụng wifi kết nối kh&ocirc;ng cần d&acirc;y</span><span style=\"color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\">&nbsp;</span><span style=\"box-sizing: border-box; font-weight: bold; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\">Sản phẩm Outdoor Wireless 5AIPESD6 (sau đ&acirc;y viết tắt l&agrave; D6 cho ngắn gọn) hiện được c&ocirc;ng ty Đỉnh Cao C&ocirc;ng Nghệ (dinhcaocongnghe.com) ph&acirc;n phối tại Việt Nam với gi&aacute; 1.680.000 triệu đồng . Sản phẩm được bảo h&agrave;nh ch&iacute;nh h&atilde;ng 1 đổi 1 trong v&ograve;ng 2 năm.</span><span style=\"color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;\">&nbsp;</span><span style=\"box-sizing: border-box; font-weight: bold; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\">Mở hộp</span><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/camera-ip/1572249.jpg\" alt=\"\" width=\"950\" height=\"596\" /><span style=\"box-sizing: border-box; font-weight: bold; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important;\">Chiếc hộp đựng D6 kh&aacute; nhỏ gọn được l&agrave;m bằng b&igrave;a carton, quanh hộp được in logo của h&atilde;ng 5A System. Cạnh b&ecirc;n tr&aacute;i hộp in model sản phẩm v&agrave; cạnh ngược lại in th&ocirc;ng tin nh&agrave; sản xuất.</span></p>',1,NULL,'5AIPESD6 PRO IP CAMERA WIRELESS',NULL,NULL,'products/May2019/IMG_1939.jpg','products/May2019/3.jpg',1,0,0,'2019-05-17 19:48:53','2019-05-19 13:36:29'),(9,'CAMERA IP 5A 4G LLS','camera-ip-5a-4g-lls',4,'- DÒNG CAMERA SỬ DỤNG SIM 3G/4G ĐẦU TIÊN\r\n- CÓ THỂ XÀI WIFI HOẶC MẠNG LAN KHI KHÔNG SỬ DỤNG SIM 3G/4G\r\n- ĐỘ PHÂN GIẢI 2 MEGAPIXEL FULL HD 1080P\r\n- ONVIB VỚI ĐẦU GHI HÌNH\r\n- TẦM XA HỒNG NGOẠI TRÊN 60M\r\n- CHỐNG NGƯỢC SÁNG , CHỐNG MƯA NẮNG , LẮP ĐĂT ĐƯỢC NƠI CÓ THỜI THIẾT XẤU NHẤT','<p><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/ptz-camera/4g-lss/t1.jpg\" alt=\"\" width=\"950\" height=\"393\" /><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/ptz-camera/4g-lss/3.jpg\" alt=\"\" width=\"951\" height=\"951\" /><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/ptz-camera/4g-lss/1.jpg\" alt=\"\" width=\"951\" height=\"713\" /><img class=\"aligncenter\" style=\"box-sizing: border-box; border: 0px; vertical-align: middle; max-width: 100%; color: #000000; font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; outline: 0px !important; height: auto !important;\" src=\"https://khoacuadangcap.com/storage/images/sanpham/ptz-camera/4g-lss/4.jpg\" alt=\"\" /></p>',1,NULL,'CAMERA IP 5A 4G LLS',NULL,NULL,'products/May2019/3-10.jpg','products/May2019/t1.jpg',0,0,1,'2019-05-19 13:40:40','2019-05-19 13:41:57');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-04-08 07:54:11','2019-04-08 07:54:11'),(2,'user','Normal User','2019-04-08 07:54:11','2019-04-08 07:54:11');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','5Asystem','','text',1,'Site'),(2,'site.description','Site Description','5Asystem website','','text',2,'Site'),(3,'site.logo','Site Logo','settings/May2019/vs3j1rRRXLGAUpCLMAUo.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','CMS','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to 5Asystems','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin'),(11,'smtp.host','Host','smtp.mailtrap.io',NULL,'text',6,'SMTP'),(12,'smtp.port','Port','465',NULL,'text',7,'SMTP'),(13,'smtp.username','Username','00dc995b858ca8',NULL,'text',8,'SMTP'),(14,'smtp.password','Password','a7b42ab510a1e0',NULL,'text',9,'SMTP'),(15,'smtp.default_email','Default email','default_email@gmail.com',NULL,'text',12,'SMTP'),(16,'smtp.notify_email','Notify email','nghoang2013@gmail.com',NULL,'text',13,'SMTP'),(17,'smtp.encryption','Encryption','ssl',NULL,'text',10,'SMTP'),(18,'smtp.name','Name','System',NULL,'text',11,'SMTP'),(19,'bank.owner','Account owner','Xtreme',NULL,'text',14,'Bank'),(20,'bank.name','Bank name','City bank',NULL,'text',15,'Bank'),(21,'bank.note','Message in transfer note',NULL,NULL,'text',16,'Bank'),(22,'social.fb','Facebook','https://www.facebook.com/',NULL,'text',17,'Social'),(23,'social.youtube','Youtube','https://www.youtube.com/',NULL,'text',18,'Social'),(25,'fedex.street_lines','StreetLines','320 Stanley Ave',NULL,'text',19,'FedEx'),(26,'fedex.city','City','Greenwood',NULL,'text',20,'FedEx'),(27,'fedex.state_or_province_code','State Or Province Code','SC',NULL,'text',21,'FedEx'),(28,'fedex.postal_code','Postal Code','29649',NULL,'text',22,'FedEx'),(29,'fedex.country_code','Country Code','US',NULL,'text',23,'FedEx');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingcart`
--

LOCK TABLES `shoppingcart` WRITE;
/*!40000 ALTER TABLE `shoppingcart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sizes`
--

LOCK TABLES `sizes` WRITE;
/*!40000 ALTER TABLE `sizes` DISABLE KEYS */;
INSERT INTO `sizes` VALUES (1,'S','2019-04-13 10:59:36','2019-04-13 10:59:36'),(2,'M','2019-04-13 10:59:41','2019-04-13 10:59:41'),(3,'L','2019-04-13 10:59:45','2019-04-13 10:59:45');
/*!40000 ALTER TABLE `sizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2019-04-08 07:54:12','2019-04-08 07:54:12'),(2,'data_types','display_name_singular',6,'pt','Página','2019-04-08 07:54:12','2019-04-08 07:54:12'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2019-04-08 07:54:12','2019-04-08 07:54:12'),(4,'data_types','display_name_singular',4,'pt','Categoria','2019-04-08 07:54:12','2019-04-08 07:54:12'),(5,'data_types','display_name_singular',2,'pt','Menu','2019-04-08 07:54:12','2019-04-08 07:54:12'),(6,'data_types','display_name_singular',3,'pt','Função','2019-04-08 07:54:12','2019-04-08 07:54:12'),(7,'data_types','display_name_plural',5,'pt','Posts','2019-04-08 07:54:12','2019-04-08 07:54:12'),(8,'data_types','display_name_plural',6,'pt','Páginas','2019-04-08 07:54:12','2019-04-08 07:54:12'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2019-04-08 07:54:12','2019-04-08 07:54:12'),(10,'data_types','display_name_plural',4,'pt','Categorias','2019-04-08 07:54:12','2019-04-08 07:54:12'),(11,'data_types','display_name_plural',2,'pt','Menus','2019-04-08 07:54:12','2019-04-08 07:54:12'),(12,'data_types','display_name_plural',3,'pt','Funções','2019-04-08 07:54:12','2019-04-08 07:54:12'),(13,'categories','slug',1,'pt','categoria-1','2019-04-08 07:54:12','2019-04-08 07:54:12'),(14,'categories','name',1,'pt','Categoria 1','2019-04-08 07:54:12','2019-04-08 07:54:12'),(15,'categories','slug',2,'pt','categoria-2','2019-04-08 07:54:12','2019-04-08 07:54:12'),(16,'categories','name',2,'pt','Categoria 2','2019-04-08 07:54:12','2019-04-08 07:54:12'),(17,'pages','title',1,'pt','Olá Mundo','2019-04-08 07:54:12','2019-04-08 07:54:12'),(18,'pages','slug',1,'pt','ola-mundo','2019-04-08 07:54:12','2019-04-08 07:54:12'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2019-04-08 07:54:12','2019-04-08 07:54:12'),(20,'menu_items','title',1,'pt','Painel de Controle','2019-04-08 07:54:12','2019-04-08 07:54:12'),(21,'menu_items','title',2,'pt','Media','2019-04-08 07:54:12','2019-04-08 07:54:12'),(22,'menu_items','title',12,'pt','Publicações','2019-04-08 07:54:12','2019-04-08 07:54:12'),(23,'menu_items','title',3,'pt','Utilizadores','2019-04-08 07:54:12','2019-04-08 07:54:12'),(24,'menu_items','title',11,'pt','Categorias','2019-04-08 07:54:12','2019-04-08 07:54:12'),(25,'menu_items','title',13,'pt','Páginas','2019-04-08 07:54:12','2019-04-08 07:54:12'),(26,'menu_items','title',4,'pt','Funções','2019-04-08 07:54:12','2019-04-08 07:54:12'),(27,'menu_items','title',5,'pt','Ferramentas','2019-04-08 07:54:12','2019-04-08 07:54:12'),(28,'menu_items','title',6,'pt','Menus','2019-04-08 07:54:12','2019-04-08 07:54:12'),(29,'menu_items','title',7,'pt','Base de dados','2019-04-08 07:54:12','2019-04-08 07:54:12'),(30,'menu_items','title',10,'pt','Configurações','2019-04-08 07:54:12','2019-04-08 07:54:12');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,'USD','en','2019-04-13 08:42:41','2019-04-13 08:42:41'),(2,'EUR','eu','2019-04-13 08:42:52','2019-04-13 08:42:52');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_coupons`
--

DROP TABLE IF EXISTS `user_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_coupons`
--

LOCK TABLES `user_coupons` WRITE;
/*!40000 ALTER TABLE `user_coupons` DISABLE KEYS */;
INSERT INTO `user_coupons` VALUES (3,9,1,NULL,NULL),(4,9,2,NULL,NULL),(5,8,1,NULL,NULL);
/*!40000 ALTER TABLE `user_coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (9,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT '1',
  `birthdate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/May2019/TVQhhGVq3KIkG0eacTrR.jpg',NULL,'$2y$10$UnSzNJMQaOotQsgKRDyNJesJ210/YRkzPG.bQUNdMLIhHXvca1YaS','CSmbSG1sJFD08CbSqQ8x75txp4AcFQXuMmY7WuM93EGXutNFf1Ih5SPb9d56','{\"locale\":\"en\"}','2019-04-08 07:54:12','2019-05-20 06:12:38',NULL,NULL,NULL,NULL,NULL),(5,NULL,'Hoang Nguyen','nghoang2013@gmail.com','users/default.png','2019-05-14 07:31:07','$2y$10$ti8PTnswI7Vqr6PCemYE0uIsWQlA8kehjFVKQOslahQzrLHJtA0l2','CqtnyxydOKODMSAWnu1toFLiAgbDkEpulVPnxRrq72pUaFbAosbyfHmcpKJa',NULL,'2019-05-14 07:30:05','2019-05-18 22:53:14','Xtreme','VN',2,0,'2009-02-10'),(6,NULL,'Hoang Nguyen','lac@admin.com','users/default.png','2019-05-14 07:31:07','$2y$10$ti8PTnswI7Vqr6PCemYE0uIsWQlA8kehjFVKQOslahQzrLHJtA0l2','Ie3gYpxLIGQOiqrvJ5yDIXDCYxSbCDg5nXwH0u774aNq7IvbyeI6FLQpMLO6',NULL,'2019-05-14 10:53:44','2019-05-14 10:53:44',NULL,NULL,NULL,1,NULL),(8,NULL,'Hoang Nguyen','xtreme@gmail.com','users/default.png','2019-05-19 18:31:29','$2y$10$YzKK7ly67guAanYXD4cFY.6bRRfItnOjWU4e9FHbWiujqOmLqxmTO','8Ae7fVyzhXZxEGrpHp2Si11OfSAbE7S77baMPkBcyS2Pvbli3cwusUgJmI4U',NULL,'2019-05-19 18:30:25','2019-05-19 18:31:29','Hoang','Nguyen',NULL,1,NULL),(9,2,'Admin 5Asystems','admin@5asystems.com','users/default.png',NULL,'$2y$10$FUgcr1uAxSJn2bvz1cvZz.XXkOL8QEpnqSs7Wjwdw0AnVZBHqWuze','S7VmZDiqfUNIE9mBkLxq9piKhVNW0oAJLxVcvDlY7e53RXKT9lMJafIlqH8h','{\"locale\":\"en\"}','2019-05-20 12:28:21','2019-05-20 12:28:21',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warranties`
--

DROP TABLE IF EXISTS `warranties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `warranties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `duration` int(11) NOT NULL DEFAULT '12',
  `price` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warranties`
--

LOCK TABLES `warranties` WRITE;
/*!40000 ALTER TABLE `warranties` DISABLE KEYS */;
/*!40000 ALTER TABLE `warranties` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-27  1:46:07
