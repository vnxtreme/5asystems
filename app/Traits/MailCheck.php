<?php
namespace App\Traits;

use Illuminate\Support\Facades\Config;
/**
 * 
 */
trait MailCheck
{
	public function checkSMTPconnection()
    {
		$SMTPconfig = Config::get('mail');
		
        try {
            $transport = new \Swift_SmtpTransport($SMTPconfig['host'], $SMTPconfig['port'], $SMTPconfig['encryption']);
            $transport->setUsername($SMTPconfig['username']);
            $transport->setPassword($SMTPconfig['password']);
            $mailer = new \Swift_Mailer($transport);
            $mailer->getTransport()->start();
            return true;
        } catch (\Swift_TransportException $e) {
            // dd($e->getMessage());
            return false;
        } catch (\Exception $e) {
            // dd($e->getMessage());
            return false;
        };
	}
	
	public function hasSMTPaccount()
	{
		$SMTPconfig = Config::get('mail');
		return $SMTPconfig['username'] and $SMTPconfig['password'] ?: false;
	}
}
