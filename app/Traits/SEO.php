<?php
namespace App\Traits;

/**
 *
 */
trait SEO
{
    public function extract_SEO($object): array
    {
        return [
            'seo_title' => $object->seo_title ? $object->seo_title : $this->find_title($object),
            'meta_description' => $object->meta_description,
			'meta_keywords' => $object->meta_keywords,
			'meta_image_fb' => $object->image_fb
        ];
    }

    protected function find_title($object): string
    {
        return $object->title ? $object->title : $object->name;
    }
}
