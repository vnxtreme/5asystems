<?php
namespace App\Traits;

/**
 *
 */
trait UnitConversion
{
    public function stringToInt(String $string)
    {
        return (int) str_replace(',', '', $string);
    }

    public function stringToFloat(String $string)
    {
        return (float) str_replace(',', '', $string);
    }
}
