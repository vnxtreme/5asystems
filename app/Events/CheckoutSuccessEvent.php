<?php

namespace App\Events;

use App\Models\OrderInformation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CheckoutSuccessEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $orderInfo;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(OrderInformation $orderInfo)
    {
        $this->orderInfo = $orderInfo;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
