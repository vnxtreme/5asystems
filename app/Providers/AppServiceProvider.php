<?php

namespace App\Providers;

use App\Models\ProductCategory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\MyCompany;
use App\Models\Contact;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('path.public', function(){
		// 	return base_path('public_html');
		// });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$categories = ProductCategory::with('subCategory')->sortQuery()->whereNull('parent_id')->get();
		$myCompanies = MyCompany::whereStatus(1)->get();
		$contacts = Contact::get();
        View::share(compact('categories', 'myCompanies', 'contacts'));

        /**
         * Paginate a standard Laravel Collection.
         *
         * @return array
         */
        Collection::macro('paginate', function (int $perPage, int $total = null, int $page = null, string $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path'     => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }
}
