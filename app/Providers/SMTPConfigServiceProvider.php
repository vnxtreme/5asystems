<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class SMTPConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $smtpConfig = Setting::whereGroup('SMTP')->get()->pluck('value','key');

        if ($smtpConfig) {
            $config = [
                'driver'     => 'smtp',
                'host'       => $smtpConfig['smtp.host'],
                'port'       => (int) $smtpConfig['smtp.port'],
                'from'       => [
                    'address' => $smtpConfig['smtp.default_email'],
                    'name'    => $smtpConfig['smtp.name'],
                ],
                'username'   => $smtpConfig['smtp.username'],
                'password'   => $smtpConfig['smtp.password'],
                'encryption' => env('APP_ENV') === 'production' ? $smtpConfig['smtp.encryption'] : null,
            ];
            Config::set('mail', $config);
        }
    }
}
