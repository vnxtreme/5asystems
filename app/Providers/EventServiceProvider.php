<?php

namespace App\Providers;

use App\Events\CheckoutSuccessEvent;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Listeners\CouponUpdateListener;
use App\Listeners\sendEmailCheckoutSuccessListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class           => [
            SendEmailVerificationNotification::class,
        ],
        'cart.added'                => [
            CouponUpdateListener::class,
        ],
        'cart.updated'              => [
            CouponUpdateListener::class,
        ],
        'cart.removed'              => [
            CouponUpdateListener::class,
        ],
        CheckoutSuccessEvent::class => [
            sendEmailCheckoutSuccessListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
