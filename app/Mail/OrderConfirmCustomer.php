<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConfirmCustomer extends Mailable
{
    use Queueable, SerializesModels;

    protected $orderInfo;
    // protected $total_amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderInfo)
    {
        $this->orderInfo = $orderInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailTemplate = EmailTemplate::find(1);
        $html          = str_replace(
            [
                '[customer_name]',
                '[total_amount]',
            ],
            [
                $this->orderInfo->name,
                $this->orderInfo->total,
            ],
            $emailTemplate->html
        );

        return $this
            ->to($this->orderInfo->email, $this->orderInfo->name)
            ->subject($emailTemplate->subject)
            ->html($html);
    }
}
