<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Setting;

class OrderConfirmAdmin extends Mailable
{
    use Queueable, SerializesModels;

    protected $orderInfo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderInfo)
    {
        $this->orderInfo = $orderInfo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$adminEmail = Setting::where('key', 'smtp.notify_email')->first();
		
        $emailTemplate = EmailTemplate::find(2);
        $html          = str_replace(
            [
                '[customer_name]',
                '[email]',
                '[phone]',
                '[address]',
                '[city]',
                '[total_amount]',
            ],
            [
                $this->orderInfo->name,
                $this->orderInfo->email,
                $this->orderInfo->phone,
                $this->orderInfo->address,
                $this->orderInfo->city,
                $this->orderInfo->total,
            ],
            $emailTemplate->html
        );

		return $this
			->to($adminEmail->value ?? null)
			->subject($emailTemplate->subject)
			->html($html);
    }
}
