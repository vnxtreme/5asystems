<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WarnLowQuantity extends Mailable
{
    use Queueable, SerializesModels;

	protected $productDetail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($productDetail)
    {
        $this->productDetail = $productDetail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$emailTemplate = EmailTemplate::find(3);
        $html          = str_replace(
            [
				'[name]',
				'[product_id]',
                '[qty]',
            ],
            [
                $this->productDetail->product->name,
                $this->productDetail->product->id,
                $this->productDetail->quantity,
            ],
            $emailTemplate->html
		);
		
		return $this->to('nghoang@gmail.com')
			->subject('Low in Quantity')
			->html($html);
    }
}
