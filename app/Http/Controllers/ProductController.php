<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDetail;
use App\Traits\SEO;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use SEO;
    const PRODUCT_LIMIT = 12;

    function list(Request $request) {
        $categorySlug = $request->categorySlug;
        $column       = $request->col;
        $order        = $request->order;

        $category = ProductCategory::with('allSubCategories', 'products', 'subCategory', 'parentCategory')->whereSlug($categorySlug)->firstOrFail();

        $products = $this->getAllProduct($category);

        if ($column):
            $products = $this->sort($products, $column, $order);
        endif;

		$products = $products->paginate(static::PRODUCT_LIMIT);
		$seoMeta = $this->extract_SEO($category);

        return view('product.list', compact('category', 'products', 'seoMeta'));
    }

    public function getAllProduct($category): collection
    {
        $products = new Collection();

        $products = $products->merge($category->products);

        if ($category->allSubCategories->isNotEmpty()):
            $products = $this->productsFromNestedCategories($products, $category->allSubCategories);
        endif;

        return $products;
    }

    protected function productsFromNestedCategories($products, $categories)
    {
        return $categories->reduce(function ($products, $subCategory) {
            //recursive
            if ($subCategory->allSubCategories->isNotEmpty()):
                $products = $this->productsFromNestedCategories($products, $subCategory->allSubCategories);
            endif;
            return $products->merge($subCategory->products);
        }, $products);
    }

    public function detail(Request $request)
    {
        $slug            = $request->slug;
        $productDetailId = $request->pdid;

        $product = Product::has('productDetail')->active()->whereSlug($slug)->firstOrFail();

        if ($productDetailId):
            $firstProductDetail = $product->productDetail->filter(function ($detail) use ($productDetailId) {
                return $detail->id == $productDetailId;
            })->first();
        else:
            $firstProductDetail = $product->productDetail[0];
        endif;
        $colorList = $product->productDetail->groupBy('color_id');
        $sizeList  = $colorList[$firstProductDetail->color_id];

        $relatedProducts = Product::relatedProducts($product);
        $seoMeta         = $this->extract_SEO($product);

        return view('product.detail', compact('product', 'firstProductDetail', 'relatedProducts', 'colorList', 'sizeList', 'seoMeta'));
    }

    public function search(Request $request)
    {
        $key    = $request->key;
        $column = $request->col;
        $order  = $request->order;

        $products = Product::has('firstProductDetail')->isVisible()->where('name', 'like', "%{$key}%")->get();

        if ($column):
            $products = $this->sort($products, $column, $order);
        endif;

        $products = $products->paginate(static::PRODUCT_LIMIT);

        return view('product.search', compact('products'));
    }


    protected function sort($collection, $column, $order): collection
    {
        switch ($column) {
            case 'price':
                return $this->sortByRelationship($collection, $column, $order);
                break;

            default:
                return $this->sortByColumn($collection, $column, $order);
                break;
        }
    }

    protected function sortByRelationship($collection, $column, $order): collection
    {
        return $order == 'desc' ? $collection->sortByDesc("firstProductDetail.{$column}") : $collection->sortBy("firstProductDetail.{$column}");
    }

    protected function sortByColumn($collection, $column, $order): collection
    {
        return $order == 'desc' ? $collection->sortByDesc($column) : $collection->sortBy($column);
    }
}
