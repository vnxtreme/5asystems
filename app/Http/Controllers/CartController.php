<?php

namespace App\Http\Controllers;

use App\Events\CheckoutSuccessEvent;
use App\Http\Controllers\FedEx\RateRequestController;
use App\Http\Controllers\FedEx\ValidateAddressController;
use App\Models\Address;
use App\Models\OrderInformation;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\UserCoupon;
use App\Traits\MailCheck;
use App\Traits\SEO;
use App\Traits\UnitConversion;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    use SEO, MailCheck, UnitConversion;

    protected function findInCart($productDetail)
    {
        return Cart::search(function ($itemCart) use ($productDetail) {
            return $itemCart->id == $productDetail->id;
        })->first();
    }

    /* AJAX */
    public function add(Request $request)
    {
        $productId = $request->product_id;
        $colorId   = $request->options['color'];
        $sizeId    = $request->options['size'];

        $product = Product::findOrFail($productId);

        $productDetail = $product->productDetail->filter(function ($item) use ($colorId, $sizeId) {
            return $item->color_id == $colorId and $item->size_id == $sizeId;
        })->first();

        if (!$productDetail) {
            abort(404);
        }

        $matchItemCart = $this->findInCart($productDetail);

        if ($matchItemCart):
            Cart::update($matchItemCart->rowId, $matchItemCart->qty + $request->qty);
        else:
            $options = array_merge(
                $request->options,
                [
                    'slug'  => $product->slug,
                    'image' => $product->thumbnail('cropped'),
                    'color' => $productDetail->color->name,
                    'size'  => $productDetail->size->name,
                    'weight' => $product->weight ?? 1000,
                    'isFreeShipping'  => $product->is_free_shipping,
                ]
            );
            Cart::add($productDetail, $request->qty, $options);
        endif;

        return [
            'status' => 'success',
            'cart'   => Cart::content(),
        ];
    }

    public function remove(Request $request)
    {
        Cart::remove($request->rowId);

        return redirect()->back();
    }

    public function update(Request $request)
    {
        $qty = $request['product-quantity-spin'];
        Cart::update($request->rowId, $qty);

        return redirect()->back();
    }

    public function cart(): View
    {
        $seoMeta = [
            'seo_title'        => 'Cart',
            'meta_description' => 'Cart list',
            'meta_keywords'    => 'cart, list',
            'meta_image_fb'    => '',
        ];
        $data = array_merge(compact('seoMeta'), $this->getCartNumbers());

        return view('cart.list')->with($data);
    }

    public function checkout()
    {
        if (!Cart::count()):
            return redirect()->route('home');
        endif;

        $seoMeta = [
            'seo_title'        => 'Checkout',
            'meta_description' => 'Cart page',
            'meta_keywords'    => 'checkout, payment',
            'meta_image_fb'    => '',
        ];
        $data = array_merge(compact('seoMeta'), $this->getCartNumbers());

        return view('cart.checkout')->with($data);
    }

    public function checkoutPost(Request $request)
    {
        if (Cart::content()->isNotEmpty()):
            $cartNumbers = $this->getCartNumbers();

            if ($request->address_id and Auth::check()):
                $addressData = $this->addressData($request->address_id);

                $data = [
                    'name'                   => "{$addressData['first_name']} {$addressData['last_name']}",
                    'email'                  => $request->email,
                    'phone'                  => $addressData['phone'],
                    'address'                => $addressData['address'],
                    'city'                   => $addressData['city'],
                    'state_or_province_code' => $addressData['state_or_province_code'],
                    'postal_code'            => $addressData['postal_code'],
                    'delivery_message'       => $request->delivery_message,
                    'payment_method'         => $request->payment_method ?? 'bank',
                ];
            else:
                $request->validate([
                    'first_name'      => 'required',
                    'last_name'       => 'required',
                    'email'           => 'required|email',
                    'phone'           => 'required',
                    'address'         => 'required',
                    'city'            => 'required',
                    'postal_code'     => 'required|integer',
                    'delivery_method' => 'required',
                ]);

                $data = [
                    'name'                   => "{$request->first_name} {$request->last_name}",
                    'email'                  => $request->email,
                    'phone'                  => $request->phone,
                    'address'                => $request->address,
                    'city'                   => $request->city,
                    'state_or_province_code' => $request->state_or_province_code,
                    'postal_code'            => $request->postal_code,
                    'delivery_message'       => $request->delivery_message,
                    'payment_method'         => $request->payment_method ?? 'bank',
                ];
            endif;

            $data = array_merge($data, [
                'tax'          => $cartNumbers['tax'],
                'discount'     => $cartNumbers['discount'],
                'coupon'       => $cartNumbers['coupon'],
                'delivery_fee' => $cartNumbers['deliveryFee'],
                'sub_total'    => $cartNumbers['newSubtotal'],
                'total'        => $cartNumbers['newTotal'],
                'data'         => $request->data,
                'transaction_id'=>$request->transaction_id
            ]);

            //create default address for Login user
            if (Auth::check() and Auth::user()->addresses->isEmpty()):
                Auth::user()->addresses()->create($request->all());
            endif;

            //save coupon+user
            $cartNumbers['coupon'] && $cartNumbers['is_one_time'] ? $this->saveUserCoupon($cartNumbers['id']) : '';

            //persist Order details
            $orderInfo = $this->persistOrder($data);

            Cart::destroy();
            session()->forget('coupon');
            session()->forget('deliveryFee');

            //send email customer,send email admin...
            event(new CheckoutSuccessEvent($orderInfo));

            $seoMeta = [
                'seo_title'        => 'Cart',
                'meta_description' => 'Cart list',
                'meta_keywords'    => 'cart, list',
                'meta_image_fb'    => '',
            ];

            return view('cart.checkout-success', compact('orderInfo'));
        endif;

        return redirect()->route('home');
    }

    public function addressData($addressId)
    {
        $address = Address::where('user_id', \Auth::id())->where('id', $addressId)->firstOrFail();

        return [
            'first_name'             => $address->first_name,
            'last_name'              => $address->last_name,
            'phone'                  => $address->phone,
            'address'                => $address->address,
            'city'                   => $address->city,
            'state_or_province_code' => $address->state_or_province_code,
            'postal_code'            => $address->postal_code,
        ];
    }

    public function persistOrder($data)
    {
        return \DB::transaction(function () use ($data) {
            $orderInfo = OrderInformation::create($data);

            foreach (Cart::content() as $item) {
                $orderInfo->order()->create([
                    'product_detail_id' => $item->id,
                    'quantity'          => $item->qty,
                    'price'             => $item->price,
                    'product_name'      => $item->name,
                    'product_color'     => $item->options->color,
                    'product_size'      => $item->options->size,
                    'product_image'     => $item->options->image,
                ]);

                //minus qty in Product_detail
                $this->updateProductDetailQuantity($item);
            }

            return $orderInfo;
        });
    }

    protected function getCartNumbers(): array
    {
        $tax         = $this->stringToFloat(Cart::tax()) / 100;
        $discount    = session()->get('coupon')['discount'] ?? 0;
        $newSubtotal = $this->stringToFloat(Cart::subtotal()) - $discount;
        $deliveryFee = (float) session()->get('deliveryFee');

        $newTotal    = $newSubtotal * (1 + $tax) + $deliveryFee;
        $coupon      = session()->get('coupon')['name'] ?? null;
        $id          = session()->get('coupon')['id'] ?? null;
        $is_one_time = session()->get('coupon')['is_one_time'] ?? null;

        return compact('tax', 'discount', 'deliveryFee', 'newSubtotal', 'newTotal', 'coupon', 'id', 'is_one_time');
    }

    protected function saveUserCoupon(Int $couponId): void
    {
        UserCoupon::insert([
            'user_id'   => Auth::id(),
            'coupon_id' => $couponId,
        ]);
    }

    protected function updateProductDetailQuantity($item): void
    {
        $productDetail = ProductDetail::find($item->id);
        $productDetail->quantity -= $item->qty;
        $productDetail->save();
    }

    public function validateAddress(Request $request)
    {
        $request->validate([
            'address'                => 'required',
            'city'                   => 'required',
            'state_or_province_code' => 'required',
            'postal_code'            => 'required',
        ]);

        //validate address
        $validateAddress = new ValidateAddressController();
        $result          = $validateAddress->handle($request->all());

        if ($result):
            return $this->showDeliveryRate($request->all());
        endif;

        return response()->json(['message' => 'The given data was invalid.'], 422);
    }
    
    public function validateAddressId(Request $request)
    {
        $addressId = $request->address_id;
        $addressData = $this->addressData($addressId);

        $validateAddress = new ValidateAddressController();
        $result          = $validateAddress->handle($addressData);
        
        if($result){
            return $this->showDeliveryRate($addressData);
        }

        return response()->json(['message' => 'The given data was invalid.'], 422);
    }

    protected function showDeliveryRate($addressData)
    {
        $rateRequest = new RateRequestController();
        $cartQuantity = Cart::count();
        $deliveryFee = $this->isFreeShipping() ? 0 : $rateRequest->checkRate($addressData, $cartQuantity); //get from API

        $deliveryFee ? session(['deliveryFee' => $deliveryFee]) : '';

        extract($this->getCartNumbers(), EXTR_SKIP); //get $newTotal

        return response()->json([
            'deliveryFee' => $deliveryFee,
            'newTotal'    => $newTotal,
        ]);
    }
	
	public function allItemsWeight()
	{
		$cartContent = Cart::content();
		return $cartContent->reduce(function($accumulator, $item){
			return $accumulator + (int) $item->options->weight;
		}, 0);
    }
    
    protected function isFreeShipping(): bool
    {
        $items = Cart::content();
        foreach ($items as $item) {
            if(!$item->options->isFreeShipping){
                return false;
            }
        }

        return true;
    }
}
