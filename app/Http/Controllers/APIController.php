<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDetail;
use App\Models\ProductInput;
use App\Models\Serial;
use App\Models\Size;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function getColors(): Collection
    {
        return Color::all();
    }

    public function getSizes(): Collection
    {
        return Size::all();
    }

    public function listProductDetails(Request $request)
    {
        $products       = Product::with('productDetail', 'productDetail.color', 'productDetail.size')->active()->where('category_id', $request->category_id)->get();
        $idArray        = $products->pluck('id')->toArray();
        $productDetails = ProductDetail::with('product')->whereIn('product_id', $idArray)->get();

        return $productDetails;
    }

    public function productCategories()
    {
        return ProductCategory::with('subCategory')->where('parent_id', null)->get();
    }

    public function productInfo(Request $request)
    {
        $id = $request->id;

        return Product::with('productDetail')->whereId($id)->first();
    }

    public function getSerial(Request $request)
    {
        $productDetailId = $request->product_detail_id;
        return Serial::where('product_detail_id', $productDetailId)->get()->pluck('serial');
    }

    
}
