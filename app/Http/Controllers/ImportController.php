<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StockImport;

class ImportController extends Controller
{
	public function importView()
	{
		return view('import');
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function product(Request $request) 
    {
        Excel::import(new ProductImport, $request->file('file'));
           
		return back()
			->with([
				'message'    => "Import succeeded",
				'alert-type' => 'success',
			]);
	}
	
	public function stock(Request $request) 
    {
        Excel::import(new StockImport, $request->file('file'));
           
		return back()
			->with([
				'message'    => "Import succeeded",
				'alert-type' => 'success',
			]);
    }
}
