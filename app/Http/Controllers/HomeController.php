<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\Contact;
use App\Models\CustomerMessage;
use App\Models\MyCompany;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $carousels = Carousel::sortQuery()->get();

        $featureProductsChunk = Product::active()->featured()->get()->chunk(4);

        $newProducts        = Product::active()->isNew()->get();
        $popularProducts    = Product::active()->isPopular()->get();
        $bestsellerProducts = Product::active()->isBestseller()->get();

        $posts       = Post::homePosts()->get();
        // $myCompanies = MyCompany::whereStatus(1)->get();

        return view('home.index', compact('carousels', 'featureProductsChunk', 'popularProducts', 'bestsellerProducts', 'newProducts', 'posts'));
    }

    public function myCompany(Request $request)
    {
        $slug    = $request->slug;
        $content = MyCompany::whereSlug($slug)->whereStatus(1)->firstOrFail();

        return view('home.my-company', compact('content'));
    }

    public function contact()
    {
        // $contacts = Contact::get();
        return view('home.contact');
    }

    public function sendContact(Request $request)
    {
        $request->validate([
            'message' => 'required',
            'email'   => 'required|email',
        ]);

        CustomerMessage::create($request->all());

        return redirect()->back()->with('success', 'Message sent!');
    }
}
