<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Traits\SEO;

class NewsController extends Controller
{
	use SEO;

    public function list()
	{
		$posts = Post::active()->get();
		$seoMeta = [
			'seo_title' => 'News',
			'meta_description' => 'Latest News',
			'meta_keywords' => 'news, latest',
			'meta_image_fb' => ''
		];

		return view('news.list', compact('posts', 'seoMeta'));
	}

	public function detail(Request $request)
	{
		$slug = $request->slug;
		$post = Post::with('author')->whereSlug($slug)->active()->firstOrFail();

		$seoMeta = $this->extract_SEO($post);

		return view('news.detail', compact('post', 'seoMeta'));
	}
}
