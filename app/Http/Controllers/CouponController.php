<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use App\Models\UserCoupon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
    public function apply(Request $request)
    {
        $coupon = Coupon::where('code', $request->coupon_code)
            ->whereDate('valid_until', '>=', today())
            ->first();

        if (!$coupon):
            return redirect()->back()->withErrors(["coupon_code" => "Invalid code!"]);
        endif;

        if ($coupon->is_one_time and !Auth::check()):
            return redirect()->back()->withErrors(["coupon_code" => "Please sign in to use Coupon!"]);
        endif;

        if (Auth::check() and $this->checkCouponUsage($coupon->id)):
            return redirect()->back()->withErrors(["coupon_code" => "Coupon is already used!"]);
        endif;

        session()->put('coupon', [
            'id'          => $coupon->id,
            'name'        => $coupon->code,
            'discount'    => $coupon->discount(Cart::subtotal()),
            'is_one_time' => $coupon->is_one_time,
        ]);

        return redirect()->back()->with(['coupon_code' => "Coupon applied!"]);
    }

    protected function checkCouponUsage(Int $couponId)
    {
        $userCoupon = UserCoupon::where('user_id', Auth::id())->where('coupon_id', $couponId)->first();

        return $userCoupon ?: false;
    }

    public function destroy()
    {
        session()->forget('coupon');

        return redirect()->back();
    }
}
