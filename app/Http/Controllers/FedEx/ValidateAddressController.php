<?php
namespace App\Http\Controllers\FedEx;

use FedEx\AddressValidationService\ComplexType;
use FedEx\AddressValidationService\Request;

class ValidateAddressController
{

    public function handle(array $address): bool
    {
        // $address = [
        //     'address' => '805 McDonald Ave',
        //     'city' => 'Charlotte',
        //     'state_or_province_code' => 'NC',
        //     'postal_code' => '28203'
        // ];
        $addressValidationRequest = new ComplexType\AddressValidationRequest();
        // User Credentials
        $addressValidationRequest->WebAuthenticationDetail->UserCredential->Key      = config('fedex.key');
        $addressValidationRequest->WebAuthenticationDetail->UserCredential->Password = config('fedex.password');
        // Client Detail
        $addressValidationRequest->ClientDetail->AccountNumber = config('fedex.account_number');
        $addressValidationRequest->ClientDetail->MeterNumber   = config('fedex.meter_number');
        // Version
        $addressValidationRequest->Version->ServiceId    = 'aval';
        $addressValidationRequest->Version->Major        = 4;
        $addressValidationRequest->Version->Intermediate = 0;
        $addressValidationRequest->Version->Minor        = 0;
        // Address(es) to validate.
        $addressValidationRequest->AddressesToValidate                                  = [new ComplexType\AddressToValidate()]; // just validating 1 address in this example.
        $addressValidationRequest->AddressesToValidate[0]->Address->StreetLines         = [$address['address']];
        $addressValidationRequest->AddressesToValidate[0]->Address->City                = $address['city'];
        $addressValidationRequest->AddressesToValidate[0]->Address->StateOrProvinceCode = $address['state_or_province_code'];
        $addressValidationRequest->AddressesToValidate[0]->Address->PostalCode          = $address['postal_code'];
        $addressValidationRequest->AddressesToValidate[0]->Address->CountryCode         = 'US';
        $request                                                                        = new Request();
        $addressValidationReply                                                         = $request->getAddressValidationReply($addressValidationRequest);

        if (!empty($addressValidationReply->AddressResults)):
            return $addressValidationReply->AddressResults[0]->Classification !== "UNKNOWN";
        endif;

        return false;
    }
}
