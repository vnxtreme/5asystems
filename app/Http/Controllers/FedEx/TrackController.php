<?php

namespace App\Http\Controllers\FedEx;

use Illuminate\Http\Request as LaravelRequest;
use App\Http\Controllers\Controller;
use FedEx\TrackService\ComplexType;
use FedEx\TrackService\Request;
use FedEx\TrackService\SimpleType;

// define('FEDEX_ACCOUNT_NUMBER', '510088000');
// define('FEDEX_METER_NUMBER', '119149730');
// define('FEDEX_KEY', 'YAoDWaX8NeLUNSGe');
// define('FEDEX_PASSWORD', 'kLkuS4vOu1bpKFd2Ff5S1KgKP');

class TrackController
{
    public function trackById(LaravelRequest $request)
    {
		// $trackingId1 = $request->id;
        $trackingId1  = 568838414941;
		// $trackingId2  = 123456789012;
		
        $trackRequest = new ComplexType\TrackRequest();
        // User Credential
        $trackRequest->WebAuthenticationDetail->UserCredential->Key      = config('fedex.key');
        $trackRequest->WebAuthenticationDetail->UserCredential->Password = config('fedex.password');
        // Client Detail
        $trackRequest->ClientDetail->AccountNumber = config('fedex.account_number');
        $trackRequest->ClientDetail->MeterNumber   = config('fedex.meter_number');
        // Version
        $trackRequest->Version->ServiceId    = 'trck';
        $trackRequest->Version->Major        = 16;
        $trackRequest->Version->Intermediate = 0;
        $trackRequest->Version->Minor        = 0;
        // Track 2 shipments
        $trackRequest->SelectionDetails = [
            new ComplexType\TrackSelectionDetail(),
            // new ComplexType\TrackSelectionDetail()
        ];
        // Track shipment 1
        $trackRequest->SelectionDetails[0]->PackageIdentifier->Value = $trackingId1;
        $trackRequest->SelectionDetails[0]->PackageIdentifier->Type  = SimpleType\TrackIdentifierType::_TRACKING_NUMBER_OR_DOORTAG;
        // // Track shipment 2
        // $trackRequest->SelectionDetails[1]->PackageIdentifier->Value = $trackingId2;
        // $trackRequest->SelectionDetails[1]->PackageIdentifier->Type = SimpleType\TrackIdentifierType::_TRACKING_NUMBER_OR_DOORTAG;
        $request    = new Request();
        $trackReply = $request->getTrackReply($trackRequest);

        $response = $this->parseData($trackReply);
        dd($response);
        // $response = [
        //     [
        //         "Carrier"              => "FedEx Express",
        //         "TrackingNumber"       => "568838414941",
        //         "Service"              => "FedEx 2Day",
        //         "ServiceCommitMessage" => "No scheduled delivery date available at this time.",
        //         "StatusDetail"         => "At destination sort facility",
        //         "CreationTime"         => "2014-01-08T00:00:00",
        //         "Events"               => collect([
        //             "Timestamp"        => "2014-01-07T19:37:00-07:00",
        //             "EventType"        => "AR",
        //             "EventDescription" => "At destination sort facility",
        //             "Address"          => collect([]),
        //             "ArrivalLocation"  => "SORT_FACILITY",
        //         ]),

        //     ],
        // ];
    }

    public function parseData($trackReply)
    {
        $data                  = [];
        $completedTrackDetails = $trackReply->CompletedTrackDetails[0];
        $trackDetails          = $completedTrackDetails->TrackDetails;

        foreach ($trackDetails as $detail):
            array_push($data, [
                'Carrier'              => $detail->OperatingCompanyOrCarrierDescription,
                'TrackingNumber'       => $detail->TrackingNumber,
                'Service'              => $detail->Service->Description,
                'ServiceCommitMessage' => $detail->ServiceCommitMessage,
                'StatusDetail'         => $detail->StatusDetail->Description,
                'CreationTime'         => $detail->StatusDetail->CreationTime,
                'Events'               => $detail->Events,
            ]);
        endforeach;

        return $data;
    }
}
