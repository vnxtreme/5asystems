<?php
namespace App\Http\Controllers\FedEx;

// use Illuminate\Http\Request as LaravelRequest;
use App\Models\Setting;
use FedEx\RateService\ComplexType;
use FedEx\RateService\Request;
use FedEx\RateService\SimpleType;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Http\Controllers\CartController;

class RateRequestController
{
    public function checkRate( array $address, $cartQuantity)
    {
        // $address = [
        //     'address'                => '805 McDonald Ave',
        //     'city'                   => 'Charlotte',
        //     'state_or_province_code' => 'NC',
        //     'postal_code'            => '28203',
        // ];
		$allItemsWeight = (new CartController())->allItemsWeight();
		
        $packageDimension = Setting::packageDimension();
        $packageDimension = $packageDimension->mapWithKeys(function ($row) {
            return [$row['key'] => (int) $row['value']];
		});
        
		//number of package(s) required
		$packageCount = (int) round($cartQuantity / $packageDimension['package-dimension.items_per_package'], 0);
		$packageCount = $packageCount < 1 ? 1 : $packageCount;
        // dd($packageDimension['package-dimension.height']);
		
        //start API
        $rateRequest = new ComplexType\RateRequest();
        //authentication & client details
        $rateRequest->WebAuthenticationDetail->UserCredential->Key      = config('fedex.key');
        $rateRequest->WebAuthenticationDetail->UserCredential->Password = config('fedex.password');
        $rateRequest->ClientDetail->AccountNumber                       = config('fedex.account_number');
        $rateRequest->ClientDetail->MeterNumber                         = config('fedex.meter_number');
        $rateRequest->TransactionDetail->CustomerTransactionId          = 'get rate service request';
        //version
        $rateRequest->Version->ServiceId     = 'crs';
        $rateRequest->Version->Major         = 24;
        $rateRequest->Version->Minor         = 0;
        $rateRequest->Version->Intermediate  = 0;
        $rateRequest->ReturnTransitAndCommit = true;
        //shipper
        $rateRequest->RequestedShipment->PreferredCurrency                     = 'USD';
        $rateRequest->RequestedShipment->Shipper->Address->StreetLines         = [setting('fedex.street_lines')];
        $rateRequest->RequestedShipment->Shipper->Address->City                = setting('fedex.city');
        $rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = setting('fedex.state_or_province_code');
        $rateRequest->RequestedShipment->Shipper->Address->PostalCode          = (int) setting('fedex.postal_code');
        $rateRequest->RequestedShipment->Shipper->Address->CountryCode         = setting('fedex.country_code');
        //recipient
        $rateRequest->RequestedShipment->Recipient->Address->StreetLines         = [$address['address']];
        $rateRequest->RequestedShipment->Recipient->Address->City                = $address['city'];
        $rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $address['state_or_province_code'];
        $rateRequest->RequestedShipment->Recipient->Address->PostalCode          = (int) $address['postal_code'];
        $rateRequest->RequestedShipment->Recipient->Address->CountryCode         = 'US';
        //shipping charges payment
        $rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;
        //rate request types
        $rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_NONE, SimpleType\RateRequestType::_LIST];
		$rateRequest->RequestedShipment->PackageCount     = $packageCount;

		$requestedPackageLineItemsArray = [];
		for ($i=0; $i < $packageCount; $i++) :
			array_push($requestedPackageLineItemsArray, new ComplexType\RequestedPackageLineItem()) ;
			$rateRequest->RequestedShipment->RequestedPackageLineItems = $requestedPackageLineItemsArray;

			$rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Weight->Value      = ($allItemsWeight/$packageCount/1000);
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Weight->Units      = SimpleType\WeightUnits::_KG;
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Dimensions->Length = $packageDimension['package-dimension.length'];
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Dimensions->Width  = $packageDimension['package-dimension.width'];
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Dimensions->Height = $packageDimension['package-dimension.height'];
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->Dimensions->Units  = SimpleType\LinearUnits::_CM;
	        $rateRequest->RequestedShipment->RequestedPackageLineItems[$i]->GroupPackageCount  = $i+1;
        endfor;
        
// dd($packageCount, $rateRequest->RequestedShipment->RequestedPackageLineItems);
        //create package line items
        // $rateRequest->RequestedShipment->RequestedPackageLineItems = [
        //     new ComplexType\RequestedPackageLineItem(),
        //     new ComplexType\RequestedPackageLineItem(),
		// ];
        // //package 1
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value      = 1.5;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value      = 1.5;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Units      = SimpleType\WeightUnits::_KG;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Length = 30;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Width  = 20;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Height = 20;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Units  = SimpleType\LinearUnits::_CM;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[0]->GroupPackageCount  = 1;
        // //package 2
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Weight->Value      = 5;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Weight->Units      = SimpleType\WeightUnits::_KG;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Dimensions->Length = 20;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Dimensions->Width  = 20;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Dimensions->Height = 10;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->Dimensions->Units  = SimpleType\LinearUnits::_CM;
        // $rateRequest->RequestedShipment->RequestedPackageLineItems[1]->GroupPackageCount  = 1;
        $rateServiceRequest                                                               = new Request();
        // $rateServiceRequest->getSoapClient()->__setLocation(Request::PRODUCTION_URL); //use production URL
        $rateReply = $rateServiceRequest->getGetRatesReply($rateRequest); // send true as the 2nd argument to return the SoapClient's stdClass response.

        // if (!empty($rateReply->RateReplyDetails)) {
        //     foreach ($rateReply->RateReplyDetails as $rateReplyDetail) {
        //         var_dump($rateReplyDetail->ServiceType);
        //         if (!empty($rateReplyDetail->RatedShipmentDetails)) {
        //             foreach ($rateReplyDetail->RatedShipmentDetails as $ratedShipmentDetail) {
        //                 var_dump($ratedShipmentDetail->ShipmentRateDetail->RateType . ": " . $ratedShipmentDetail->ShipmentRateDetail->TotalNetCharge->Amount);
        //             }
        //         }
        //         echo "<hr />";
        //     }
        // }

        if (!empty($rateReply->RateReplyDetails)):
            return $this->getRate($rateReply->RateReplyDetails);
        endif;

        $this->checkRate($address, $cartQuantity);
    }

    protected function getRate(array $RateReplyDetails)
    {
        $rateDetail = array_filter($RateReplyDetails, function ($rateReplyDetail) {
            return $rateReplyDetail->ServiceType == "FEDEX_GROUND";
        });

        try {
            $rateDetail = reset($rateDetail);
            return $rateDetail->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
        } catch (\Throwable $errors) {
            throw $errors;
        }
    }
}
