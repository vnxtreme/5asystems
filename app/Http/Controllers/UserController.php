<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use App\Models\OrderInformation;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function account()
    {
        return view('user.account');
    }

    public function profile()
    {
        return view('user.profile');
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'first_name'   => 'required',
            'last_name'    => 'required',
            'new_password' => 'nullable|min:6',
            'birthdate'    => 'date_format:Y-m-d|before:today|nullable',
        ]);

        $user = \Auth::user();
        $data = [
            'gender'     => $request->gender,
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'birthdate'  => $request->birthdate,
        ];

		//change password
        if ($request->new_password and !Hash::check($request->new_password, $user->password)):
            $data = array_add($data, 'password', bcrypt($request->new_password)); 
        endif;

        $user->update($data);

        return redirect()->route('user.profile')->with('success', 'Profile updated!');
    }

    public function addresses(): View
    {
        $user      = \Auth::user();
        $addresses = $user->addresses;

        return view('user.addresses', compact('user', 'addresses'));
    }

    public function addressAdd(Request $request)
    {
        if ($request->isMethod('post')):
            $dataArray = $request->all();
            unset($dataArray['_token']);

            \Auth::user()->addresses()->create($dataArray);

            return redirect()->route('user.addresses')->with('success', 'Address added!');
		endif;
		
		return view('user.address-add');
    }

    public function adressUpdate(Request $request)
    {
        $addressId = $request->address_id;
        $address   = Address::findOrFail($addressId);

        if ($request->isMethod('post')):
            $dataArray = $request->all();
            $addressId = $request->address_id;
            $address->update($dataArray);

            return redirect()->route('user.addresses')->with('success', 'Address updated!');
        endif;

        return view('user.address-update', compact('address'));
    }

    public function addressDelete(Request $request)
    {
        $addressId = $request->address_id;
        $address   = Address::findOrFail($addressId);
        $address->delete();

        return redirect()->route('user.addresses')->with('success', 'Address deleted!');
    }

    public function orderHistory()
    {
		$orderInformations = OrderInformation::where('email', Auth::user()->email)->get();
		
        return view('user.order-history', compact('orderInformations'));
    }
}
