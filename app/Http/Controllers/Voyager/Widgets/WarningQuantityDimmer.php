<?php

namespace App\Http\Controllers\Voyager\Widgets;

use Illuminate\Support\Str;
use App\Models\ProductDetail;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\Product;

class WarningQuantityDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
		$lowInQuantity = ProductDetail::has('product')->with('product:id,name,slug')->where('quantity', '<=', 10)->get()->toArray();

        return view('voyager::dimmer', array_merge(compact('lowInQuantity'), [
            'icon'   => 'voyager-file-text',
            'title'  => "Low in Quantity",
            'text'   => __('voyager::dimmer.page_text', ['count' => 1, 'string' => Str::lower('123')]),
            'button' => [
                'text' => __('voyager::dimmer.page_link_text'),
                'link' => route('voyager.pages.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
		]));
		// return view('voyager::dimmer', compact('lowInQuantity'));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return app('VoyagerAuth')->user()->can('browse', new Product());
    }
}
