<?php

namespace App\Http\Controllers\Voyager;

use App\Models\ProductDetail;
use App\Traits\UploadImage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

class ProductVoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
	use UploadImage;
	
    /**
     * Update data.
     */
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }
// dd($request->all());
        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        if ($request->attribute) {
            $this->updateProductAttributes($request, $data);
        }

        event(new BreadDataUpdated($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    /**
     * Store new data.
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));
        if ($request->attribute) {
            $this->updateProductAttributes($request, $data);
        }
        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    protected function updateProductAttributes(Request $request, $data): void
    {
        $attribute = $request->attribute;
        $productId = $data->id;
        ProductDetail::where('product_id', $productId)->delete();

        foreach ($attribute as $key => $row) {

            // if (isset($row['image']) and is_file($row['image'])):
            //     $imagePath = $this->handleUpload($row['image'], 'products');
            //     $row['image'] = $imagePath;
            // endif;

			if (isset($row['images']) and count($row['images']) > 0):
                $arrayImages = [];
                foreach ($row['images'] as $singleImage) {
                    if (is_file($singleImage)):
						array_push($arrayImages, $this->handleUpload($singleImage, 'products'));
					else:
						array_push($arrayImages, $singleImage);
                    endif;
                }
                $row['images'] = json_encode($arrayImages);
            endif;

            ProductDetail::updateOrCreate(
                [
                    'product_id' => $productId,
                    'id' => $row['id'],
                ],
                $row
            );
        }
    }
}
