<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Serial;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\json_encode;
use TCG\Voyager\Events\BreadDataAdded;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Events\BreadDataUpdated;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderInformationVoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function changeSerialStatusToOne(Request $request): void
    {
        $serial             = $request->serial;
        $orderInformationId = $request->order_information_id;

        Serial::whereSerial($serial)->update(['status' => 1, 'order_information_id' => $orderInformationId]);
    }

    public function changeSerialStatusToZero(Request $request): void
    {
        $serial = $request->serial;

        Serial::whereSerial($serial)->update(['status' => 0, 'order_information_id' => null]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val  = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        // Log::useDailyFiles(storage_path().'/logs/order-information-activities.log');
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
		$id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
		
		Log::channel('single')->info("Update: ".json_encode($request->all()));
		
        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }
}
