<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Serial;
use App\Models\Product;
use App\Models\ProductInput;
use Illuminate\Http\Request;
use App\Models\ProductDetail;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Http\JsonResponse;
use TCG\Voyager\Events\BreadDataAdded;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Events\BreadDataDeleted;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductInputVoyagerController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
	//API
	public function getProductInput(Request $request)
    {
		$productInput = ProductInput::with('serials', 'productDetail', 'productDetail.product', 'productDetail.product.category')->find($request->product_input_id);;
		
		$productIdArray = Product::with('productDetail')->whereCategoryId($productInput->productDetail->product->category->id)->get()->pluck('id');
		$productDetails = ProductDetail::has('product')->with('product')->whereIn('product_id', $productIdArray)->get();

        return compact('productInput', 'productDetails');
	}
	
	//API
	public function uniqueSerial(Request $request)
    {
        $request->validate([
            'serials' => 'unique:serials,serial',
        ]);

        return ['status' => true];
	}
	
	//API
	public function removeSerial(Request $request)
    {
		// $product_detail_id = $request->product_detail_id;
		$serial = $request->serial;

        return Serial::where('serial', $serial)->delete();
	}
	
	//API
	public function productInputSubmit(Request $request)
    {
		$product_input_id = $request->product_input_id;
		$product_detail_id = $request->product_detail_id;
		$quantity = $request->quantity;

		if($product_input_id):
			$productInput = ProductInput::find($product_input_id);
			$quantity = $request->quantity - $productInput->quantity;//positive if add, negative if deleted
		endif;

        $productInput = ProductInput::updateOrCreate(
			[
				'id' => (int) $product_input_id
			],
			[
				'product_detail_id' => $product_detail_id,
				'quantity' => $request->quantity
			]
		);

		$this->updateQuantity($product_detail_id, $quantity);
        
        foreach ($request->serials as $serial) {
            $productInput->serials()->updateOrCreate(
				[
					'product_input_id' => $product_input_id,
					'serial'            => $serial
				],
				['product_detail_id' => $product_detail_id]
			);
		}
		
		return ['status' => 'success'];
	}

    /**
     * Add quantity to Product Detail using product_detail_id.
     */
    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val  = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        //update Quantity
        $this->updateQuantity($request->product_detail_id, $request->quantity);

        event(new BreadDataAdded($dataType, $data));

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    protected function updateQuantity($product_detail_id, $quantity)
    {
        $productDetail = ProductDetail::whereId($product_detail_id)->first();
        $productDetail->quantity += (int) $quantity;
        $productDetail->save();
    }

    protected function removeQuantity($product_detail_id, $quantity)
    {
		$productDetail           = ProductDetail::whereId($product_detail_id)->first();

		if($productDetail->quantity < (int) $quantity){
			return [
                'message'    => "Can't delete. Instock quantity of {$productDetail->product->name} is smaller than this Input",
                'alert-type' => 'error',
            ];
		}

        $productDetail->quantity -= (int) $quantity;

        $productDetail->save();
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses($model)))) {
				//###remove
				$response = $this->removeQuantity($data->product_detail_id, $data->quantity);
				if($response['alert-type'] == 'error'):
					return redirect()->route("voyager.{$dataType->slug}.index")->with($response);
				endif;

                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res  = $data->destroy($ids);
        $data = $res
        ? [
            'message'    => __('voyager::generic.successfully_deleted') . " {$displayName}",
            'alert-type' => 'success',
        ]
        : [
            'message'    => __('voyager::generic.error_deleting') . " {$displayName}",
            'alert-type' => 'error',
        ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    /**
     * Get BREAD relations data.
     * ### Customize return text (1.controller)
     * ### Need to add `fulldata` in app.js to work (search keyword: items-route) (2.js)
     * ### And create custom relationship-product-detail file (3.view)
     *
     * @param Request $request
     * @return json
     */
    public function relation(Request $request): JsonResponse
    {
        $slug     = $this->getSlug($request);
        $page     = $request->input('page');
        $fulldata = $request->fulldata;
        $on_page  = 50;
        $search   = $request->input('search', false);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        foreach ($dataType->editRows as $key => $row) {
            if ($row->field === $request->input('type')) {
                $options = $row->details;
                $skip    = $on_page * ($page - 1);

                // If search query, use LIKE to filter results depending on field label
                if ($search) {
                    if ($fulldata):
                        $total_count         = app('App\Models\Product')->where('name', 'LIKE', '%' . $search . '%')->count();
                        $relationshipOptions = app($options->model)
                            ->join('products', 'products.id', '=', 'product_id')
                            ->take($on_page)
                            ->skip($skip)
                            ->where('products.name', 'LIKE', '%' . $search . '%')
                            ->get();
                    else:
                        $total_count         = app($options->model)->where($options->label, 'LIKE', '%' . $search . '%')->count();
                        $relationshipOptions = app($options->model)->take($on_page)->skip($skip)
                            ->where($options->label, 'LIKE', '%' . $search . '%')
                            ->get();
                    endif;
                } else {
                    $total_count         = app($options->model)->count();
                    $relationshipOptions = app($options->model)->take($on_page)->skip($skip)->get();
                }

                $results = [];

                foreach ($relationshipOptions as $relationshipOption) {
                    /* ### Customize return text */
                    if ($fulldata):
                        $text      = "{$relationshipOption->product->name} - Size:{$relationshipOption->size->name} - Color:{$relationshipOption->color->name} - Warranty:{$relationshipOption->warranty} - Price:".number_format($relationshipOption->price)." ".config('cart.currency');
                        $results[] = [
                            'id'   => $relationshipOption->{$options->key},
                            'text' => $text,
                        ];
                    else:
                        $results[] = [
                            'id'   => $relationshipOption->{$options->key},
                            'text' => $relationshipOption->{$options->label},
                        ];
                    endif;
                }

                return response()->json([
                    'results'    => $results,
                    'pagination' => [
                        'more' => ($total_count > ($skip + $on_page)),
                    ],
                ]);
            }
        }

        // No result found, return empty array
        return response()->json([], 404);
    }
}
