<?php

namespace App\Listeners;

use App\Mail\OrderConfirmAdmin;
use App\Mail\OrderConfirmCustomer;
use App\Traits\MailCheck;
use Illuminate\Support\Facades\Mail;

class sendEmailCheckoutSuccessListener
{
    use MailCheck;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if ($this->hasSMTPaccount() and $this->checkSMTPconnection() and $event->orderInfo->email):
            Mail::send(new OrderConfirmCustomer($event->orderInfo));
            Mail::send(new OrderConfirmAdmin($event->orderInfo));
        endif;
    }
}
