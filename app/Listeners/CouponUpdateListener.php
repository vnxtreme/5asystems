<?php

namespace App\Listeners;

use App\Models\Coupon;
use Gloudemans\Shoppingcart\Facades\Cart;

class CouponUpdateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $couponName = session()->get('coupon')['name'];

        if ($couponName and Cart::count() > 0):
            $coupon = Coupon::where('code', $couponName)->first();
            session()->put('coupon', [
                'name'     => $coupon->code,
                'discount' => $coupon->discount(Cart::subtotal()),
            ]);
        else:
            session()->forget('coupon');
        endif;
    }
}
