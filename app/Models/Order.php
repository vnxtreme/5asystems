<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $guarded = [];
	
	public function productDetail()
	{
		return $this->belongsTo('App\Models\ProductDetail');
	}
}
