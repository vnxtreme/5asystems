<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProductCategory extends Model
{
    // protected $with = ['products'];

    public function subCategory()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function allSubCategories()
    {
        return $this->subCategory()->with('allSubCategories');
    }

    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id')
			->has('productDetail')
			->active()
            ->latest();
    }

    public function productsOfSubCategory()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    public function scopeSortQuery(Builder $query, $column='order_by')
    {
        return $query->orderByRaw("-{$column} DESC");
    }
}
