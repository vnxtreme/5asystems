<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    // protected $guarded = [];
    protected $fillable = [
		'user_id',
        'first_name',
        'last_name',
		'company',
        'phone',
        'address',
        'city',
		'postal_code',
        'default',
        'state_or_province_code'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function getFullnameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
