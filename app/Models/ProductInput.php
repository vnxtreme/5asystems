<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductInput extends Model
{
	protected $fillable = ['product_detail_id', 'quantity'];

    public function serials()
	{
		return $this->hasMany('App\Models\Serial', 'product_input_id', 'id');
	}

	public function productDetail()
	{
		return $this->hasOne('App\Models\ProductDetail', 'id', 'product_detail_id');
	}
}
