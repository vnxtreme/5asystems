<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInformation extends Model
{
	protected $guarded = [];
	protected $with = ['order'];
	
	public function order()
	{
		return $this->hasMany('App\Models\Order');
	}
}
