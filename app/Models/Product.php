<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use Translatable,
        Resizable;

    protected $guarded = [];
	
	protected $load = ['productDetail', 'firstProductDetail'];

    public function scopeIsVisible(Builder $query)
    {
        return $query->where('status', 1);
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ProductCategory');
    }

    public function productDetail()
    {
        return $this->hasMany('App\Models\ProductDetail', 'product_id', 'id');
    }
    
    public function firstProductDetail()
    {
        return $this->hasOne('App\Models\ProductDetail', 'product_id', 'id');
	}
	
	public function scopeRelatedProducts(Builder $query, $product)
	{
		return $query->has('productDetail')->active()->where('category_id', $product->category_id)->where('slug', '!=', $product->slug)->inRandomOrder()->limit(9)->get();
	}

	public function scopeActive(Builder $query)
	{
		return $query->has('productDetail')->whereStatus(1);
	}

	public function scopeFeatured(Builder $query)
	{
		return $query->whereFeatured(1)->limit(7);
	}

	public function scopeIsPopular(Builder $query)
	{
		return $query->whereIsPopular(1)->limit(9);
	}

	public function scopeIsBestseller(Builder $query)
	{
		return $query->whereIsBestseller(1)->limit(9);
	}

	public function scopeIsNew(Builder $query)
	{
		return $query->latest('created_at')->limit(6);
	}
}
