<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UnitConversion;

class Coupon extends Model
{
	use UnitConversion;

    public function discount(String $total)
    {
		$total = $this->stringToFloat($total);

        switch ($this->type) {
            case 'percentage':
                return round($total * $this->value / 100, 2);
                break;

			case 'number':
                return $this->value > $total ? $total : $this->value;
                break;

            default:
                return 0;
                break;
        }
    }
}
