<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable;

	protected $with = ['addresses'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
	// ];
	protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
	];
	
	public function addresses()
	{
		return $this->hasMany('App\Models\Address', 'user_id', 'id');
	}

	public function getFullnameAttribute()
	{
		return "{$this->first_name} {$this->last_name}";
	}

	public function scopeRoleNotNull(Builder $query)
	{
		return $query->whereNotNull('role_id')->where('role_id', '>=', Auth::user()->role_id);
	}
}
