<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    public function scopeHomePosts(Builder $query)
	{
		return $query->active()->latest()->limit(6);
	}

	public function scopeActive(Builder $query)
	{
		return $query->whereStatus('PUBLISHED');
	}

	public function author()
	{
		return $this->belongsTo('App\Models\User', 'author_id', 'id');
	}
}
