<?php

namespace App\Models;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model implements Buyable
{
    // protected $fillable = ['product_id', 'size_id', 'color_id', 'warranty', 'unit_id', 'quantity', 'price', 'group_key'];
    protected $guarded = [];
    protected $with = ['color', 'size'];

    public function color()
    {
        return $this->belongsTo('App\Models\Color');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\Size');
    }

    // public function unit()
    // {
    //     return $this->belongsTo('App\Models\Unit');
    // }

    public function product()
    {
        return $this->belongsTo('App\Models\Product')->active();
    }

    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    public function getBuyableDescription($options = null)
    {
        return $this->product->name;
    }

    public function getBuyablePrice($options = null)
    {
        return $this->price;
	}
	
	public function activeSerials()
	{
		return $this->hasMany('App\Models\Serial', 'product_detail_id', 'id')->where('status', 0);
	}

	public function serials()
	{
		return $this->hasMany('App\Models\Serial', 'product_detail_id', 'id');
	}
}
