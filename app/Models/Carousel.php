<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Carousel extends Model
{
    public function scopeSortQuery(Builder $query, $column='order_by')
    {
        return $query->orderByRaw("-{$column} DESC");
    }
}
