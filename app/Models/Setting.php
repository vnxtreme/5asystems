<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Setting extends Model
{
    public function scopePackageDimension(Builder $query)
	{
		return $query->whereGroup('Package Dimension')->get();
	}
}
