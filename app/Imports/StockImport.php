<?php

namespace App\Imports;

use App\Models\ProductInput;
use Maatwebsite\Excel\Concerns\ToModel;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Facades\DB;
use App\Models\ProductDetail;

class StockImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) or $row[0] == 'Product detail ID') {
			return null;
        }
        $serials = json_decode($row[1]);

        DB::transaction(function() use ($row, $serials){
            $productInput = ProductInput::create([
                'product_detail_id' => (int) $row[0],
                'quantity' => count($serials)
            ]);

            $productDetail = ProductDetail::find($row[0]);
            $productDetail->quantity += count($serials);
            $productDetail->save();

            foreach ($serials as $serial) {
                $productInput->serials()->create([
                    'product_input_id' => $productInput->id,
                    'product_detail_id' => $row[0],
                    'serial' => $serial,
                ]);
            }
            return $productInput;
        });   
    }
}
