<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Str;

class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
		if (!isset($row[0]) or !isset($row[1]) or $row[0] == 'ID') {
			return null;
		}

        $product =  new Product([
            'name' => $row[1],
            'slug' => Str::slug($row[1]),
            'category_id' => $row[2],
            'excerpt' => $row[3],
            'content' => $row[4],
            'seo_title' => $row[5],
            'meta_description' => $row[6],
            'meta_keywords' => $row[7],
            'image' => $row[8],
            'image_fb' => $row[9],
			'featured' => $row[10],
			'weight' => $row[11] ?? 1000,
		]);
		$product->save();
		
		$product->productDetail()->create([
			'color_id' => $row[12],
            'size_id' => $row[13],
            'warranty' => $row[14],
            'quantity' => $row[15],
            'price' => $row[16],
		]);
		
		return $product;
    }
}
