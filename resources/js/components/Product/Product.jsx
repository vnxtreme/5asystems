import React, { Component } from 'react';
import InputTypeAllocator from './InputTypeAllocator';

export default class Product extends Component {
    constructor(props) {
        super(props);

        this.state = {
            formInputs: [
				{
                    name: 'SKU',
                    columnName: 'id',
                    default: '',
                    type: 'text',
                    class: 'col-sm-2',
					data: null,
					readonly: true
                },
                {
                    name: 'color',
                    columnName: 'color_id',
                    default: '',
                    type: 'dropdown',
                    class: 'col-sm-2',
                    url: '/api/get-colors',
                    data: null,
                },
                {
                    name: 'size',
                    columnName: 'size_id',
                    default: '',
                    type: 'dropdown',
                    class: 'col-sm-2',
                    url: '/api/get-sizes',
                    data: null,
                },
                {
                    name: 'price',
                    columnName: 'price',
                    default: 0,
                    type: 'double',
                    class: 'col-sm-2',
                    data: null,
                },
                {
                    name: 'warranty',
                    columnName: 'warranty',
                    default: '12',
                    type: 'number',
                    class: 'col-sm-2',
                    data: null,
				},
				{
                    name: 'Quantity',
                    columnName: 'quantity',
                    default: 0,
                    type: 'text',
                    class: 'col-sm-2 ',
					data: null,
					disabled: true,
                },
                {
                    name: 'images',
                    columnName: 'images',
                    default: '',
                    type: 'multiple-image',
                    class: 'col-sm-7',
                    data: null,
                },
            ],
            rows: [],
        };

        this.addNewRow = this.addNewRow.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    addNewRow() {
        this.setState(prevState => ({
            rows: [...prevState.rows, prevState.formInputs],
        }));
    }

    handleDelete(e) {
        const answer = confirm('Are you sure?');
        if (answer) {
            if (this.state.rows.length < 2) {
                alert('Product must have at least 1 attribute');
                return false;
            }
            let groupIndex = e.target.getAttribute('data-groupindex');
            let rows = [...this.state.rows];
            rows.splice(groupIndex, 1);
            this.setState(prevState => ({ rows: rows }));
        }
    }

    async componentDidMount() {
		for (const attribute of this.state.formInputs) {
			if (attribute.url) {
                const { data } = await axios.get(attribute.url);
                attribute.data = data;
            }
		}

        if (document.getElementById('product_id')) {
            const productId = document.getElementById('product_id').value;
            const { data: product } = await axios.post('/api/get-product-info', { id: productId });
            const productDetails = product.product_detail;

            let formInputs = [...this.state.formInputs];
            let arrayHolder = [];
            productDetails.forEach((productDetail, index) => {
                arrayHolder[index] = [];
                formInputs.forEach((inputInfo, key) => {
                    let newInputInfo = { ...inputInfo };
                    newInputInfo.default = productDetail[inputInfo.columnName];
                    arrayHolder[index] = [...arrayHolder[index], { ...newInputInfo }];
                });
            });
// console.log(arrayHolder)
            this.setState(prevState => ({ rows: [...prevState.rows, ...arrayHolder] }));
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className='panel-heading'>
                    <h3 className='panel-title'>Attributes</h3>
                    <div className='panel-actions'>
                        <a className='panel-action btn btn-success' onClick={this.addNewRow}>
                            Add
                        </a>
                    </div>
                </div>
                <div className='panel-body'>
                    
                        {this.state.rows &&
                            this.state.rows.map((row, groupIndex) => {
                                return (
                                    <React.Fragment key={groupIndex}>
										<div className='row'>
                                        <div className='col-sm-11 pl-0'>
                                            {row.map((attribute, index) => {
                                                return <InputTypeAllocator key={index} groupIndex={groupIndex} attribute={attribute} />;
                                            })}
                                        </div>
                                        <div className='col-sm-1 pl-0 text-center'>
                                            <label>Delete</label>
                                            <a style={{margin:0}} className='btn btn-warning form-control' onClick={this.handleDelete} data-groupindex={groupIndex} >X</a>
                                        </div>
										</div>
										<hr/>
                                    </React.Fragment>
                                );
                            })}
                    
                </div>
            </React.Fragment>
        );
    }
}
