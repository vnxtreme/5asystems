import React from 'react';
import Dropdown from './InputTypes/Dropdown';
import Input from './InputTypes/Input';
import Double from './InputTypes/Double';
import Thumbnail from './InputTypes/Thumbnail';
import MultipleImage from './InputTypes/MultipleImage';

export default function InputTypeAllocator(props) {
    switch (props.attribute.type) {
        case 'dropdown':
            return <Dropdown groupIndex={props.groupIndex} attribute={props.attribute} />;
            break;

        case 'double':
            return <Double groupIndex={props.groupIndex} attribute={props.attribute} />;
            break;

        case 'thumbnail':
            return <Thumbnail groupIndex={props.groupIndex} attribute={props.attribute} />;
            break;

        case 'multiple-image':
            return <MultipleImage groupIndex={props.groupIndex} attribute={props.attribute} />;
			break;

        default:
            return <Input groupIndex={props.groupIndex} attribute={props.attribute} />;
            break;
    }
}
