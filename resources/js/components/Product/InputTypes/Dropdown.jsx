import React, { Component } from 'react';

export default class Dropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
			options: [],
			value: ''
        };

        this.handleDropdownOnchange = this.handleDropdownOnchange.bind(this);
    }

    handleDropdownOnchange(e) {
        this.setState({
            value: e.target.value,
        });
	}
	
	componentDidMount(){
		this.setState({
            value: this.props.attribute.default,
        });
	}

    render() {
        const { attribute, groupIndex } = this.props;

        return (
            <div className={`form-group ${attribute.class}`}>
                <label htmlFor={attribute.name}>{attribute.name.charAt(0).toUpperCase() + attribute.name.slice(1)}</label>
                <select className='form-control' name={`attribute[${groupIndex}][${attribute.columnName}]`} onChange={this.handleDropdownOnchange} value={this.state.value}>
                    {attribute.data &&
                        attribute.data.map((option, key) => (
                            <option key={key} value={option.id}>
                                {option.name}
                            </option>
                        ))}
                </select>
            </div>
        );
    }
}
