import React, { Component } from 'react';

export default class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: null,
        };

        this.handleInputOnchange = this.handleInputOnchange.bind(this);
    }

    handleInputOnchange(e) {
        this.setState({ value: e.target.value });
    }

	componentDidMount(){
		// this.setState({ value: this.props.attribute.default });
	}

    render() {
        const { attribute, groupIndex } = this.props;

        return (
            <div className={`form-group ${attribute.class}`}>
                <label htmlFor={attribute.name}>{attribute.name.charAt(0).toUpperCase() + attribute.name.slice(1)}</label>
                <input
                    className='form-control'
                    name={`attribute[${groupIndex}][${attribute.columnName}]`}
                    type={attribute.type}
                    value={this.state.value || attribute.default}
					onChange={this.handleInputOnchange}
					disabled={attribute.disabled}
					readOnly={attribute.readonly}
                />
            </div>
        );
    }
}
