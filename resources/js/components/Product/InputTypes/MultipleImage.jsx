import React, { Component } from 'react';
import { isUrl } from '../../../utils/common';

export default class MultipleImage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images: '',
        };
    }

    async componentDidMount() {
        if (this.props.attribute.default && this.props.attribute.default.length > 0) {
            let multiImages = JSON.parse(this.props.attribute.default);
            await this.setState({ images: multiImages });
            console.log(this.state.images);
        }
    }

    render() {
        const { attribute, groupIndex } = this.props;
        const { images } = this.state;
        return (
            <div className={`form-group ${attribute.class}`}>
                <label htmlFor={attribute.name}>{attribute.name.charAt(0).toUpperCase() + attribute.name.slice(1)}</label>
                <br />
                {images &&
                    images.map((image, index) => {
                        return (
                            <React.Fragment key={index}>
                                <img src={image.includes('data:image') ? image : `/storage/${image}`} alt='' width='100' />
                                <input type='hidden' name={`attribute[${groupIndex}][${attribute.columnName}][]`} value={image} />
                            </React.Fragment>
                        );
                    })}

                <input
                    className='form-control'
                    name={`attribute[${groupIndex}][${attribute.columnName}][]`}
                    type='file'
                    accept='image/*'
                    multiple
                    // onChange={this.handleInputOnchange}
                />
            </div>
        );
    }
}
