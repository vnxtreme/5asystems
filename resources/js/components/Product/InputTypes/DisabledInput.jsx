import React, { Component } from 'react';

export default class DisabledInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: null,
        };
    }

    render() {
        const { attribute, groupIndex } = this.props;

        return (
            <div className={`form-group ${attribute.class}`}>
                <label htmlFor={attribute.name}>{attribute.name.charAt(0).toUpperCase() + attribute.name.slice(1)}</label>
                <input
                    className='form-control'
                    name={`attribute[${groupIndex}][${attribute.columnName}]`}
                    type={attribute.type}
					value={this.state.value || attribute.default}
					disabled
                />
            </div>
        );
    }
}
