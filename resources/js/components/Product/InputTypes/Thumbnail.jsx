import React, { Component } from 'react';
import { isUrl } from '../../../utils/common';

export default class Thumbnail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            image: '',
        };

        this.handleInputOnchange = this.handleInputOnchange.bind(this);
    }

    async handleInputOnchange(element) {
        let fileReader = new FileReader();
        fileReader.onloadend = fileElement => {
            this.setState({ image: fileElement.target.result });
        };
        fileReader.readAsDataURL(element.target.files[0]);
    }

    async componentDidMount() {
        await this.setState({ image: this.props.attribute.default });
    }

    render() {
        const { attribute, groupIndex } = this.props;
        const { image } = this.state;
        return (
            <div className={`form-group ${attribute.class}`}>
                <label htmlFor={attribute.name}>{attribute.name.charAt(0).toUpperCase() + attribute.name.slice(1)}</label>
                <br />
                {image && <img src={image.includes('data:image') ? image : `/storage/${image}`} alt='' width='100' />}
                {image && <input type='hidden' name={`attribute[${groupIndex}][${attribute.columnName}]`} value={image} />}
                
                <input
                    className='form-control'
                    name={`attribute[${groupIndex}][${attribute.columnName}]`}
                    type='file'
                    accept='image/*'
                    onChange={this.handleInputOnchange}
                />
            </div>
        );
    }
}
