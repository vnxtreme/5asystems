import React, { Component } from 'react';
import SerialInput from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';

export default class ProductInputs extends Component {
	constructor(props) {
		super(props);

		this.state = {
			productCategories: [],
			productDetails: [],
			currentProductDetailId: null,
			categoryId: null,
			serials: [],
			productInputId: null
		};

		this.getProductDetails = this.getProductDetails.bind(this);
		this.selectedProductDetailId = this.selectedProductDetailId.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		const productInputId = document.getElementById('product_input_id').value;
		if (productInputId) {
			
			axios.post('/api/product-input', { product_input_id: productInputId })
				.then(({ data }) => {
					this.setState({ 
						currentProductDetailId: data.productInput.product_detail_id,
						categoryId: data.productInput.product_detail.product.category.id, 
						productDetails: data.productDetails,
						serials: data.productInput.serials.map(row => row.serial),
						productInputId: productInputId
					});
				});
		}
		axios.get('/api/get-product-categories').then(({ data }) => {
			this.setState({ productCategories: data });
		});
	}

	getProductDetails(e) {
		let categoryId = e.target.value;
		this.setState({categoryId});
		axios.post('/api/list-product-details', { category_id: categoryId }).then(({ data }) => {
			this.setState({ productDetails: data });
		});
	}

	selectedProductDetailId(e) {
		this.setState({ currentProductDetailId: e.target.value });
	}

	handleSubmit() {
		if (this.state.currentProductDetailId && this.state.serials.length) {
			const data = {
				product_input_id: this.state.productInputId,
				product_detail_id: this.state.currentProductDetailId, 
				serials: this.state.serials, 
				quantity: this.state.serials.length
			};
			axios.post('/api/product-input/submit', data)
				.then(response => {
					window.location.href = '/admin/product-inputs';
				})
				.catch(error => {
					console.log(error)
				})
		}
	}

	async handleChange(serials) {
		if (serials.length > this.state.serials.length) {
			//add
			try {
				let { data } = await axios.post('/api/product-input/check-serial-unique', { serials: serials[serials.length - 1] });

				this.setState({ serials });
			} catch (e) {
				alert(Object.values(e.response.data.errors)[0][0]);
			}
		} else {
			//delete
			let { data } = await axios.post('/api/product-input/remove-serial', { serial: this.state.serials[this.state.serials.length - 1] });
			this.setState({ serials });
		}
	}

	render() {
		return (
			<div className='panel panel-bordered'>
				<div className='panel-body'>
					<div className='form-group col-md-12'>
						<label className='control-label'>Category</label>
						<select className='form-control' name='category_id' onChange={this.getProductDetails} required value={this.state.categoryId || ''}>
							<option value=''>--</option>
							{this.state.productCategories &&
								this.state.productCategories.map(category => (
									<React.Fragment key={category.id}>
										<option value={category.id} /* selected={this.state.categoryId == category.id ? 'selected' : ''} */>{category.name}</option>
										{category.sub_category &&
											category.sub_category.map(subCategory => (
												<option key={subCategory.id} value={subCategory.id} /* selected={this.state.categoryId == subCategory.id ? 'selected' : ''} */>
													-- {subCategory.name}
												</option>
											))}
									</React.Fragment>
								))}
						</select>
					</div>

					<div className='form-group col-md-12'>
						<label className='control-label'>Product</label>
						<select className='form-control' name='product_detail_id' onChange={this.selectedProductDetailId} value={this.state.currentProductDetailId || ''} required>
							<option value=''>--</option>
							{this.state.productDetails &&
								this.state.productDetails.map(productDetail => (
									<option key={productDetail.id} value={productDetail.id}>{`${productDetail.id} - ${productDetail.product.name} - ${productDetail.color.name} - ${productDetail.size.name}`}</option>
								))}
						</select>
					</div>

					<div className='form-group col-md-12 '>
						<input type="hidden" name="quantity" value={this.state.serials} />
						<label className='control-label'>Product Serials ({this.state.serials.length})</label>
						<SerialInput
							value={this.state.serials}
							onChange={this.handleChange}
							onlyUnique='true'
							inputProps={{ placeholder: 'Add serial' }}
						/>
					</div>
				</div>

				<div className='panel-footer'>
					<button type='button' className='btn btn-primary save' onClick={this.handleSubmit}>
						Save
                    </button>
				</div>
			</div>
		);
	}
}
