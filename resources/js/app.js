/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Product from './components/Product/Product';
import ProductInput from './components/ProductInput/ProductInput';


if (document.getElementById('product_attributes')) {
    ReactDOM.render(<Product />, document.getElementById('product_attributes'));
}

// if (document.getElementById('serial')) {
//     ReactDOM.render(<InputTag/>, document.getElementById('serial'));
// }

if (document.getElementById('product_inputs')) {
    ReactDOM.render(<ProductInput/>, document.getElementById('product_inputs'));
}