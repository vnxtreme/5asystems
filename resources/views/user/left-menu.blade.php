<div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
	<div class="block-categories hidden-sm-down">
		<h4 class="category-title" href="{{route('user.account')}}">User</h4>
		<ul class="category-top-menu">
			<li>
				<ul class="category-sub-menu">
					<li data-depth="0">
						<a href="{{route('user.profile')}}">Profile</a>
					</li>
					<li data-depth="0">
						<a href="{{route('user.addresses')}}">Addresses</a>
					</li>
					<li data-depth="0">
						<a href="{{route('user.order-history')}}">Order history</a>
					</li>
					<li data-depth="0">
						<a href="{{ route('logout') }}"
							onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							Sign out
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>