@extends('_layout')
@section('content')
<script>
	document.body.id = 'my-account';
	document.body.classList = 'layout-left-column page-my-account'
</script>

@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Your account</h1>
		</header>

		<section id="content" class="page-content">
			<div class="row">
				<div class="links">

					<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link"
						href="{{route('user.profile')}}">
						<span class="link-item">
							<i class="material-icons"></i>
							Profile
						</span>
					</a>

					<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="addresses-link"
						href="{{route('user.addresses')}}">
						<span class="link-item">
							<i class="material-icons"></i>
							Addresses
						</span>
					</a>

					<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="history-link"
						href="{{route('user.order-history')}}">
						<span class="link-item">
							<i class="material-icons"></i>
							Order history
						</span>
					</a>
				</div>
			</div>
		</section>

	</section>
</div>

@endsection