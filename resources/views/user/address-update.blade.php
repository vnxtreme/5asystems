@extends('_layout')
@section('content')
<script>
	document.body.id = 'addresses';
	document.body.classList = 'page-addresses'
</script>

@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Update your address</h1>
		</header>

		<section id="content" class="page-content">
			@include('user.notification')

			<div class="address-form">
				<div class="js-address-form">
					<form method="POST" action="{{route('user.addresses.update', ['address_id' => $address->id])}}" >
						@csrf
						<section class="form-fields">
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required"> First name</label>
								<div class="col-md-6">
									<input class="form-control" name="first_name" type="text" value="{{$address->first_name}}"
										maxlength="32" required="">
								</div>
								<div class="col-md-3 form-control-comment"></div>
							</div>
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required"> Last name</label>
								<div class="col-md-6">
									<input class="form-control" name="last_name" type="text" value="{{$address->last_name}}"
										maxlength="32" required="">
								</div>

								<div class="col-md-3 form-control-comment"></div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									Address
								</label>
								<div class="col-md-6">
									<input class="form-control" name="address" type="text" value="{{$address->address}}"
										maxlength="128" required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									Zip/Postal Code
								</label>
								<div class="col-md-6">
									<input class="form-control" name="postal_code" type="text" value="{{$address->postal_code}}" maxlength="12"
										required="">
								</div>
							</div>
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									City
								</label>
								<div class="col-md-6">
									<input class="form-control" name="city" type="text" value="{{$address->city}}" maxlength="64"
										required="">
								</div>
							</div>
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									State Or Province Code
								</label>
								<div class="col-md-6">
									<input class="form-control" name="state_or_province_code" type="text" value="{{$address->state_or_province_code}}" maxlength="64"
										required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label">
									Phone
								</label>
								<div class="col-md-6">
									<input class="form-control" name="phone" type="text" value="{{$address->phone}}" maxlength="32">
								</div>

								<div class="col-md-3 form-control-comment">Optional</div>
							</div>

						</section>

						<footer class="form-footer clearfix">
							<button class="btn btn-primary float-xs-right" type="submit">Save</button>
						</footer>
					</form>
				</div>
			</div>
		</section>
	</section>
</div>

@endsection