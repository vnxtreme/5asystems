@extends('_layout')
@section('content')
<script>
	document.body.id = 'addresses';
	document.body.classList = 'page-addresses'
</script>

@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">
				Your address
			</h1>
		</header>

		<section id="content" class="page-content">
			@include('user.notification')

			@forelse ($addresses as $address)
			<div class="col-lg-4 col-md-6 col-sm-6">
				<article id="address-17" class="address" data-id-address="17">
					<div class="address-body">
						<h4>{{$address->fullname}}</h4>
						<address>{{$address->address}}<br>{{$address->city}}
							<br>{{$address->postal_code}}<br>{{$address->phone}}</address>
					</div>

					<div class="address-footer">
						<a href="{{route('user.addresses.update', ['address_id' => $address->id])}}">
							<i class="material-icons"></i>
							<span>Update</span>
						</a>
						<a href="{{route('user.addresses.delete', ['address_id' => $address->id])}}">
							<i class="material-icons"></i>
							<span>Delete</span>
						</a>
					</div>
				</article>
			</div>
			@empty

			@endforelse
			<div class="clearfix"></div>

			<div class="addresses-footer">
				<a href="{{route('user.addresses.add')}}" data-link-action="add-address">
					<i class="material-icons"></i>
					<span>Create new address</span>
				</a>
			</div>
		</section>
	</section>
</div>

@endsection