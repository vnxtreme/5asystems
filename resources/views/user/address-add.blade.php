@extends('_layout')
@section('content')
<script>
	document.body.id = 'addresses';
	document.body.classList = 'page-addresses'
</script>

@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Add address</h1>
		</header>

		<section id="content" class="page-content">
			@include('user.notification')

			<div class="address-form">
				<div class="js-address-form">
					<form method="POST" action="{{route('user.addresses.add')}}">
						@csrf
						<section class="form-fields">
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required"> Firstname</label>
								<div class="col-md-6">
									<input class="form-control" name="first_name" type="text" maxlength="32"
										required="">
								</div>
							</div>
							<div class="form-group row ">
								<label class="col-md-3 form-control-label required"> Lastname</label>
								<div class="col-md-6">
									<input class="form-control" name="last_name" type="text" maxlength="32" required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									Address
								</label>
								<div class="col-md-6">
									<input class="form-control" name="address" type="text" maxlength="128" required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									City
								</label>
								<div class="col-md-6">
									<input class="form-control" name="city" type="text" maxlength="64" required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									State Or Province Code
								</label>
								<div class="col-md-6">
									<input class="form-control" name="state_or_province_code" type="text" maxlength="64" required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label required">
									Zip/Postal Code
								</label>
								<div class="col-md-6">
									<input class="form-control" name="postal_code" type="text" maxlength="12"
										required="">
								</div>
							</div>

							<div class="form-group row ">
								<label class="col-md-3 form-control-label">
									Phone
								</label>
								<div class="col-md-6">
									<input class="form-control" name="phone" type="text" maxlength="32">
								</div>

								<div class="col-md-3 form-control-comment">Optional</div>
							</div>

						</section>

						<footer class="form-footer clearfix">
							<button class="btn btn-primary float-xs-right" type="submit">Save</button>
						</footer>
					</form>
				</div>
			</div>
		</section>
	</section>
</div>

@endsection