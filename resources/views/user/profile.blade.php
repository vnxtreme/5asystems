@extends('_layout')
@section('content')
<script>
	document.body.id = 'authentication';
	document.body.classList = 'page-identity page-customer-account'
</script>
@php
$user = Auth::user();
@endphp
@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Your Personal Information</h1>
		</header>

		<section id="content" class="page-content">
			@include('user.notification')

			<form action="{{route('user.profile.update')}}" id="customer-form" class="js-customer-form" method="POST"
				autocomplete="off">
				@csrf
				<section>
					<div class="form-group row ">
						<label class="col-md-3 form-control-label">Social title</label>
						<div class="col-md-6 form-control-valign">
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="1" {{$user->gender==1 ? "checked" : ''}}>
									<span></span>
								</span>
								Mr.
							</label>
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="2" {{$user->gender==2 ? "checked" : ''}}>
									<span></span>
								</span>
								Mrs.
							</label>
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">
							First name
						</label>
						<div class="col-md-6">
							<input class="form-control" name="first_name" type="text" value="{{$user->first_name}}"
								required="">
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">
							Last name
						</label>
						<div class="col-md-6">
							<input class="form-control" name="last_name" type="text" value="{{$user->last_name}}"
								required="">
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Email</label>
						<div class="col-md-6">
							<input class="form-control" name="email" type="email" value="{{$user->email}}" disabled>
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Password</label>
						<div class="col-md-6">
							<div class="input-group js-parent-focus">
								<input class="form-control js-child-focus js-visible-password" name="password"
									type="password" value="*********" pattern=".{5,}" disabled>
							</div>
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label">New password</label>
						<div class="col-md-6">
							<div class="input-group js-parent-focus">
								<input class="form-control js-child-focus js-visible-password" name="new_password"
									type="password" pattern=".{5,}">
								<span class="input-group-btn">
									<button class="btn" type="button" data-action="show-password" data-text-show="Show"
										data-text-hide="Hide">Show</button>
								</span>
							</div>
						</div>

						<div class="col-md-3 form-control-comment">
							Optional
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label">Birthdate</label>
						<div class="col-md-6">
							<input class="form-control" name="birthdate" type="date" placeholder="MM/DD/YYYY" value={{old('birthdate') ?? $user->birthdate}}>
							<span class="form-control-comment">(E.g.: 05/31/1980)</span>
						</div>

						<div class="col-md-3 form-control-comment">
							Optional
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label">
						</label>
						<div class="col-md-6">
							<span class="custom-checkbox">
								<input name="newsletter" type="checkbox" value="1"
									{{$user->newsletter == 1? 'checked' : ''}}>
								<span><i class="material-icons checkbox-checked"></i></span>
								<label>Sign up for our newsletter<br><em>You may unsubscribe at any moment.</em></label>
							</span>
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>
				</section>

				<footer class="form-footer clearfix">
					<button class="btn btn-primary form-control-submit float-xs-right" data-link-action="save-customer"
						type="submit">Save</button>
				</footer>
			</form>
		</section>
	</section>
</div>

@endsection