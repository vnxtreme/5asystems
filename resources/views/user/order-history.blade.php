@extends('_layout')
@section('content')
<script>
	document.body.classList = 'page-customer-account'
</script>

@include('user.left-menu')

<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Order History</h1>
		</header>

		<section id="content" class="page-content">
			@if($orderInformations->isNotEmpty())
				<table class="table table-striped table-bordered table-labeled hidden-sm-down">
					<thead class="thead-default">
						<tr>
							<th>Order reference</th>
							<th>Date</th>
							<th>Total price</th>
							<th class="hidden-md-down">Payment</th>
							<th class="hidden-md-down">Status</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($orderInformations as $orderInfo)
						<tr>
							<th scope="row">{{$orderInfo->id}}</th>
							<td>{{$orderInfo->created_at}}</td>
							<td >USD {{$orderInfo->total}}</td>
							<td class="hidden-md-down">Payments by bank</td>
							<td>
								<span class="label label-pill bright" style="background-color:#4169E1">
									Awaiting check payment
								</span>
							</td>
							
							<td class="text-sm-center order-actions">
								<a href="javascript:;" data-link-action="view-order-details">
									Details
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				<div class="orders hidden-md-up">
					<div class="order">
						<div class="row">
							<div class="col-xs-10">
								<a
									href="https://webbay.pl/szablony/60_digital_electronics_store/en/index.php?controller=order-detail&amp;id_order=13">
									<h3>DDVCEDGWN</h3>
								</a>
								<div class="date">05/19/2019</div>
								<div class="total">PLN3,351.00</div>
								<div class="status">
									<span class="label label-pill bright" style="background-color:#4169E1">
										Awaiting check payment
									</span>
								</div>
							</div>
							<div class="col-xs-2 text-xs-right">
								<div>
									<a href="https://webbay.pl/szablony/60_digital_electronics_store/en/index.php?controller=order-detail&amp;id_order=13"
										data-link-action="view-order-details" title="Details">
										<i class="material-icons"></i>
									</a>
								</div>
								<div>
									<a href="https://webbay.pl/szablony/60_digital_electronics_store/en/order?submitReorder=&amp;id_order=13"
										title="Reorder">
										<i class="material-icons"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			@else
				<aside id="notifications">
					<div class="container">
						<article class="alert alert-warning" role="alert" data-alert="warning">
							<ul>
								<li>You have not placed any orders.</li>
							</ul>
						</article>
					</div>
				</aside>
			@endif

		</section>
	</section>
</div>

@endsection