@extends('_layout')
@section('content')
<script>
	document.body.id = 'index';
</script>

<div id="content-wrapper">
	<section id="main">
		<section id="content" class="page-home">
			<div class="dd-loading-background" style="display: none;"></div>

			@include('components.carousel')

			<div class="row">
				@if(isset($featureProductsChunk[0]) and $featureProductsChunk[0]->isNotEmpty())
					@php
						$featureProductsFirst = $featureProductsChunk[0];
					@endphp

					@foreach ($featureProductsFirst as $featured)
						<div class="banner1 banner" style="padding: 0 5px;">
							<div class="ikona_1"></div>
							<div class="ikona_2"></div>
							<a class="banner1_link" href="{{route('product.detail', ['slug'=>$featured->slug])}}">
								<div class="kreski">
									<div class="content-banery">
										<div class="opis"> {{$featured->name}}
											<div class="buttons-banners">See more</div>
										</div>
									</div>
									<div class="oslona">
										<img src="{{asset("storage/{$featured->image_fb}")}}" alt="{{$featured->name}}"
											title="{{$featured->name}}" width="358" height="238">
									</div>
								</div>
							</a>
						</div>
						{{-- <a class="banner1_link" href="{{route('product.detail', ['slug'=>$featured->slug])}}">
						</a> --}}
					@endforeach
				@endif

			</div>
			<section class="featured-products clearfix">
				<div class="linia_tytul">
					<div class="tytuly">
						New products
					</div>
					<div class="SliderNavigation">
						<a class="btn prev slider_new_prev">&nbsp;</a>
						<a class="btn next slider_new_next">&nbsp;</a>
					</div>
				</div>
				<div class="products">
					<!-- Define Number of product for SLIDER -->
					<ul id="new-carousel"
						class="bestseller_grid product_list grid row gridcount owl-carousel owl-theme">
						@if($newProducts->isNotEmpty())
						@foreach ($newProducts as $new)
						<li class="item">
							@include('components.product-square', ['product'=> $new])
						</li>
						@endforeach
						@endif
					</ul>
				</div>
			</section>

			<section class="featured-products clearfix">
				<div class="linia_tytul">
					<div class="tytuly">
						Popular Products
					</div>
					<div class="SliderNavigation">
						<a class="btn prev sliderfeature_prev">&nbsp;</a>
						<a class="btn next sliderfeature_next">&nbsp;</a>
					</div>
				</div>
				<div class="products">
					<ul id="feature-carousel" class="product_list owl-carousel owl-theme">
						@if($popularProducts->isNotEmpty())
						@foreach ($popularProducts as $popular)
						<li class="item">
							@include('components.product-square', ['product'=> $popular])
						</li>
						@endforeach
						@endif
					</ul>
				</div>
			</section>

			@if(isset($featureProductsChunk[1]) and $featureProductsChunk[1]->isNotEmpty())
				@php
					$featureProductsSecond = $featureProductsChunk[1];
				@endphp
				<div class="row">
					@foreach ($featureProductsSecond as $featureProduct2)
						<div class="banner4 banner">
							<div class="ikona_1"></div>
							<div class="ikona_2"></div>
							<a class="banner4_link"
								href="{{route('product.detail', ['slug' => $featureProduct2->slug])}}">
								<div class="kreski">
									<div class="content-banery">
										<div class="opis">{{$featureProduct2->name}}
											<div class="buttons-banners">see more</div>
										</div>
									</div>
									<div class="oslona">
										<img src="{{asset("storage/{$featureProduct2->image_fb}")}}" alt="speakers" title="speakers" width="358" height="238" />
									</div>
								</div>
							</a>
						</div>
						<a class="banner4_link"
							href="{{route('product.detail', ['slug' => $featureProduct2->slug])}}">
						</a>
					@endforeach
				</div>
			@endif

			<section class="featured-products clearfix">
				<div class="linia_tytul">
					<div class="tytuly">
						Best Sellers
					</div>
					<div class="SliderNavigation">
						<a class="btn prev slider_bestseller_prev">&nbsp;</a>
						<a class="btn next slider_bestseller_next">&nbsp;</a>
					</div>
				</div>
				<div class="products">
					<!-- Define Number of product for SLIDER -->
					<ul id="bestseller-carousel" class="product_list owl-carousel owl-theme">

						@if($bestsellerProducts->isNotEmpty())
						@foreach ($bestsellerProducts as $bestseller)
						<li class="item">
							@include('components.product-square', ['product'=> $bestseller])
						</li>
						@endforeach
						@endif
					</ul>
				</div>
			</section>

			@if($posts->isNotEmpty())
			<section class="featured-products clearfix">
				<div id="smartblog_block" class="block products_block  clearfix">
					<div class="linia_tytul">
						<div class="tytuly">
							<a href="{{route('news')}}">Latest News</a>
						</div>
						<div class="SliderNavigation">
							<a class="btn prev smartblog_prev">&nbsp;</a>
							<a class="btn next smartblog_next">&nbsp;</a>
						</div>
					</div>
					<div class="sdsblog-box-content block_content row">
						<div id="smartblog-carousel" class="owl-carousel product_list owl-theme">
							
							@foreach ($posts as $post)
							<div class="item sds_blog_post">
								<div class="blog_post">
									<span class="news_module_image_holder">
										<a href="{{route('news.detail',['slug' => $post->slug])}}" target="_blank">
											<img alt="Viderer voluptatum te eum" class="feat_img_small"
												src="{{asset("storage/{$post->image}")}}">
											<span class="blog-hover"></span>
										</a>
										<span class="blog_date">
											<span class="day_date"> 3</span>
											<span class="date_inner">
												<span class="day_month">Aug</span>
											</span>
										</span>
										<span class="blogicons">
											<a title="Click to view Full Image"
												href="{{asset("storage/{$post->image}")}}"
												data-lightbox="example-set" class="icon zoom">&nbsp;</a>
										</span>
									</span>
									<div class="blog_content">
										<div class="blog_inner">
											<h4 class="sds_post_title">
												<a href="{{route('news.detail',['slug' => $post->slug])}}" target="_blank">{{$post->title}}</a>
											</h4>
											<p class="desc">
												{{$post->excerpt}}
											</p>
											<div class="read_more">
												<a title="Click to view Read More" href="{{route('news.detail',['slug' => $post->slug])}}" class="icon readmore" target="_blank">Read more</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							
						</div>
					</div>
				</div>
			</section>
			@endif

		</section>
	</section>
</div>

{{-- <div id="featureProduct" class="page-home"></div>
<div id="bestseller" class="page-home"></div> --}}
@endsection