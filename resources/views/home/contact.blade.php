@extends('_layout')
@section('content')
<div id="left-column" class="col-xs-12 col-sm-3">
	<div class="contact-rich">
		<h4>Store information</h4>
		@forelse ($contacts as $contact)
			<div class="block">
				<div class="icon">
					<i class="material-icons"></i> 
				</div>
				
				<div class="data">
					{{$contact->name}}
					<br><br>
					{{$contact->address}}
					<br><br>
					<a href="tel:{{$contact->phone}}">{{$contact->phone}}</a>
					<br><br>
					<a href="mailto:{{$contact->email}}">{{$contact->email}}</a>
				</div>
			</div>
		@empty
			-
		@endforelse
	</div>
</div>
<div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
	<section id="main">
		<section id="content" class="page-content card card-block">
			<section class="contact-form">
				<form action="{{route('contact.post')}}" method="post" >
					@csrf
					<section class="form-fields">
						@if (session('success'))
						<div class="alert alert-success">
							{{ session('success') }}
						</div>
						@endif
						<div class="form-group row">
							<div class="col-md-9 col-md-offset-3">
								<h3>Contact us</h3>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label">Subject</label>
							<div class="col-md-6">
								<select name="id_contact" class="form-control form-control-select">
									<option value="1">Customer service</option>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label">Email address</label>
							<div class="col-md-6">
								<input class="form-control" name="email" type="email" placeholder="your@email.com" required>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label">Message</label>
							<div class="col-md-9">
								<textarea class="form-control" name="message" placeholder="How can we help?"
									rows="3" required></textarea>
							</div>
						</div>

						<div class="form-group row">
							<div class="offset-md-3">

							</div>
						</div>

					</section>

					<footer class="form-footer text-sm-right">
						<style>
							input[name=url] {
								display: none !important;
							}
						</style>
						<input class="btn btn-primary" type="submit" name="submitMessage" value="Send">
					</footer>

				</form>
			</section>
		</section>
	</section>
</div>
@endsection