@extends('_layout')
@section('content')
<nav data-depth="2" class="breadcrumb hidden-sm-down">
	<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route('home')}}">
				<span itemprop="name">Home</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="javascript:;">
				<span itemprop="name">{{$content->title}}</span>
			</a>
			<meta itemprop="position" content="2">
		</li>
	</ol>
</nav>
<div id="content-wrapper">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">
				{{$content->title}}
			</h1>
		</header>

		<section id="content" class="page-content page-cms page-cms-1">
			{!! $content->content !!}
		</section>
	</section>

</div>
@endsection