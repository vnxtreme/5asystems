<div class="highlighted-informations">
	<div class="buttons-actions_align">
		<form class="quick-add" onsubmit="return onSubmit(this);">
			@csrf
			<input type="hidden" name="product_id" value="{{$product->id}}">
			<input type="hidden" name="options[color]" value="{{$product->firstProductDetail->color_id}}">
			<input type="hidden" name="options[size]" value="{{$product->firstProductDetail->size_id}}">
			{{-- <input type="hidden" name="id_customization" value="0" id="product_customization_id_25"> --}}
			<div class="product-add-to-cart-buttons">
				<div class="product-quantity">
					<div class="qty hidden-xl-down">
						<input type="text" name="qty" id="quantity_wanted" value="1" class="input-group" min="1">
					</div>
					<div class="add">
						<button class="add-to-cart-buttons" style="outline: none; text-decoration: none;"
							title="Add to cart" type="submit" ></button>
					</div>
				</div>
			</div>
		</form>

		<input class="product-refresh" data-url-update="false" name="refresh" type="submit" value="Refresh" hidden="">
		<a href="{{route('product.detail', ['slug' => $product->slug])}}" title="Show" class="view"></a>
		{{-- <a href="{{route('product.detail', ['slug' => $item->slug])}}" class="quick-view" title="Quick view" >
		<div class="quick-view-buttons">
			<i class="material-icons quick"></i>
		</div>
		</a> --}}
	</div>
</div>
<script>
	var onSubmit = function (form){
		data = $(form).serialize();
		
		$.post('/cart/add-cart',data, function(resp){
			const totalItem = Object.values(resp.cart).reduce(function(accumulator, item){
				return accumulator + parseInt(item.qty);
			}, 0)
				
			$('.cart-products-count').html(`(${totalItem})`);
			$.notify("Item added", resp.status);
		}).fail(function(error){
			console.error( error)
		})
		return false;
	}
</script>