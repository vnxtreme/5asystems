<footer class="page-footer">
	<!-- Footer content -->
</footer>
<footer id="footer">

	<!-- Dodanie strzałki powrótnej do góry strony -->
	<a href="#" title="Do góry!" id="do_gory" style="display: none;"></a>
	<!-- End Dodanie strzałki powrótnej do góry strony -->
	<div class="row">
		{{-- <div class="news newstlo">
			<div class="block_newsletter col-lg-12 col-md-12 col-sm-12">
				<h2 class="newsletter-tekst">Newsletter</h2>

				<p class="tekstn col-md-12 col-xs-12">Get our latest news and special sales</p>
				<form action="https://webbay.pl/szablony/60_digital_electronics_store/en/#footer" method="post">

					<div class="col-xs-12 news-center">
						<input class="btn btn-primary pull-xs-right hidden-xs-down" name="submitNewsletter"
							type="submit" value="Subscribe">
						<input class="btn btn-primary pull-xs-right hidden-sm-up" name="submitNewsletter" type="submit"
							value="OK">
						<div class="input-wrapper">
							<input name="email" type="text" value="" placeholder="Your email address">
						</div>
						<input type="hidden" name="action" value="0">
						<div class="clearfix"></div>
					</div>

				</form>
			</div>
		</div> --}}

		
		<div class="block-social">
			<ul>
				@if(setting('social.fb'))
					<li class="facebook"><a href="{{setting('social.fb')}}" target="_blank">Facebook</a></li>
				@endif
				@if(setting('social.youtube'))
					<li class="youtube"><a href="{{setting('social.youtube')}}" target="_blank">Youtube</a></li>
				@endif
				{{-- <li class="twitter"><a href="#" target="_blank">Twitter</a></li>
				<li class="rss"><a href="#" target="_blank">Rss</a></li> --}}
				{{-- <li class="instagram"><a href="#" target="_blank">Instagram</a></li> --}}
			</ul>
		</div>


	</div>
	<div class="container">
		<div class="footer-container">
			<div class="row">
				<div class="col-md-6 links" style="padding: 0px;">
					<div class="col-md-6 wrapper">
						<i class="material-icons">reorder</i>
						<h3 class="h3 hidden-sm-down">Products</h3>
						<div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_97878"
							data-toggle="collapse">
							<span class="h3">Products</span>
							<span class="pull-xs-right">
								<span class="navbar-toggler collapse-icons">
									<i class="material-icons add"></i>
									<i class="material-icons remove"></i>
								</span>
							</span>
						</div>
						<ul id="footer_sub_menu_97878" class="collapse">
							@forelse ($categories as $cat)
								<li>
									<a class="cms-page-link" href="{{route('product.list', ['slug' => $cat->slug])}}" title="{{$cat->name}}">
										{{$cat->name}}
									</a>
								</li>
							@empty
								
							@endforelse
						</ul>
					</div>

					
					<div class="col-md-6 wrapper">
						<i class="material-icons">reorder</i>
						<h3 class="h3 hidden-sm-down">Our company</h3>
						<div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_96564"
							data-toggle="collapse">
							<span class="h3">Our company</span>
							<span class="pull-xs-right">
								<span class="navbar-toggler collapse-icons">
									<i class="material-icons add"></i>
									<i class="material-icons remove"></i>
								</span>
							</span>
						</div>
						<ul id="footer_sub_menu_96564" class="collapse">
							@if($myCompanies->isNotEmpty())
							@foreach ($myCompanies as $company)
								<li>
									<a class="cms-page-link" href="{{route('my-company', ['slug' => $company->slug])}}"
										title="{{$company->title}}">
										{{$company->title}}
									</a>
								</li>
							@endforeach
							@endif
							<li>
								<a class="cms-page-link" href="{{route('contact')}}" title="Contact us">
									Contact us
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				<div id="block_myaccount_infos" class="col-md-3 links wrapper">
					<i class="material-icons">account_box</i>
					<h3 class="myaccount-title hidden-sm-down">
						<a class="text-uppercase"
							href="{{route('user.account')}}" rel="nofollow">
							Your account
						</a>
					</h3>
					<div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
						<span class="h3">Your account</span>
						<span class="pull-xs-right">
							<span class="navbar-toggler collapse-icons">
								<i class="material-icons add"></i>
								<i class="material-icons remove"></i>
							</span>
						</span>
					</div>
					<ul class="account-list collapse" id="footer_account_list">
						<li>
							<a href="{{route('user.profile')}}" title="Personal info" rel="nofollow">
								Personal info
							</a>
						</li>
						<li>
							<a href="{{route('user.order-history')}}" title="Orders" rel="nofollow">
								Orders
							</a>
						</li>
						<li>
							<a href="{{route('user.addresses')}}" title="Addresses" rel="nofollow">
								Addresses
							</a>
						</li>

					</ul>
				</div>
				<div class="block-contact col-md-4 links wrapper">
					<div class="hidden-sm-down"><i class="fa fa-map-marker" aria-hidden="true"></i>
						<h4 class="text-uppercase block-contact-title">Store information</h4>
						<ul id="block-contact_list" class="collapse">

							<li>
								<div class="data">
									{{$contacts[0]->name}}<br>{{$contacts[0]->address}}</div>
							</li>

							<li>
								<i class="fa fa-phone" aria-hidden="true"></i>
								<br>
								<div class="data">
									Call us: <a href="tel:{{$contacts[0]->phone}}">{{$contacts[0]->phone}}</a>
								</div>
							</li>

							<li>
								<i class="fa fa-envelope" aria-hidden="true"></i>
								<div class="data">
									<br>
									Email us: <span><a href="mailto:{{$contacts[0]->email}}">{{$contacts[0]->email}}</a></span>
								</div>
							</li>
						</ul>
					</div>
					<div class="title clearfix hidden-md-up">
						<i class="fa fa-map-marker" aria-hidden="true"></i><a class="h3"
							href="https://webbay.pl/szablony/60_digital_electronics_store/en/stores">Store
							information</a>
					</div>
				</div>
			</div>
			<div class="row">

			</div>
			<div class="row">
				<div class="col-md-12">
					<p>
					</p>
					<div class="linkstopka">
						<a class="_blank" href="{{route('home')}}" target="_blank">
							© 2019 - All rights reserved
						</a>
					</div>
					<p></p>
				</div>
			</div>
		</div>
	</div>
</footer>