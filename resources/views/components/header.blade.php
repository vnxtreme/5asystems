<header id="header">
@php
	// dd(Auth::user());
@endphp
	<div class="header-top">
		<div class="hidden-sm-down" id="_desktop_logo">
			<a href="{{route('home')}}/">
				<img class="logo img-responsive" src="{{asset("storage/".setting('site.logo') )}}"
					alt="Digital Electronics">
			</a>
		</div>
		<div class="ikony-belka">
			<div class="tlo_menu"></div>
			<div class="tlo_belka"></div>
			<div class="belka fixed_menuani">
				<div class="lewabelka col-xs-12" style="padding: 0px;">

					<div class="menu_and_logo">

						<div class="row">

							@if($categories->isNotEmpty())
							<div class="menu col-lg-12 js-top-menu position-static hidden-md-down"
								id="_desktop_top_menu">

								<ul class="top-menu" id="top-menu" data-depth="0">
									@foreach ($categories as $category)
									<li class="category">
										<a class="dropdown-item"
											href="{{route('product.list', ['slug' => $category->slug])}}"
											data-depth="0">
											<span class="pull-xs-right hidden-md-up">
												<span data-target="#top_sub_menu_{{$category->id}}" data-toggle="collapse"
													class="navbar-toggler collapse-icons">
													<i class="material-icons add"></i>
													<i class="material-icons remove"></i>
												</span>
											</span>
											{{$category->name}}
										</a>

										@if(isset($category->subCategory) and $category->subCategory->isNotEmpty())
										<div class="popover sub-menu js-sub-menu collapse"
											id="top_sub_menu_{{$category->id}}">

											<ul class="top-menu" data-depth="1">
												@foreach ($category->subCategory as $subCategory)
												<li class="category">
													<a class="dropdown-item dropdown-submenu"
														href="{{route('product.list', ['slug' => $subCategory->slug])}}"
														data-depth="1"
													>
														@if(isset($subCategory->subCategory) and $subCategory->subCategory->isNotEmpty())
															<span class="pull-xs-right hidden-md-up">
																<span data-target="#top_sub_menu_{{$subCategory->id}}"
																	data-toggle="collapse"
																	class="navbar-toggler collapse-icons">
																	<i class="material-icons add"></i>
																	<i class="material-icons remove"></i>
																</span>
															</span>
														@endif
														{{$subCategory->name}}
													</a>

													@if(isset($subCategory->subCategory) and
													$subCategory->subCategory->isNotEmpty())
													<div class="collapse" id="top_sub_menu_{{$subCategory->id}}">

														<ul class="top-menu" data-depth="2">
															@foreach($subCategory->subCategory as $grantSubCategory)
															<li class="category" {{-- id="category-14" --}}>
																<a class="dropdown-item"
																	href="{{route('product.list', ['slug' => $grantSubCategory->slug])}}"
																	data-depth="2">
																	{{$grantSubCategory->name}}
																</a>
															</li>
															@endforeach
														</ul>

													</div>
													@endif
												</li>
												@endforeach

											</ul>

											@if($category->image)
											<div class="banerek_menus">
												<div class="banerek_menu">
													<img src="{{asset("storage/{$category->image}")}}" alt="">
												</div>
											</div>
											@endif
										</div>
										@endif
									</li>
									@endforeach
								</ul>

							</div>
							@endif
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				@include('components.navbar')
			</div>

		</div>

		<div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
			<div class="js-top-menu mobile" id="_mobile_top_menu"></div>
			<div class="js-top-menu-bottom">
				<div id="_mobile_currency_selector"></div>
				<div id="_mobile_language_selector"></div>
				<div id="_mobile_contact_link"></div>
			</div>
		</div>

	</div>

</header>