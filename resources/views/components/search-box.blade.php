<div id="search_widget" class="col-lg-4 col-md-5 col-sm-12 search-widget">
	<span class="search_button"></span>
	<div class="search_toggle">
		<form method="get" action="{{route('search')}}">
			{{-- <input type="hidden" name="controller" value="search"> --}}
			<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
			<input type="text" name="key" placeholder="Search product..." class="ui-autocomplete-input"
				autocomplete="off">
			<button type="submit"></button>
		</form>
	</div>
</div>