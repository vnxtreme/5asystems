<nav class="header-nav">
	<div class="right-nav">
		<!-- Block search module TOP -->
		@include('components.search-box')
		<!-- /Block search module TOP -->

		<div class="user-info dropdown js-dropdown">
			<span class="user-info-title expand-more _gray-darker" data-toggle="dropdown">
				<div class="login-icons"></div>
			</span>
			<ul class="dropdown-menu">
				@auth
					<li>
						<a class="account dropdown-item" href="{{route('user.account')}}" title="View my account" rel="nofollow">
							<span class="">{{Auth::user()->name}}</span>
						</a>
					</li>
					<li>
						<a class="account dropdown-item" href="{{route('user.profile')}}" title="View profile" rel="nofollow">
							<span class="">Profile</span>
						</a>
					</li>
					<li>
						<a class="account dropdown-item" href="{{route('user.addresses')}}" title="View addresses" rel="nofollow">
							<span class="">Addresses</span>
						</a>
					</li>
					<li>
						<a class="account dropdown-item" href="{{route('user.order-history')}}" title="View addresses" rel="nofollow">
							<span class="">Order History</span>
						</a>
					</li>
					<li>
						<a class="account dropdown-item" href="{{ route('logout') }}"
							onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
							title="View my customer account" rel="nofollow"
						>
							<span class="">{{ __('Logout') }}</span>
						</a>
					</li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						@csrf
					</form>
				@else
				<li>
					<a class="dropdown-item" href="{{route('register')}}" title="Register account"
						rel="nofollow">
						<span>Sign up</span></a>
				</li>
				<li>
					<a class="dropdown-item" href="{{route('login')}}" title="Log in to your customer account"
						rel="nofollow">
						<span>Sign in</span></a>
				</li>
				@endauth
			</ul>
		</div>
		<div id="_desktop_cart">
			<div class="blockcart cart-preview active">
				<div class="header">
					<a rel="nofollow" href="{{route('cart')}}">
						<div class="cart-icons"></div>
						<span class="cart-products-count">({{Cart::count()}})</span>
					</a>
				</div>
			</div>
		</div>


		<div class="hidden-md-up text-xs-center mobile">
			<div class="pull-xs-left" id="menu-icon">
				<i class="material-icons"></i>
			</div>
			<div class="pull-xs-right" id="_mobile_cart"></div>
			<div class="pull-xs-right" id="_mobile_user_info"></div>
			<div class="top-logo" id="_mobile_logo"></div>
			<div class="clearfix"></div>
		</div>
	</div>
</nav>