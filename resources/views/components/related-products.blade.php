@if($relatedProducts->isNotEmpty())
<section class="featured-products clearfix">
	<div class="linia_tytul">
		<div class="tytuly">
			{{$relatedProducts->count()}} other products in the same category:
		</div>
		<div class="SliderNavigation">
			<a class="btn prev slider_productscategory_prev">&nbsp;</a>
			<a class="btn next slider_productscategory_next">&nbsp;</a>
		</div>
	</div>

	<div class="products">

		<ul id="productscategory-carousel" class="product_list owl-carousel owl-theme">
			@foreach ($relatedProducts as $product)
				<li class="item">
					@include('components.product-square')
				</li>
			@endforeach

			<div class="owl-controls clickable">
				<div class="owl-pagination">
					<div class="owl-page"><span class=""></span></div>
					<div class="owl-page active"><span class=""></span></div>
				</div>
			</div>
		</ul>


	</div>
</section>
@endif