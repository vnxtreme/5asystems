@php
	$key = request()->key;
	$filters = [
		'Relevance' => $key? url()->current()."?key={$key}" : url()->current(),
		'Name, A to Z' => $key ? url()->current()."?key={$key}&col=name&order=asc" : url()->current()."?col=name&order=asc",
		'Name, Z to A' => $key ? url()->current()."?key={$key}&col=name&order=desc" : url()->current()."?col=name&order=desc",
		'Price, low to high' => $key ? url()->current()."?key={$key}&col=price&order=asc" : url()->current()."?col=price&order=asc",
		'Price, high to low' => $key ? url()->current()."?key={$key}&col=price&order=desc" : url()->current()."?col=price&order=desc",
	];
	// dd($searchArr);
@endphp
<div class="row">
	<span class="col-sm-3 col-md-3 hidden-sm-down sort-by">Sort by:</span>
	<div class="col-sm-9 col-xs-8 col-md-9 products-sort-order dropdown">
		<button class="btn-unstyle select-title" rel="nofollow" data-toggle="dropdown" aria-haspopup="true"
			aria-expanded="false">
			@php
				$currentFilter = array_filter($filters, function($filter){
					return $filter === request()->fullUrl();
				});
			@endphp
			{{ key($currentFilter) ?? 'Relevance' }} <i class="material-icons float-xs-right"></i>
		</button>
		<div class="dropdown-menu">
			@foreach ($filters as $name => $filter)
				<a rel="nofollow" href="{{$filter}}" class="select-list js-search-link">
					{{$name}}
				</a>
			@endforeach
		</div>
	</div>

	<div class="col-sm-3 col-xs-4 hidden-lg-up filter-button">
		<button id="search_filter_toggler" class="btn btn-secondary">
			Filter
		</button>
	</div>
</div>
<script>
	$('.select-list').click(function(e){
		window.location = e.target.getAttribute('href')
	})
</script>