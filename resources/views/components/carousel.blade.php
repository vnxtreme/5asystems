<div id="carousel" data-ride="carousel" class="carousel slide" data-interval="5000" data-pause="">
	<ul class="carousel-inner" role="listbox">
		@foreach ($carousels as $index => $carousel)
		<li class="carousel-item {{$index == 0 ? 'active' : ''}}">
			<figure>
				<a href="{{$carousel->link}}" target="_blank">
					<img src="{{asset("storage/{$carousel->image}")}}" alt="">
				</a>
				<figcaption class="caption"><a href="{{$carousel->link}}" target="_blank">
						<h2 class="display-1 text-uppercase"></h2>
					</a>
					<div class="caption-description"><a href="{{$carousel->link}}" target="_blank">
							<h2 class="napisy-1">{{$carousel->name}}</h2>
							<div class="napisy-2">
								<p>{{$carousel->description}}</p>
							</div>
						</a>
						<p><a href="{{$carousel->link}}" target="_blank" class="shop_now"> Shop now</a></p>
					</div>
				</figcaption>

			</figure>
		</li>
		@endforeach
	</ul>
	<div class="direction">
		<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
			<span class="icon-prev hidden-xs" aria-hidden="true">
				<i class="material-icons"></i>
			</span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
			<span class="icon-next" aria-hidden="true">
				<i class="material-icons"></i>
			</span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>