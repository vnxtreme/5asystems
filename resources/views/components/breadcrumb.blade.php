<nav data-depth="3" class="breadcrumb hidden-sm-down">
	<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="https://webbay.pl/szablony/60_digital_electronics_store/en/">
				<span itemprop="name">Home</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="https://webbay.pl/szablony/60_digital_electronics_store/en/10-television">
				<span itemprop="name">Television</span>
			</a>
			<meta itemprop="position" content="2">
		</li>
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="https://webbay.pl/szablony/60_digital_electronics_store/en/11-plasma">
				<span itemprop="name">Plasma</span>
			</a>
			<meta itemprop="position" content="3">
		</li>
	</ol>
</nav>