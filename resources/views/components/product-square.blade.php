<article class="product-miniature js-product-miniature" itemscope="" itemtype="http://schema.org/Product">
	<div class="thumbnail-container">
		<div class="dd-product-image">
			<a href="{{route('product.detail', ['slug' => $product->slug])}}" class="thumbnail product-thumbnail">
				<img class="ddproduct-img1"
					src="{{Voyager::image($product->thumbnail('cropped')) }}"
					alt="{{$product->name}}"
				/>
			</a>
		</div>

		<div class="product-desc">
			<div class="product-description">
				<h1 class="h3 product-title" itemprop="name">
					<a href="{{route('product.detail', ['slug' => $product->slug])}}">{{$product->name}}</a>
				</h1>
				<div class="product-price-and-shipping">
					<span class="sr-only">Price</span>
					<span itemprop="price" class="price">USD {{$product->firstProductDetail->price}}</span>
				</div>
			</div>
			@include('components.highlighted-information')
		</div>
	</div>
</article>