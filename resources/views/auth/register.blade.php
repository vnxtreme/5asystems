@extends('_layout')

@section('content')
<script>
	document.body.id = 'authentication';
    document.body.classList = 'page-customer-account page-authentication'

</script>
<section id="main">
	<header class="page-header">
		<h1 class="heading-cms">
			Create an account
		</h1>
	</header>
	<section id="content" class="page-content card card-block">
		<section class="register-form">
			@if(session()->has('message'))
				<p style="color:green; text-align:center;">{{session()->get('message')}}</p>
			@endif
			<p>Already have an account? <a href="{{route('login')}}">Login instead!</a></p>
			<form action="{{route('register')}}" id="customer-form" class="js-customer-form" method="post">
				@csrf
				<section>
					<div class="form-group row ">
						<label class="col-md-3 form-control-label">Social title</label>
						<div class="col-md-6 form-control-valign">
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="1">
									<span></span>
								</span>
								Mr.
							</label>
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="2">
									<span></span>
								</span>
								Mrs.
							</label>
						</div>

						<div class="col-md-3 form-control-comment">
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">
							First name
						</label>
						<div class="col-md-6">
							<input class="form-control" name="first_name" type="text" value="{{old('first_name') ?? ''}}" required="">
							@if ($errors->has('first_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('first_name') }}</strong>
							</span>
							@endif
						</div>

						<div class="col-md-3 form-control-comment">
						</div>
					</div>
					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Last name</label>
						<div class="col-md-6">
							<input class="form-control" name="last_name" type="text" value="{{old('last_name') ?? ''}}" required="">
							@if ($errors->has('last_name'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('last_name') }}</strong>
							</span>
							@endif
						</div>

						<div class="col-md-3 form-control-comment">
						</div>
					</div>
					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Email</label>
						<div class="col-md-6">
							<input class="form-control" name="email" type="email" value="{{old('email') ?? ''}}" required="">
							@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Password</label>
						<div class="col-md-6">
							<div class="input-group js-parent-focus">
								<input class="form-control js-child-focus js-visible-password" name="password"
									type="password" value="" pattern=".{5,}" required="">
								<span class="input-group-btn">
									<button class="btn" type="button" data-action="show-password" data-text-show="Show"
										data-text-hide="Hide">
										Show
									</button>
								</span>
							</div>
							@if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>

						<div class="col-md-3 form-control-comment"></div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label"></label>
						<div class="col-md-6">
							<span class="custom-checkbox">
								<input name="newsletter" type="checkbox" value="1" checked>
								<span><i class="material-icons checkbox-checked"></i></span>
								<label>Receive offers from our partners</label>
							</span>
						</div>

						<div class="col-md-3 form-control-comment">
						</div>
					</div>
				</section>

				<footer class="form-footer clearfix">
					<input type="hidden" name="submitCreate" value="1">

					<button class="btn btn-primary form-control-submit float-xs-right" type="submit">
						Save
					</button>
				</footer>
			</form>
		</section>
	</section>

	<footer class="page-footer">
		<!-- Footer content -->
	</footer>
</section>
@endsection