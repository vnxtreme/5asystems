@extends('_layout')

@section('content')
<script>
    document.body.id = 'authentication';
	document.body.classList = 'page-customer-account page-authentication'
</script>
<div id="content-wrapper">

    <section id="main">
        <header class="page-header">
            <h1 class="heading-cms">{{ __('Verify Your Email Address') }}</h1>
        </header>
        <section id="content" class="page-content card card-block">
            <div class="card-body">
                @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('A fresh verification link has been sent to your email address.') }}
                </div>
                @endif

                {{ __('Before proceeding, please check your email for a verification link.') }}
                {{ __('If you did not receive the email') }}, <u><a
                        href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a></u>.
            </div>
        </section>
    </section>

</div>
@endsection