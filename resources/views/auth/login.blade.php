@extends('_layout')

@section('content')
<script>
	document.body.id = 'authentication';
	document.body.classList = 'page-customer-account page-authentication'
</script>

<div id="content-wrapper">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Log in to your account</h1>
		</header>
		<section id="content" class="page-content card card-block">
			<section class="login-form">
				@if(session()->has('message'))
					<p style="color:green; text-align:center;">{{session()->get('message')}}</p>
				@endif
				<form id="login-form" action="{{route('login')}}" method="post">
					@csrf
					<section>
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								Email
							</label>
							<div class="col-md-6">
								<input class="form-control" name="email" type="email" value="" required="">
								@if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
							</div>

							<div class="col-md-3 form-control-comment">
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								Password
							</label>
							<div class="col-md-6">
								<div class="input-group js-parent-focus">
									<input class="form-control js-child-focus js-visible-password" name="password"
										type="password" value="" pattern=".{5,}" required="">
									<span class="input-group-btn">
										<button class="btn" type="button" data-action="show-password"
											data-text-show="Show" data-text-hide="Hide">
											Show
										</button>
									</span>
								</div>
								
								@if ($errors->has('password'))
		                <span class="invalid-feedback" role="alert">
		                    <strong>{{ $errors->first('password') }}</strong>
		                </span>
		            @endif

							</div>

							<div class="col-md-3 form-control-comment">
							</div>
						</div>

						<div class="forgot-password">
							<a href="{{route('password.request')}}" rel="nofollow">Forgot your password?</a>
						</div>
					</section>

					<footer class="form-footer text-sm-center clearfix">
						<input type="hidden" name="submitLogin" value="1">

						<button class="btn btn-primary" data-link-action="sign-in" type="submit">
							Sign in
						</button>
					</footer>
				</form>
			</section>
			<hr>

			<div class="no-account">
				<a href="{{route('register')}}">No account? Create one here</a>
			</div>
		</section>

		<footer class="page-footer">
			<!-- Footer content -->
		</footer>
	</section>
</div>
@endsection