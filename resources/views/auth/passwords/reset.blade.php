@extends('_layout')

@section('content')
<script>
	document.body.id = 'authentication';
	document.body.classList = 'page-customer-account page-authentication'
</script>

<div id="content-wrapper">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">{{ __('Reset Password') }}</h1>
		</header>
		<section id="content" class="page-content card card-block">
			<section class="login-form">
				<form action="{{ route('password.update') }}" method="post">
					@csrf
					<input type="hidden" name="token" value="{{ $token }}">
					<section>
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Email</label>
							<div class="col-md-6">
								<input class="form-control" name="email" type="email" value="" required="">
								@if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">Password</label>
							<div class="col-md-6">
								<div class="input-group js-parent-focus">
									<input class="form-control js-child-focus js-visible-password" name="password"
										type="password" value="" pattern=".{5,}" required="">
									<span class="input-group-btn">
										<button class="btn" type="button" data-action="show-password"
											data-text-show="Show" data-text-hide="Hide">
											Show
										</button>
									</span>
								</div>
								
								@if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								{{ __('Confirm Password') }}
							</label>
							<div class="col-md-6">
								<div class="input-group js-parent-focus">
									<input class="form-control js-child-focus js-visible-password" name="password_confirmation"
										type="password" value="" pattern=".{5,}" required="">
									<span class="input-group-btn">
										<button class="btn" type="button" data-action="show-password"
											data-text-show="Show" data-text-hide="Hide">
											Show
										</button>
									</span>
								</div>
								
								@if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif

							</div>
						</div>
					</section>

					<footer class="form-footer text-sm-center clearfix">
						<button class="btn btn-primary" data-link-action="sign-in" type="submit">
							{{ __('Reset Password') }}
						</button>
					</footer>
				</form>
			</section>
		</section>

		<footer class="page-footer">
			<!-- Footer content -->
		</footer>
	</section>
</div>
@endsection