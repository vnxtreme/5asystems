@extends('_layout')

@section('content')
<script>
	document.body.id = 'authentication';
	document.body.classList = 'page-customer-account page-authentication'
</script>

<div id="content-wrapper">
	<section id="main">
		<header class="page-header">
			<h1 class="heading-cms">Reset Password</h1>
		</header>
		<section id="content" class="page-content card card-block">
			<section class="login-form">
				<form action="{{route('password.email')}}" method="post">
					@csrf
					<section>
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								Email
							</label>
							<div class="col-md-6">
								<input class="form-control" name="email" type="email" value="{{old('email')}}" required="">
								@if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
							</div>

							<div class="col-md-3 form-control-comment">
							</div>
						</div>
					</section>

					<footer class="form-footer text-sm-center clearfix">
						<button class="btn btn-primary" data-link-action="sign-in" type="submit">
							{{ __('Send Password Reset Link') }}
						</button>
					</footer>
				</form>
			</section>
		</section>

		<footer class="page-footer">
			<!-- Footer content -->
		</footer>
	</section>
</div>
@endsection