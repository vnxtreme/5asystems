<section id="checkout-delivery-step" class="checkout-step -current -reachable js-current-step">
	<h1 class="step-title h3">
		<i class="material-icons done"></i>
		<span class="step-number">3</span>
		Shipping Method
	</h1>

	<div class="content">
		<div class="errors">

		</div>
		<div class="delivery-options-list">
			<div class="form-fields">
				<div class="delivery-options">
					<div class="row delivery-option">
						<div class="col-sm-1">
							<span class="custom-radio float-xs-left required">
								<input type="radio" name="delivery_method" id="delivery_fedex" value="1" required>
								<span></span>
							</span>
						</div>
						<label for="delivery_option_2" class="col-sm-11 delivery-option-2">
							<div class="row">
								<div class="col-sm-5 col-xs-12">
									<div class="row">
										<div class="col-xs-3">
											<img src="{{asset("img/shipping.jpg")}}" alt="My carrier">
										</div>
										<div class="col-xs-9">
											<span class="h6 carrier-name">FedEx Ground</span>
										</div>
									</div>
								</div>
								<div class="col-sm-4 col-xs-12">
									<span class="carrier-delay" style="display:none;">
										Processing data...
										<img src="{{asset('img/loader.gif')}}" alt="" width="80">
									</span>
								</div>
								<div class="col-sm-3 col-xs-12">
									USD <span class="carrier-price">{{number_format($deliveryFee, 2)}}</span>
								</div>
							</div>
						</label>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="order-options">
					<div id="delivery">
						<label for="delivery_message">If you would like to add a comment about your order, please write it in the field below.</label>
						<textarea rows="3" cols="120" name="delivery_message"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$('#delivery_fedex').on('click',function(){
		$('.errors').empty();
		if($('input[name="address_id"]').length > 0){
			//auth
			let address_id = $('input[name="address_id"]:checked').val(),
				_token = $('input[name="_token"]').val();
			$('.carrier-delay').css('display', 'inline-block');
			$.post('/cart/validate-address-id', {address_id, _token})
				.done(function(response){
					// console.log(response);

					const deliveryFee = parseFloat(response.deliveryFee);
					$('.carrier-price').text(deliveryFee);
					$('.cart-total .total-value').text(response.newTotal);
					$('input[name="total-value"]').val(response.newTotal);
					$('.carrier-delay').css('display', 'none');
					$('#step-4').removeClass('invisible');
				})
				.fail(function(errors){
					const message = `${errors.responseJSON.message} Please use correct address`;
					$('.errors').html(`<div class="alert alert-warning">${message}</div>`);
					$('#delivery_fedex').attr('checked', false).trigger('change');
					$('.carrier-delay').css('display', 'none');
				})
		} else {
			//guest
			let addressData = $('#checkout-form').serialize();console.log(addressData)
			$('.carrier-delay').css('display', 'inline-block');
			$.post('/cart/validate-address', addressData)
				.done(function(response){
					// console.log(response);

					const deliveryFee = parseFloat(response.deliveryFee);
					$('.carrier-price').text(deliveryFee);
					$('.cart-total .total-value').text(response.newTotal);
					$('input[name="total-value"]').val(response.newTotal);
					$('.carrier-delay').css('display', 'none');
					$('#step-4').removeClass('invisible');
				})
				.fail(function(errors){
					const message = `${errors.responseJSON.message} Please use correct address`;
					$('.errors').html(`<div class="alert alert-warning">${message}</div>`);
					$('#delivery_fedex').attr('checked', false).trigger('change');
					$('.carrier-delay').css('display', 'none');
				})
		}
	})
</script>