<section class="checkout-step -current js-current-step">
	<h1 class="step-title h3">
		<i class="material-icons done"></i>
		<span class="step-number">1</span>
		Personal Information
	</h1>

	<div class="content">
		<ul class="nav nav-inline my-2" role="tablist">
			@auth
				<p class="identity">
					Connected as <a href="{{route('user.profile')}}">{{Auth::user()->fullname}}</a>.
				</p>
				<p>
					Not you? <a href="{{route('logout')}}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">Log out</a>
				</p>
				<p><small>If you log out now, your cart will be emptied.</small></p>
				<input name="first_name" type="hidden" value="{{Auth::user()->first_name}}">
				<input name="last_name" type="hidden" value="{{Auth::user()->last_name}}" >
				<input name="email" type="hidden" value="{{Auth::user()->email}}" >
			@else
				<li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#checkout-guest-form" role="tab"
						aria-controls="checkout-guest-form" aria-selected="true">
						Order as a guest
					</a>
				</li>
			@endauth
		</ul>

		@guest
		<div class="tab-content">
			<div class="tab-pane active" role="tabpanel" id="registerForm">

				<section>
					<div class="form-group row ">
						<label class="col-md-3 form-control-label">Social title</label>
						<div class="col-md-6 form-control-valign">
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="1" checked>
									<span></span>
								</span>
								Mr.
							</label>
							<label class="radio-inline">
								<span class="custom-radio">
									<input name="gender" type="radio" value="2">
									<span></span>
								</span>
								Mrs.
							</label>
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">First name</label>
						<div class="col-md-6">
							<input class="form-control" name="first_name" type="text" value="{{Auth::user()->first_name ?? ''}}" required>
						</div>
						<div class="col-md-3 form-control-comment">
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Last name</label>
						<div class="col-md-6">
							<input class="form-control" name="last_name" type="text" value="{{Auth::user()->last_name ?? ''}}" required>
						</div>
						<div class="col-md-3 form-control-comment">
						</div>
					</div>

					<div class="form-group row ">
						<label class="col-md-3 form-control-label required">Email</label>
						<div class="col-md-6">
							<input class="form-control" name="email" type="email" value="{{Auth::user()->email ?? ''}}"
								required />
						</div>
						<div class="col-md-3 form-control-comment"></div>
					</div>
				</section>
			</div>
		</div>
		@endguest
	</div>
</section>
