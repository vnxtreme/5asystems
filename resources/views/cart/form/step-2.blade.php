<section class="checkout-step -current js-current-step">
	<h1 class="step-title h3">
		<i class="material-icons done"></i>
		<span class="step-number">2</span>
		Addresses
	</h1>

	<div class="content">
		<div class="js-address-form">
			<p>The selected address will be used both as your personal address (for invoice) and as your
				delivery address.</p>
			<div id="delivery-address" class="js-address-form row" style="display:flex; flex-wrap: wrap;">
				@if(Auth::check() and Auth::user()->addresses->isNotEmpty())

					@foreach (Auth::user()->addresses as $address)
						<article class="address-item selected col-sm-4" onclick="resetShippingMethod();" id="id-address-delivery-address-{{$address->id}}">
							<header class="h4">
								<label class="radio-block">
									<span class="custom-radio">
										<input type="radio" name="address_id" value="{{$address->id}}"
											{{$loop->index == 0 ? 'checked' : ''}}>
										<span></span>
									</span>
									<span class="address-alias h4">{{$address->fullname}}</span>
									<div class="address">{{$address->company}}<br>{{$address->address}}<br>{{$address->city}}
										{{$address->postal_code}}<br>{{$address->phone}}</div>
								</label>
							</header>
						</article>
					@endforeach
					<script>
						var resetShippingMethod = function(){
							$('#delivery_fedex').prop('checked', false);
							$('#step-4').addClass('invisible');
						}
					</script>
					<p class="add-address row">
						<a href="{{route('user.addresses.add')}}"> <i class="material-icons"></i>add new address</a>
					</p>
				@else
					<section class="form-fields" style="width:100%;">
						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								Address
							</label>
							<div class="col-md-6">
								<input class="form-control" name="address" id="address" type="text" value="" maxlength="128" required="">
							</div>

							<div class="col-md-3 form-control-comment">
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								City
							</label>
							<div class="col-md-6">
								<input class="form-control" name="city" id="city" type="text" value="" maxlength="64" required="">
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								State Or Province Code
							</label>
							<div class="col-md-6">
								<input class="form-control" name="state_or_province_code" id="state_or_province_code" type="text" value="" maxlength="64" required="">
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label required">
								Zip/Postal Code
							</label>
							<div class="col-md-6">
								<input class="form-control" name="postal_code" id="postal_code" type="text" value="" maxlength="12"
									required="">
							</div>

							<div class="col-md-3 form-control-comment">
							</div>
						</div>

						<div class="form-group row ">
							<label class="col-md-3 form-control-label">
								Phone
							</label>
							<div class="col-md-6">
								<input class="form-control" name="phone" type="text" value="" maxlength="32">
							</div>

							<div class="col-md-3 form-control-comment">
								Optional
							</div>
						</div>
					</section>
				@endauth
			</div>
		</div>

	</div>
</section>