<section id="step-4" class="checkout-step -current -reachable js-current-step invisible">
	<h1 class="step-title h3">
		<i class="material-icons done"></i>
		<span class="step-number">4</span>
		Payment
	</h1>

	<div class="content">
		<div class="payment-options ">
			{{-- Pay by bank wire --}}
			<div>
				<div class="payment-option clearfix">
					<span class="custom-radio float-xs-left">
						<input class="ps-shown-by-js " id="payment-option-1" name="payment_method" value="bank" type="radio" required>
						<span></span>
					</span>

					<label for="payment-option-1">
						<span>Pay by bank wire</span>
					</label>
				</div>
			</div>

			<div id="payment-option-1-additional-information" class="js-additional-information definition-list additional-information">
				<section>
					<p>
						Please transfer the invoice amount to our bank account. You will receive our order
						confirmation by email containing bank details and order number.
						Goods will be reserved 7 days for you and we'll process the order immediately after
						receiving the payment.
					</p>
					<p>Payment is made by transfer of the invoice amount to the following account:</p>

					<dl>
						<dt>Amount</dt>
						<dd class="cart-total">USD <span class="total-value">{{number_format($newTotal, 2)}}</span> (tax incl.)</dd>
						<dt>Bank name</dt>
						<dd>{{setting('bank.name') ?? '___________'}}</dd>
						<dt>Name of account owner</dt>
						<dd>{{setting('bank.owner') ?? '___________'}}</dd>
						<dt>Please include these details</dt>
						<dd>{{setting('bank.note') ?? '___________'}}</dd>

					</dl>
					<p>
						<div class="ps-shown-by-js" style="text-align:center;">
							<button type="submit" class="btn btn-primary center-block place-your-order">
								Place your order
							</button>
						</div>
					</p>
				</section>
			</div>
			{{-- ./Pay by bank wire --}}

			{{-- Paypal --}}
			<div>
				<div id="payment-option-2-container" class="payment-option clearfix">
					<span class="custom-radio float-xs-left">
						<input class="ps-shown-by-js " id="payment-option-2" name="payment_method" value="paypal" type="radio" required="">
						<span></span>
					</span>

					<label for="payment-option-2">
						<span>Paypal</span>
					</label>

				</div>
			</div>
			<div id="payment-option-2-additional-information" class="js-additional-information definition-list additional-information ps-hidden ">
				<section>
					<div id="paypal-button-container"></div>

					<!-- Include the PayPal JavaScript SDK -->
					<script
						src="https://www.paypal.com/sdk/js?client-id=AX5c44JpPyyLqNGP5-F6gv7QK2TSstiqq__XmTRla_CBjXvvMq_xQmuC82mf1ZtlDkxMyQmHCn4cpVdb&currency=USD">
					</script>

					<script>
						// Render the PayPal button into #paypal-button-container
						paypal.Buttons({
							onInit: function(data, actions) {
								// Disable the buttons
								actions.disable();
	
								document.querySelector('#delivery_fedex')
									.addEventListener('change', function(event) {
										if (event.target.checked) {
											actions.enable();
										} else {
											actions.disable();
										}
									});
							},
							// onClick is called when the button is clicked
							onClick: function(data, actions) {
								let firstName = $('input[name="first_name"]').val(),
									lastName = $('input[name="last_name"]').val(),
									email = $('input[name="email"]').val(),
									address = $('#address').val(),
									city = $('#city').val(),
									state_or_province_code = $('#state_or_province_code').val(),
									postal_code = $('#postal_code').val(),
									delivery_method = $('#delivery_fedex').is(':checked');
								if($('input[name="address_id"]').length > 0){
									return actions.resolve();
								}else {
									if (!firstName || !lastName || !email || !address || !city || !state_or_province_code || !postal_code || !postal_code) {
										$.notify('Please fill in all fields', 'error');
										return actions.reject();
									} else {
										return actions.resolve();
									}
								}
							},
							// Set up the transaction
							createOrder: function(data, actions) {
								return actions.order.create({
									purchase_units: [{
										amount: {
											value: $('input[name="total-value"]').val()
										}
									}]
								});
							},
							// Finalize the transaction
							onApprove: function(data, actions) {
								return actions.order.capture().then(function(details) {
									// Show a success message to the buyer
									alert(`Transaction completed by ${details.payer.name.given_name}!`);
									$('#data-response').val(JSON.stringify(details));
									if(details.status == "COMPLETED"){
										$('#data-transaction-id').val(details.purchase_units[0].payments.captures[0].id);
									}
									$('#step-4').addClass('invisible');
									$('#checkout-form').submit();
								});
							}
						}).render('#paypal-button-container');
					</script>
				</section>
			</div>
			{{-- ./Paypal --}}
		</div>

		{{-- <div id="payment-confirmation">
			<div class="ps-shown-by-js">
				<button type="submit" class="btn btn-primary center-block place-your-order">
					Place your order
				</button>
			</div>
		</div> --}}
	</div>
</section>