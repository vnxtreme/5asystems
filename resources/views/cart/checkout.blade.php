@extends('_layout')
@section('content')

<section id="content">
	<div class="row">
		<form id="checkout-form" method="POST" action="{{route('cart.checkout.post')}}">
			@csrf
			<div class="col-md-8">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul style="margin:0;">
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@include('cart.form.step-1')
				@include('cart.form.step-2')
				@include('cart.form.step-3')
				@include('cart.form.step-4')
				<input id="data-response" type="hidden" name="data" value="">
				<input id="data-transaction-id" type="hidden" name="transaction_id" >
			</div>
		</form>

		<div class="col-md-3">
			<section id="js-checkout-summary" class="card js-cart">
				<div class="card-block">
					<div class="cart-summary-products">
						<p>{{Cart::count()}} items</p>
						<p>
							<a href="#" data-toggle="collapse" data-target="#cart-summary-product-list">
								show details
							</a>
						</p>

						<div class="collapse" id="cart-summary-product-list">
							@if(Cart::content())
							<ul class="media-list">
								@foreach(Cart::content() as $item)
								<li class="media">
									<div class="media-left">
										<img class="media-object" src="{{asset("storage/{$item->options->image}")}}" alt="{{$item->name}}">
									</div>
									<div class="media-body">
										<span class="product-name">{{$item->name}}</span>
										<span class="product-quantity">x{{$item->qty}}</span>
										<span class="product-price float-xs-right">USD {{$item->price}}</span>
									</div>
								</li>
								@endforeach
							</ul>
							@endif
						</div>

					</div>

					<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-products">
						@if(session()->has('coupon'))
						<div class="cart-summary-line cart-total">
							<span class="label">Discount</span>
							<span class="value"> -{{$discount}}</span>
						</div>
						@endif

						<span class="label">Subtotal</span>
						<span class="value">{{number_format($newSubtotal,2)}}</span>
					</div>
					<div class="cart-summary-line cart-summary-subtotals" id="cart-subtotal-shipping">
						<span class="label">Shipping</span>
						<span class="value carrier-price">{{number_format($deliveryFee, 2)}}</span>
					</div>
				</div>

				<hr class="separator">
				<div class="card-block cart-summary-totals">
					<div class="cart-summary-line cart-total">
						<span class="label">Total (tax incl.)</span>
						<span class="value">{{config('cart.currency')}} <span class="total-value">{{number_format($newTotal, 2)}}</span></span>
						<input type="hidden" name="total-value" />
					</div>
				</div>

			</section>
		</div>
	</div>
</section>

<script defer>
	document.body.id = 'checkout';
	$('#checkout-form').on('submit', function(){
		$('.place-your-order').attr('disabled', true);
	})
</script>

@endsection