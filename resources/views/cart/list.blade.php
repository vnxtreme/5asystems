@extends('_layout')
@section('content')

<script>
	document.body.id = 'cart';
</script>

@php
$cart = Cart::content();
@endphp

<section id="main">
	<div class="cart-grid row">

		<!-- Left Block: cart product informations & shpping -->
		<div class="cart-grid-body col-xs-12 col-sm-7 col-xl-8">

			<!-- cart products detailed -->
			<div class="card cart-container">
				<div class="card-block">
					<h1 class="h1">Shopping Cart</h1>
				</div>
				<hr class="separator">

				<div class="cart-overview js-cart">
					@if($cart->isNotEmpty())
					<ul class="cart-items">
						@foreach ($cart as $item)
						<form method="POST" action="{{route('cart.update')}}">
							@csrf
							<input type="hidden" name="rowId" value="{{$item->rowId}}" />
							<li class="cart-item">
								<div class="product-line-grid">
									<!--  product left content: image-->
									<div class="product-line-grid-left col-md-3 col-xs-4">
										<span class="product-image media-middle">
											<img src="{{asset("storage/{$item->options->image}")}}" alt="{{$item->name}}">
										</span>
									</div>

									<!--  product left body: description -->
									<div class="product-line-grid-body col-md-4 col-xs-8">
										<div class="product-line-info">
											<a class="label" href="{{route('product.detail', ['slug'=> $item->options->slug])}}">{{$item->name}}</a>
										</div>

										<div class="product-line-info product-price h5 ">
											<div class="current-price">
												<span class="price">{{ config('cart.currency')." ".number_format($item->price, 2) }}</span>
											</div>
										</div>

										<br>

										<div class="product-line-info">
											<span class="label">Size:</span>
											<span class="value">{{$item->options->size}}</span>
										</div>
										<div class="product-line-info">
											<span class="label">Color:</span>
											<span class="value">{{$item->options->color}}</span>
										</div>

									</div>

									<!--  product left body: description -->
									<div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
										<div class="row">
											<div class="col-xs-4 hidden-md-up"></div>
											<div class="col-md-10 col-xs-10">
												<div class="row">
													<div class="col-md-6 col-xs-6 qty">
														<div class="bootstrap-touchspin">
															<input class="js-cart-line-product-quantity form-control" type="text"
																value="{{ $item->qty }}" name="product-quantity-spin" min="1" />
														</div>
														<button type="submit" class="btn btn-primary">Update</button>
													</div>
													<div class="col-md-6 col-xs-2 price">
														<span class="product-price">
															<strong>{{ config('cart.currency')." ".number_format($item->price*$item->qty, 2) }}</strong>
														</span>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-xs-2 text-xs-right">
												<div class="cart-line-product-actions">
													<a class="remove-from-cart" rel="nofollow"
														href="{{route('cart.remove', ['rowId' => $item->rowId])}}">
														<i class="material-icons float-xs-left">delete</i>
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="clearfix"></div>
								</div>

							</li>
						</form>
						@endforeach
					</ul>
					@else
					<span class="no-items">There are no more items in your cart</span>
					@endif
				</div>

			</div>

			<a class="label" href="{{route('home')}}">
				<i class="material-icons">chevron_left</i>Continue shopping
			</a>
			<!-- shipping informations -->
		</div>

		<!-- Right Block: cart subtotal & cart total -->
		<div class="cart-grid-right col-xs-12 col-sm-5 col-xl-4">
			<div class="card cart-summary">
				<div class="cart-detailed-totals">

					<div class="card-block">
						<div class="cart-summary-line" id="cart-subtotal-products">
							<span class="label js-subtotal">{{Cart::count()}} items</span>
							<span class="value"> {{Cart::subtotal() ?? 0}}</span>
						</div>
						<div class="cart-summary-line" id="cart-subtotal-shipping">
							<span class="label">Shipping</span>
							<span class="value">{{number_format($deliveryFee, 2)}}</span>
							<div><small class="value"></small></div>
						</div>
					</div>

					<hr class="separator">

					<div class="card-block">
						@if(session()->has('coupon'))
						<div class="cart-summary-line cart-total">
							<span class="label">Discount</span>
							<small>
								<form action="{{ route('coupon.destroy') }}" method="POST" style="display: inline;">
									@csrf
									<input type="submit" value="remove" style="border: 1px solid gray;"/>
								</form>
							</small>
							<span class="value"> -{{$discount}}</span>
						</div>
						@endif

						<div class="cart-summary-line cart-total">
							<span class="label">Total (tax incl.)</span>
							<span class="value"> {{number_format($newTotal, 2) ?? 0}}</span>
						</div>
					</div>

					<hr class="separator">

					<div class="card-block">
						<div class="cart-summary-line cart-total">
							<form action="{{route('coupon.apply')}}" method="POST">
								@csrf
								<div class="form-group row">
									<label class="col-xs-3 form-control-label required">Coupon</label>
									<div class="col-xs-5">
										<input class="form-control form-control-inline" name="coupon_code" type="text" required="">
									</div>
									<button class="btn btn-primary" type="submit">Apply</button>

									@if (session()->has('coupon_code') or $errors->has('coupon_code'))
									<div class="cart-summary-line cart-total" style="text-align: center">
										<span class="label" role="alert">
											<strong>{{ session()->get('coupon_code') ?? $errors->first('coupon_code')  }}</strong>
										</span>
									</div>
									@endif

								</div>
							</form>
						</div>

					</div>

					<hr class="separator">
				</div>

				<div class="checkout cart-detailed-actions card-block">
					<div class="text-sm-center">
						<a href="{{isset($item) ? route('cart.checkout') : 'javascript:;'}}" class="btn btn-primary">Proceed to
							checkout</a>
					</div>
				</div>
			</div>


		</div>

	</div>
</section>
@endsection