@extends('_layout')
@section('content')
<section id="main">
	<section id="content-hook_order_confirmation" class="card">
		<div class="card-block">
			<div class="row">
				<div class="col-md-12">
					<h3 class="h1 card-title">
						<i class="material-icons done"></i>Your order is confirmed
					</h3>
					<p>
						An email has been sent to your mail address {{$orderInfo->email}}.
					</p>
				</div>
			</div>
		</div>
	</section>

	{{-- <section id="content" class="page-content page-order-confirmation card">
		<div class="card-block">
			<div class="row">


				<div id="order-items" class="col-md-12">


					<h3 class="card-title h3">Order items</h3>


					<div class="order-confirmation-table">


						<div class="order-line row">
							<div class="col-sm-2 col-xs-3">
								<span class="image">
									<img
										src="https://webbay.pl/szablony/60_digital_electronics_store/31-home_default/test.jpg">
								</span>
							</div>
							<div class="col-sm-4 col-xs-9 details">
								<span>Red Headphones Beats - Size : S- Color : White</span>

							</div>
							<div class="col-sm-6 col-xs-12 qty">
								<div class="row">
									<div class="col-xs-5 text-sm-right text-xs-left">PLN2,061.00</div>
									<div class="col-xs-2">1</div>
									<div class="col-xs-5 text-xs-right bold">PLN2,061.00</div>
								</div>
							</div>
						</div>

						<hr>

						<table>
							<tbody>
								<tr>
									<td>Subtotal</td>
									<td>PLN2,061.00</td>
								</tr>
								<tr>
									<td>Shipping and handling</td>
									<td>Free</td>
								</tr>
								<tr class="font-weight-bold">
									<td><span class="text-uppercase">Total</span> (tax incl.)</td>
									<td>PLN2,061.00</td>
								</tr>
							</tbody>
						</table>


					</div>
				</div>



				<div id="order-details" class="col-md-4">
					<h3 class="h3 card-title">Order details:</h3>
					<ul>
						<li>Order reference: PFEAIMSNS</li>
						<li>Payment method: Payments by check</li>
						<li>
							Shipping method: Digital Electronics<br>
							<em>Pick up in-store</em>
						</li>
					</ul>
				</div>


			</div>
		</div>
	</section> --}}

	<section id="content-hook_payment_return" class="card definition-list">
		<div class="card-block">
			<div class="row">
				<div class="col-md-12">
					<p>
						<br><br><strong>Your order will be sent as soon as we receive your payment.</strong>
						<br><br>For any questions or for further information, please contact our customer service department.
					</p>

				</div>
			</div>
		</div>
	</section>

</section>
@endsection