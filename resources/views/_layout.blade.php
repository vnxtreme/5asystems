<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<title>{{$seoMeta['seo_title'] ?? setting('site.title')}}</title>
	<meta name="description" content="{{$seoMeta['meta_description'] ?? setting('site.description')}}">
	<meta name="keywords" content="{{$seoMeta['meta_keywords'] ?? setting('site.keywords')}}">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('favicon.ico')}}">
	<link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">

	<meta property='og:type' content='website' />
	<meta property='og:site_name' content='{{setting('site.title')}}' />
	<meta property='og:title' content='{{$seoMeta['seo_title'] ?? setting('site.title')}}' />
	<meta property='og:description' content='{{$seoMeta['seo_description'] ?? setting('site.description')}}' />
	<meta property='og:url' content='{{url()->current()}}' />
	<meta property='og:image' content='{{$seoMeta['meta_image_fb'] ?? ''}}' />

	<link rel="stylesheet" href="{{asset('css/theme.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('css/productcomments.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('css/jquery-ui.min.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('css/jquery.ui.theme.min.css')}}" type="text/css" media="all">
	<link rel="stylesheet" href="{{asset('css/homeslider.css')}}" type="text/css" media="all">

	
	<script type="text/javascript" src="{{asset('js/jquery-1.11.0.min.js')}}"></script>

	<script type="text/javascript" src="{{asset('js/prestashop.js')}}"></script>
	<script type="text/javascript">
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
		var zoom_type = 'lens';
		var zoom_fade_in = 400;
		var zoom_fade_out = 550;
		var zoom_cursor_type = 'default';
		var zoom_window_pos = 1;
		var zoom_scroll = true;
		var zoom_easing = true;
		var zoom_tint = true;
		var zoom_tint_color = '#333';
		var zoom_tint_opacity = 0.4;
		var zoom_lens_shape = 'round';
		var zoom_lens_size = 345;
	</script>
	<script type="text/javascript">
		function applyElevateZoom(){
			var bigimage = $('.js-qv-product-cover').attr('src'); 
			$('.js-qv-product-cover').elevateZoom({
			  zoomType: zoom_type,
			  cursor: zoom_cursor_type,
			  zoomWindowFadeIn: zoom_fade_in,
			  zoomWindowFadeOut: zoom_fade_out,
			  zoomWindowPosition: zoom_window_pos,
			  scrollZoom: zoom_scroll,
			  easing: zoom_easing,
			  tint: zoom_tint,
			  tintColour: zoom_tint_color,
			  tintOpacity: zoom_tint_opacity,
			  lensShape: zoom_lens_shape,
			  lensSize: zoom_lens_size,
			  zoomImage: bigimage,
			  borderSize:1,borderColour:'#ebebeb', zoomWindowWidth:521, zoomWindowHeight:521, zoomLevel:0.7,lensBorderSize:0
			});
	  	}
  	$(document).ready(function(e) {
	  if($(".zoomContainer").length){
	  $(".zoomContainer").remove();	
	  }
	  applyElevateZoom();
	  $(document).on('click','.input-color',function(e) {
		  restartElevateZoom();
	  });
	  $(document).on('click','.js-qv-mask img.thumb',function(e) {
		  restartElevateZoom();
		});
	});	

	function restartElevateZoom(){
	  $(".zoomContainer").remove();
	  applyElevateZoom();
	}
	</script>

</head>

<body class="layout-full-width page-index tax-display-enabled">
	<!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
            xfbml            : true,
            version          : 'v3.3'
            });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
	</script>
	
	<main>
		@include('components.header')

		<aside id="notifications">
			<div class="container"></div>
		</aside>

		<section id="wrapper">
			<div class="container" style="margin-top: 20px;">
				<nav data-depth="1" class="breadcrumb hidden-sm-down">
					<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
						<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
							<a itemprop="item" href="{{route('home')}}">
								<span itemprop="name">Home</span>
							</a>
							<meta itemprop="position" content="1">
						</li>
					</ol>
				</nav>

				@yield('content')
			</div>
		</section>

		@include('components.footer')

	</main>


	<script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/core.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/theme.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.elevatezoom.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/responsiveslides.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/homeslider.js')}}"></script>
	{{-- <script type="text/javascript" src="{{asset('js/ps_searchbar.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('js/ps_shoppingcart.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/smartblog.js')}}"></script>
	{{-- <script type="text/javascript" src="{{asset('js/ps_searchbarfound.js')}}"></script> --}}
	<script type="text/javascript" src="{{asset('js/jquery.textareaCounter.plugin.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/ddproductcomments.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.rating.pack.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/owl.carousel.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/totalstorage.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/notify.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
	@stack('js')

	<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all" id="ui-id-1" tabindex="0"
		style="display: none;"></ul>

	<!-- Your customer chat code -->
    <div class="fb-customerchat"
        attribution=setup_tool
        page_id="{{setting('social.fb_page_id')}}"
        theme_color="#0a4a7c"
        logged_in_greeting="Chào bạn! Chúng tôi luôn sẳn sàng để trả lời bất kỳ câu hỏi nào của bạn."
        logged_out_greeting="Chào bạn! Chúng tôi luôn sẳn sàng để trả lời bất kỳ câu hỏi nào của bạn."
        greeting_dialog_display="hide">
	</div>

	@if(setting('social.whatsapp'))
	<div class="" style="position:fixed; bottom: 2%; left: 1%; z-index: 100;">
		<a target="_blank" href="https://api.whatsapp.com/send?phone={{setting('social.whatsapp')}}&text=Hello&source=&data=">
			<img src="{{asset('img/whatsapp.png')}}" alt="" width="80">
		</a>
	</div>
	@endif
</body>

</html>