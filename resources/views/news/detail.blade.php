@extends('_layout')
@section('content')
<div id="content-wrapper">
	<div class="width-blog">
		<div id="content" class="block">
			<div itemtype="#" itemscope="" id="sdsblogArticle" class="blog-post">
				<div itemprop="articleBody">
					<div class="page-item-title">
						<h1>{{$post->title}}</h1>
					</div>
					<div class="post-info">
						<span>
							Posted by
							&nbsp;
							<span class="author" itemprop="author"><i
									class="material-icons user"></i>&nbsp;&nbsp;{{$post->author->name}}</span>&nbsp;&nbsp;
							<span class="dateCreated" itemprop="dateCreated"><i
									class="material-icons calendar"></i>&nbsp;&nbsp;{{$post->created_at->format('m-d-Y')}}</span>&nbsp;&nbsp;
						</span>
					</div>
					<div id="lipsum" class="articleContent">
						<a id="post_images" href="{{asset("storage/{$post->image}")}}">
							<img src="{{asset("storage/{$post->image}")}}" alt="{{$post->title}}">
						</a>
					</div>

					<div class="sdsarticle-des">
						{!! $post->body !!}
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection