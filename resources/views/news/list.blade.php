@extends('_layout')
@section('content')
<div id="content-wrapper">
	<div id="smartblogcat" class="block">

		@forelse ($posts as $post)
		<div itemtype="#" itemscope="" class="sdsarticleCat clearfix">
			<div id="smartblogpost-5">
				<div class="row">
					<div class="articleContent col-sm-4 col-xs-4">
						<a itemprop="url" href="{{route('news.detail', ['slug'=>$post->slug])}}"
							title="{{$post->title}}" class="imageFeaturedLink">

							<img itemprop="image" alt="{{$post->title}}" src="{{asset("storage/{$post->image}")}}" class="imageFeatured" />
						</a>
					</div>
					<div class="blog_desc col-sm-7 col-xs-7">
						<div class="sdsarticleHeader">
							<p class="sdstitle_block"><a title="{{$post->title}}"
									href="{{route('news.detail', ['slug'=>$post->slug])}}">{{$post->title}}</a>
							</p>
						</div>
						<span class="blogdetail">
							<span class="ttpost">Posted by </span>
							&nbsp;
							<span class="author" itemprop="author">
								<i class="material-icons user"></i>&nbsp;&nbsp;{{$post->author->name}}
							</span>
						</span>
						<div class="sdsarticle-des">
							<span itemprop="description" class="clearfix">
								<div id="lipsum">
									{{$post->excerpt}}
								</div>
							</span>
						</div>
						<div class="sdsreadMore">
							<span class="more">
								<a title="{{$post->title}}"
									href="{{route('news.detail', ['slug'=>$post->slug])}}" class="r_more">Read more</a>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		@empty
		<div itemtype="#" itemscope="" class="sdsarticleCat clearfix">
			No article
		</div>
		@endforelse

	</div>
</div>
@endsection