@extends('_layout')
@section('content')

<div id="content-wrapper">
	<section id="main">
		<h2 class="h2">Search results for: {{request()->key}}</h2>

		<section id="products">
			<div>
				<div id="js-product-list-top" class="products-selection">

					<div class="col-md-6 hidden-md-down total-products">
						<ul class="display hidden-xs grid_list">
							<li id="grid" class="selected"><a href="#" title="Grid">Grid</a></li>
							<li id="list"><a href="#" title="List">List</a></li>
						</ul>

						<p>There are {{$products->total()}} products.</p>
					</div>

					<div class="col-md-6 sortowanie">
						@include('components.filter')
					</div>
					
					<div class="col-sm-12 hidden-lg-up showing">
							Showing {{$products->firstItem()}} - {{$products->lastItem()}} of {{$products->total()}} item(s)
					</div>
				</div>
			</div>

			<div id="" class="hidden-sm-down"></div>

			<div>
				<div id="js-product-list">
					<div class="products">
						<ul class="product_list gridcount grid">
							<!-- removed product_grid-->

							@forelse ($products as $product)
								@php
										$productDetail = $product->firstProductDetail;
								@endphp
								<li class="product_item col-lg-4 col-xs-12 col-sm-6 col-md-6 col-lg-3">
									<article class="product-miniature js-product-miniature" itemscope="" itemtype="http://schema.org/Product">
										<div class="thumbnail-container">
											<div class="dd-product-image">
												<a href="{{route('product.detail', ['slug' => $product->slug])}}" class="thumbnail product-thumbnail">
													<img class="ddproduct-img1" src="{{asset("storage/{$product->image}")}}" alt="{{$product->name}}">
													<img class="drugi_image img-responsive" src="{{asset("storage/{$product->image}")}}" alt="">
												</a>
											</div>
	
										</div>
										<div class="product-description">
											<h3 class="h3 product-title" itemprop="name">
												<a href="{{route('product.detail', ['slug' => $product->slug])}}">{{$product->name}}</a>
											</h3>
											<div class="product-price-and-shipping">
												<span class="sr-only">Price</span>
												<span itemprop="price" class="price">USD {{$productDetail->price}}</span>
											</div>
											<div class="product-detail">
													<p>{{$product->excerpt}}</p>
											</div>
										</div>

										@include('components.highlighted-information')

									</article>
								</li>
							@empty
								<p>No product available</p>
							@endforelse
						</ul>
					</div>

					<nav class="pagination">
						<div class="col-md-4">
								Showing {{$products->firstItem()}} - {{$products->lastItem()}} of {{$products->total()}} item(s)
						</div>
						<div class="col-md-7-5">
								{{ $products->appends(request()->except('page'))->links('vendor.pagination.customize') }}
						</div>
					</nav>

				</div>
			</div>
			<div id="js-product-list-bottom">
				<div id="js-product-list-bottom"></div>
			</div>

		</section>

	</section>
</div>
@endsection