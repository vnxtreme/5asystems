@extends('_layout')
@section('content')

@push('js')
<script>	
	$('input[name="options[color]"]').change(function(e){
		window.location.href = e.target.parentElement.href;
	})
</script>
@endpush

@php
$images = (isset($firstProductDetail) and $firstProductDetail->images) ? json_decode($firstProductDetail->images) : '';
@endphp

<nav data-depth="2" class="breadcrumb hidden-sm-down">
	<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route('home')}}">
				<span itemprop="name">Home</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route( 'product.list', ['slug' => $product->category->slug] )}}">
				<span itemprop="name">{{$product->category->name}}</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="javascript:;">
				<span itemprop="name">{{$product->name}}</span>
			</a>
			<meta itemprop="position" content="3">
		</li>
	</ol>
</nav>

<div id="content-wrapper">

	<section id="main" itemscope="" itemtype="https://schema.org/Product">
		<meta itemprop="url" content="{{request()->url()}}">

		<div class="col-md-13 description-disabled-1">
			<div class="short_description">Short description</div>

			<div id="product-description-short-26" itemprop="description">
				<p>{{$product->excerpt}}</p>
			</div>
		</div>

		<div class="col-md-14">
			<h1 class="h1 product" itemprop="name">{{$product->name}}</h1>
			<section class="page-content" id="content">
				<div class="images-container">
					<div class="product-cover">
						@if($images)
						<img class="js-qv-product-cover" src="{{asset("storage/{$images[0]}")}}" alt="" title=""
							style="width:100%;" itemprop="image" />
						@endif
						<div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
							<i class="material-icons zoom-in"></i>
						</div>
					</div>

					<div class="js-qv-mask mask additional_slider">
						<ul id="thumbnailCarousel" class="product_list thumbnail-carousel owl-carousel owl-theme">
							@if($images)
							@foreach ($images as $image)
							<li class="thumb-container item">
								<img class="thumb js-thumb" data-image-medium-src="{{asset("storage/{$image}")}}"
									data-image-large-src="{{asset("storage/{$image}")}}"
									src="{{asset("storage/{$image}")}}" alt="" title="" width="100" itemprop="image" />
							</li>
							@endforeach
							@endif

						</ul>

						<div class="SliderNavigation">
							<a class="btn prev slider_thumb_prev">&nbsp;</a>
							<a class="btn next slider_thumb_next">&nbsp;</a>
						</div>
					</div>

				</div>

				<div class="scroll-box-arrows">
					<i class="material-icons left"></i>
					<i class="material-icons right"></i>
				</div>


			</section>

		</div>

		<div class="col-md-13 description-disabled-2">
			<div class="short_description">Short description</div>

			<div id="product-description-short-26" class="description" itemprop="description">
				<p>{{$product->excerpt}}</p>
			</div>
		</div>
		<div class="col-md-13">
			<div class="product-information">
				<div class="product-prices">
					<div class="product-price h5 " itemprop="offers" itemscope="" itemtype="https://schema.org/Offer">
						<link itemprop="availability" href="https://schema.org/InStock">
						<meta itemprop="priceCurrency" content="USD">

						<div class="current-price">
							<span itemprop="price"
								content="{{$firstProductDetail->price}}">{{"USD {$firstProductDetail->price}" }}</span>
						</div>
					</div>

					<div class="tax-shipping-delivery-label">Tax included</div>
				</div>

				<div class="product-actions">

					<form action="{{route('cart.add')}}" method="post" id="add-to-cart-or-refresh">
						@csrf
						<input type="hidden" name="product_detail_id"
							value="{{request()->pdid ?? $product->productDetail[0]->id}}">
						<input type="hidden" name="product_id" value="{{$product->id}}" >

						<div class="product-variants">
							<div class="clearfix product-variants-item" style="{{$sizeList->count() > 1 ? '' : 'display:none;'}}">
								@if($sizeList)
								<span class="control-label">Size</span>
								<select id="product_size"  name="options[size]">
									@foreach($sizeList as $productSize)
										<option data-product-detail="{{$productSize->id}}" 
											value="{{$productSize->size->id}}" 
											{{request()->pdid == $productSize->id ? "selected" : ($productSize->id == $firstProductDetail->id ? "selected" : '')}}
										>
											{{$productSize->size->name}}
										</option>
									@endforeach
								</select>
								@endif
							</div>
							<script>
								$('#product_size').on('change', function(ele){
									const pdid = $(this).find(":selected").attr('data-product-detail')
									window.location.href = window.location.origin + window.location.pathname + `?pdid=${pdid}`;
								})
							</script>
							<div class="clearfix product-variants-item">
								<span class="control-label">Color</span>
								<ul id="group_2">
									@foreach($colorList as $colorId => $list)
										@php
											$productColor = $list->first();
										@endphp
										<li class="pull-xs-left input-container" title="{{$productColor->color->name}}">
											<a
												href="{{$firstProductDetail->color_id != $colorId ? route('product.detail', ['slug' => $product->slug, 'pdid'=>$productColor->id]) : 'javascript:;'}}">
												<input class="input-color" type="radio" data-product-attribute="2"
													name="options[color]" value="{{$colorId}}"
													{{$firstProductDetail->color_id == $colorId ? "checked" : ''}}
												/> 

												<span class="color"
													style="background-color: {{$productColor->color->hex}}">
													<span class="sr-only">{{$productColor->color->name}}</span>
												</span>
											</a>
										</li>
									@endforeach
								</ul>
							</div>
						</div>

						<section class="product-discounts"></section>

						<div class="clearfix"></div>
						<div class="product-add-to-cart">
							<span class="control-label">Quantity</span>

							<div class="product-quantity">
								<div class="qty">
									<div class="bootstrap-touchspin">
										<input type="text" name="qty" id="quantity_wanted" value="1"
											class="input-group form-control" min="1" style="display: block;">
									</div>
								</div>
								<div>
									<button class="add-to-cart-buttons-products"
										style="outline: none; text-decoration: none;" data-button-action="add-to-cart"
										title="Add to cart" type="submit">
										Add to cart
									</button>

									<span id="product-availability"></span>
								</div>
							</div>
							<div class="clearfix"></div>
							<p class="product-minimal-quantity"></p>
						</div>

						@include('product.social-share')

						<input class="product-refresh ps-hidden-by-js" name="refresh" type="submit" value="Refresh"
							style="display: none;">

					</form>


				</div>

			</div>
		</div>
	</section>
</div>

{{-- TABS --}}
<div class="tabs">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#description">Description</a>
		</li>
		{{-- <li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#product-details">Product Details</a>
		</li> --}}
	</ul>

	<div class="tab-content" id="tab-content">
		<div class="tab-pane fade in active" id="description">

			<div class="product-description">
				{!! $product->content !!}
			</div>
		</div>

		{{-- <div class="tab-pane fade" id="product-details">
			<div class="product-quantities">
				<label class="label">In stock</label>
				<span>108 Items</span>
			</div>

			<div class="product-out-of-stock"></div>
		</div> --}}

	</div>
	<div class="clearfix"></div>

	@include('components.related-products')

</div>


<footer class="page-footer">
	<!-- Footer content -->
</footer>


@endsection