<div class="social-sharing">
	<span>Share</span>
	<ul>
		<li class="facebook icon-gray">
			<a href="{{"https://www.facebook.com/sharer.php?u=".url()->current()}}" class="text-hide" title="Share" target="_blank">Facebook</a>
		</li>
		<li class="twitter icon-gray">
				
			<a href="{{"https://twitter.com/intent/tweet?text=".url()->current()}}" class="text-hide" title="Tweet" target="_blank">Tweet</a>
		</li>
	</ul>
</div>