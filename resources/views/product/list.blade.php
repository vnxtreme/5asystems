@extends('_layout')
@section('content')
<script>
	document.body.id = 'category';
</script>
@if(isset($category))

<nav data-depth="2" class="breadcrumb hidden-sm-down">
	<ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route('home')}}">
				<span itemprop="name">Home</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		@if($category->parentCategory and $category->parentCategory->parentCategory)
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route( 'product.list', ['slug' => $category->parentCategory->parentCategory->slug] )}}">
				<span itemprop="name">{{$category->parentCategory->parentCategory->name}}</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		@endif

		@if($category->parentCategory)
		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route( 'product.list', ['slug' => $category->parentCategory->slug] )}}">
				<span itemprop="name">{{$category->parentCategory->name}}</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
		@endif

		<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="{{route( 'product.list', ['slug' => $category->slug] )}}">
				<span itemprop="name">{{$category->name}}</span>
			</a>
			<meta itemprop="position" content="1">
		</li>
	</ol>
</nav>

{{-- <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
	<div class="block-categories hidden-sm-down">
		<h4 class="category-title">{{$category->name}}</h4>
		<ul class="category-top-menu">
			<li>
				@if($category->subCategory->isNotEmpty())
				<ul class="category-sub-menu">
					@foreach ($category->subCategory as $subCategory)
					<li data-depth="0">
						<a href="{{route('product.list', ['categorySlug' => $subCategory->slug])}}">{{$subCategory->name}}</a>

						@if($subCategory->subCategory and $subCategory->subCategory->isNotEmpty())
						<div class="navbar-toggler collapse-icons collapsed" data-toggle="collapse"
							data-target="{{"#exCollapsingNavbar{$subCategory->id}"}}" aria-expanded="false">
							<i class="material-icons add"></i><i class="material-icons remove"></i>
						</div>
						<div class="collapse" id="{{"exCollapsingNavbar{$subCategory->id}"}}" aria-expanded="false" style="">
							<ul class="category-sub-menu">
								@foreach ($subCategory->subCategory as $grandSubCategory)
								<li data-depth="1">
									<a class="category-sub-link"
										href="{{route('product.list', ['categorySlug' => $grandSubCategory->slug])}}">{{$grandSubCategory->name}}</a>
								</li>
								@endforeach
							</ul>
						</div>
						@endif
					</li>
					@endforeach
				</ul>
				@endif
			</li>
		</ul>
	</div>
	<div id="search_filters_wrapper" class="hidden-sm-down">
		<div id="search_filter_controls" class="hidden-md-up">
			<span id="_mobile_search_filters_clear_all"></span>
			<button class="btn btn-secondary ok">
				<i class="material-icons"></i>
				OK
			</button>
		</div>
	</div>
</div> --}}
@endif

<div id="content-wrapper" class="left-column col-xs-12{{--  col-sm-8 col-md-9 --}}">
	<section id="main">
		<div class="block-category card card-block hidden-sm-down">
			<h4 class="h4">{{$category->name}}</h4>
		</div>
		<div class="text-xs-center hidden-md-up">
			<h1 class="h1">{{$category->name}}</h1>
		</div>

		<section id="products">
			<div>
				<div id="js-product-list-top" class="products-selection">
					<div class="col-md-6 hidden-md-down total-products">
						<ul class="display hidden-xs grid_list">
							<li id="grid" class="selected"><a href="#" title="Grid">Grid</a></li>
							<li id="list"><a href="#" title="List">List</a></li>
						</ul>

						<p>There are {{$products->count()}} products.</p>
					</div>
					<div class="col-md-6 sortowanie">
						@include('components.filter')
					</div>
					<div class="col-sm-12 hidden-lg-up showing">
							Showing {{$products->firstItem()}} - {{$products->lastItem()}} of {{$products->total()}} item(s)
					</div>
				</div>
			</div>

			<div class="hidden-sm-down">
				<section id="js-active-search-filters" class="hide">
				</section>
			</div>

			<div>
				<div id="js-product-list">
					<div class="products">
						<ul class="product_list gridcount list">
							<!-- removed product_grid-->
							@forelse ($products as $product)
							@php
							$productDetail = $product->productDetail->first();
							@endphp
							<li class="product_item col-lg-4 col-xs-12">
								<div class="product-miniature js-product-miniature" data-id-product="22" data-id-product-attribute="40"
									itemscope="" itemtype="http://schema.org/Product">
									<div class="thumbnail-container col-md-2-5">
										<div class="dd-product-image">

											<a href="{{route('product.detail', ['slug' => $product->slug])}}"
												class="thumbnail product-thumbnail">
												<img class="ddproduct-img1" src="{{asset("storage/{$product->thumbnail('cropped')}")}}" alt="{{$product->name}}">
											</a>
										</div>

									</div>
									<div class="product-description center-block col-xs-4 col-xs-7 col-md-7-5">
										<h3 class="h3 product-title" itemprop="name"><a
												href="{{route('product.detail', ['slug' => $product->slug])}}">{{$product->name}}</a></h3>
										<div class="product-price-and-shipping">
											<span class="sr-only">Price</span>
											<span itemprop="price" class="price">USD {{$productDetail->price}}</span>
										</div>
										<div class="product-detail">
											<p>{{$product->excerpt}}</p>
										</div>

										@include('components.highlighted-information')

									</div>
								</div>
							</li>
							@empty
							<p>No product available</p>
							@endforelse

						</ul>
					</div>

					<nav class="pagination">
						<div class="col-md-6">
							Showing {{$products->firstItem()}} - {{$products->lastItem()}} of {{$products->total()}} item(s)
						</div>
						<div class="col-md-7-5">
							{{ $products->appends(request()->except('page'))->links('vendor.pagination.customize') }}
						</div>
					</nav>

				</div>

			</div>

			<div id="js-product-list-bottom">
				<div id="js-product-list-bottom"></div>
			</div>

		</section>

	</section>
</div>
@endsection