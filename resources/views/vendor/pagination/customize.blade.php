{{-- CUSTOMIZE --}}
@if ($paginator->hasPages())
    <ul class="page-list clearfix text-xs-center" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li>
                <a rel="prev" href="javscript:;" class="previous disabled">
                    <i class="st fa-angle-double-left"></i>
                </a>
            </li>
        @else
            <li class="page-item">
                <a rel="next" href="{{ $paginator->previousPageUrl() }}" class="previous">
                    <i class="st fa-angle-double-left"></i>
                </a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="current" aria-current="page">
                            <a rel="nofollow" href="javascript:;" class="disabled">{{ $page }}</a>
                        </li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                {{-- <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a> --}}
                <a rel="next" href="{{ $paginator->nextPageUrl() }}" class="next" aria-label="@lang('pagination.next')">
                    <i class="st fa-angle-double-right"></i>
                </a>
            </li>
        @else
            <li aria-disabled="true" aria-label="@lang('pagination.next')">
                <a rel="next" href="javscript:;" class="next disabled">
                    <i class="st fa-angle-double-right"></i>
                </a>
            </li>
        @endif
    </ul>
@endif
