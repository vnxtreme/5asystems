@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
						@endif
						<input id="order_information_id" type="hidden" name="order_information_id" value="{{$dataTypeContent->id}}" />

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif
                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->display_name }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add')])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
							@endforeach
							
							{{-- ### CUSTOMIZE ### --}}
							<div class="col-md-12">
								<div class="panel panel panel-bordered panel-warning">
									@php
	                                    $orderedProducts = $dataTypeContent->order;
									@endphp
									<style>
										input{
											color: black!important;
										}
									</style>
	                                <div class="panel-heading">
	                                    <h3 class="panel-title">Ordered products</h3>
	                                </div>
	                                <div class="panel-body">
										
											@foreach ($orderedProducts as $order)
											@php
												// $serials = $order->productDetail->serials;
												$serials = App\Models\Serial::where(function($query) use ($order){
													$query->whereStatus(0)
														->whereProductDetailId($order->productDetail->id);
												})
												->orWhere(function($query) use ($dataTypeContent, $order) {
													$query->where('order_information_id', $dataTypeContent->id)
														->whereProductDetailId($order->productDetail->id);
												})
												->get();
												// $serials = App\Models\Serial::whereStatus(0)->orWhere('order_information_id', $dataTypeContent->id)->get();
											@endphp

		                                    <div class="form-group col-md-1">
		                                        <img src="{{asset("storage/{$order->product_image}")}}" alt="" width="50" height="auto">
		                                    </div>
		                                    <div class="form-group col-md-6">
		                                        <label for="name">Name</label>
		                                        <input type="text" class="form-control" value="{{"{$order->product_detail_id} - {$order->product_name} - {$order->product_color} - {$order->product_size}"}}" disabled/>
		                                    </div>
		                                    <div class="form-group col-md-1">
		                                        <label >Quantity</label>
		                                        <input type="text" class="form-control" value="{{$order->quantity}}" disabled/>
		                                    </div>
		                                    <div class="form-group col-md-2">
		                                        <label >Unit price</label>
		                                        <input type="text" class="form-control" value="{{number_format($order->price, 0, ',','.')}}" disabled/>
		                                    </div>
		                                    
		                                    <div class="form-group col-md-2">
		                                        <label >Total price</label>
		                                        <input type="text" class="form-control total-price" value="{{ number_format($order->quantity * $order->price, 0, ',','.')}}" disabled/>
											</div>
											<div class="form-group col-md-12">
												<label >Serial to be delivery</label>
												{{-- <div class="serials_to_be_delivery"></div> --}}
												<select class="serial-to-be-delivery form-control select2" name="serials[]" multiple>
													@forelse ($serials as $serial)
														<option value="{{$serial->id}}" {{$serial->status == 1 ? 'selected' : ''}}>{{$serial->serial}}</option>
													@empty
														<p>No serial</p>
													@endforelse
												</select>
												{{-- <button class="btn btn-primary float-right" type="submit">Update serials</button> --}}
											</div>
											@endforeach
										
	                                </div>
								</div>
							</div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
//change serial status
$('.serial-to-be-delivery').on("select2:select", function(e) {
	let addSerial = e.params.data.text,
		order_information_id = $('#order_information_id').val();

	$.post('/api/order-information/serial-status-to-1', {serial: addSerial, order_information_id: order_information_id});
})
$('.serial-to-be-delivery').on("select2:unselect", function(e) {
	let removedSerial = e.params.data.text;

    $.post('/api/order-information/serial-status-to-0', {serial: removedSerial});
})
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
