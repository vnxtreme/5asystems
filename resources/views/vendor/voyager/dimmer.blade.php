<div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;background-image:url('{{ $image }}');">
    <div class="dimmer"></div>
    <div class="panel-content">
		<h4>{!! $title !!}</h4>

		@foreach ($lowInQuantity as $item)
        	<p>
				<a style="color: #ccc;"  href="{{route('voyager.products.edit', ['id' => $item['product']['id']] )}}">
					{{$item['product']['name']}}  <span class="badge badge-secondary">{{$item['quantity']}}</span>
				</a>
			</p>
		@endforeach
    </div>
</div>
