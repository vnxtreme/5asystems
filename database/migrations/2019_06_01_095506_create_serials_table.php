<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serials', function (Blueprint $table) {
			$table->increments('id');
			$table->string('serial', 191)->unique();
			$table->string('status', 191)->default(0);
			
            $table->unsignedBigInteger('product_detail_id')->nullable();
            $table->foreign('product_detail_id')->references('id')->on('product_details')->onUpdate('cascade')->onDelete('cascade');
            
            $table->unsignedBigInteger('product_input_id')->nullable();
            $table->foreign('product_input_id')->references('id')->on('product_inputs')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedInteger('order_information_id')->nullable();
            $table->foreign('order_information_id')->references('id')->on('order_informations')->onUpdate('cascade')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serials');
    }
}
