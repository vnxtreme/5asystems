<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_informations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('email', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->string('address', 191)->nullable();
			$table->string('payment_method', 50)->default('bank');
			$table->text('delivery_message')->nullable();
			$table->integer('delivery_fee')->default(0);

			$table->string('coupon')->nullable();
			$table->double('discount')->default(0);
			$table->double('tax')->default(0);
			$table->double('sub_total')->default(0);
			$table->double('total')->default(0);

			$table->string('city', 191)->nullable();
			$table->string('district', 191)->nullable();
			$table->string('ward', 191)->nullable();
			$table->integer('postal_code')->nullable();

			$table->string('status',50)->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_informations');
    }
}
