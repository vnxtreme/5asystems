<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			$table->string('slug')->unique();

			$table->unsignedInteger('category_id')->nullable();
			$table->foreign('category_id')->references('id')->on('product_categories')->onUpdate('cascade')->onDelete('set null');

			$table->text('excerpt')->nullable();
			$table->text('content')->nullable();

			$table->string('image')->nullable();
			$table->string('image_fb')->nullable();

			$table->boolean('feature')->default(0);

			$table->boolean('status')->default(1);
			$table->integer('order')->nullable();

			$table->string('seo_title')->nullable();
			$table->text('meta_description')->nullable();
			$table->text('meta_keywords')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
