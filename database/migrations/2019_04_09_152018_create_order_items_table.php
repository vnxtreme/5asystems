<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_items', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('order_id');
			$table->unsignedBigInteger('product_id');

			$table->unsignedInteger('size_id');
			$table->unsignedInteger('color_id');
			$table->unsignedInteger('warranty_id')->default(12);
			$table->unsignedInteger('unit_id')->default(1);
			$table->integer('quantity');
			$table->integer('price');

			$table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade');
			// $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade');
			// $table->foreign('size_id')->references('id')->on('sizes')->onUpdate('cascade');
			// $table->foreign('color_id')->references('id')->on('colors')->onUpdate('cascade');
			// $table->foreign('warranty_id')->references('id')->on('warranties')->onUpdate('cascade');
			// $table->foreign('unit_id')->references('id')->on('units')->onUpdate('cascade');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('order_items');
	}
}
