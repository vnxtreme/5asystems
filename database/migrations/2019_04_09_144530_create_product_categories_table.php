<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
			$table->increments('id');
			
            $table->string('name');
			$table->string('slug')->unique();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('product_categories')->onUpdate('cascade')->onDelete('set null');
			$table->integer('order')->nullable();
			
			$table->string('image')->nullable();
			
			$table->string('seo_title')->nullable();
			$table->text('meta_description')->nullable();
			$table->text('meta_keywords')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
