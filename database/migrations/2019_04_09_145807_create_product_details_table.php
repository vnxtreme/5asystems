<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('product_id');
  
			$table->unsignedInteger('size_id');
			$table->unsignedInteger('color_id');
			// $table->unsignedInteger('unit_id');
			$table->integer('warranty')->default(12);
			$table->integer('quantity')->default(0);
			$table->double('price')->default(0);

			$table->string('image')->nullable();
			$table->string('image_fb')->nullable();
			$table->text('images')->nullable();

			$table->integer('group_key')->default(0);

			$table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade');
			$table->foreign('size_id')->references('id')->on('sizes')->onUpdate('cascade');
			$table->foreign('color_id')->references('id')->on('colors')->onUpdate('cascade');
			// $table->foreign('warranty_id')->references('id')->on('warranties')->onUpdate('cascade')->onDelete('cascade');
			// $table->foreign('unit_id')->references('id')->on('units')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
